/*jslint node: true */
"use strict";

var gulp = require('gulp'),
    rimraf = require('gulp-rimraf'),
    rename = require('gulp-rename'),
    minifyCss = require('gulp-cssnano'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    minifyjs = require('gulp-uglify'),
    merge = require('merge-stream');

gulp.task('clean', function () {
    return gulp.src('./www/*', {read: false}).pipe(rimraf({ force: true}));
});

gulp.task('html', function () {
    return gulp.src('./src/index.html').pipe(gulp.dest('./www'));
});

gulp.task('vendor', function () {
    return merge(
        gulp.src([
            './node_modules/bootstrap/dist/js/bootstrap.min.js',
            './node_modules/bootstrap-notify/bootstrap-notify.min.js',
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/nedb/browser-version/out/nedb.min.js',
            './node_modules/bluebird/js/browser/bluebird.min.js',
            './node_modules/webmidi/webmidi.min.js',
            './node_modules/soundfont-player/dist/soundfont-player.min.js',
            './node_modules/bootstrap-slider/dist/bootstrap-slider.min.js',
            './src/js/custom-components/bootstrap-slider-knockout-binding.js',
            './node_modules/survey-knockout/survey.ko.min.js',
            './node_modules/glyphicons/glyphicons.js',
            './node_modules/knockstrap/build/knockstrap.min.js'
        ]).pipe(gulp.dest('./www/js')),
        gulp.src('./node_modules/knockout/build/output/knockout-latest.js')
            .pipe(rename('knockout.min.js'))
            .pipe(gulp.dest('./www/js'))
    );
});

gulp.task('js', function () {
    return browserify({
        entries: './src/js/index.js',
        debug: true
    })
        .transform('exposify', {
            expose: {
                'jquery': '$',
                'knockout': 'ko',
                'nedb': 'Nedb',
                'bluebird': 'Promise'
            }
        })
        .transform('stringify', {
            extensions: ['.html'],
            minify: true,
            minifyOptions: {
                removeComments: false
            }
        })
        .bundle()
        .pipe(source('index.js'))
        .pipe(buffer())
        //.pipe(minifyjs()) // uncomment for production
        .pipe(gulp.dest('./www/js'));
});

gulp.task('css', function () {
    return gulp.src([
        './node_modules/bootstrap/dist/css/bootstrap.min.css',
        './node_modules/bootstrap-slider/dist/css/bootstrap-slider.min.css',
        './node_modules/survey-knockout/survey.css',
        './src/css/slider.css',
        './node_modules/font-awesome/css/font-awesome.min.css'
    ]).pipe(gulp.dest('./www/css'));
});

gulp.task('fonts', function () {
    return gulp.src([
        './node_modules/font-awesome/fonts/fontawesome-webfont.eot',
        './node_modules/font-awesome/fonts/fontawesome-webfont.svg',
        './node_modules/font-awesome/fonts/fontawesome-webfont.ttf',
        './node_modules/font-awesome/fonts/fontawesome-webfont.woff',
        './node_modules/font-awesome/fonts/fontawesome-webfont.woff2',
        './node_modules/font-awesome/fonts/FontAwesome.otf',
        './node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.eot',
        './node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.ttf',
        './node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.woff',
        './node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.woff2',
        './node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.svg'
    ]).pipe(gulp.dest('./www/fonts'));
});

gulp.task('build', ['html', 'css', 'js', 'vendor', 'fonts']);

gulp.task('default', ['clean'], function () {
    return gulp.start('build');
});
