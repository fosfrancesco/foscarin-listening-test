(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
/*globals $*/
"use strict";

var Promise = (typeof window !== "undefined" ? window['Promise'] : typeof global !== "undefined" ? global['Promise'] : null);

function Action(options) { // add "options" parameters if needed
    // TODO: Global Initialization
    this.api_caller = options.repositories.api_caller;
}

Action.prototype.run = function (parameters, solve) { // add "onCancel" parameters if needed
    // Parameters:
    // parameters['survey answers']
    // parameters['user id']
    // parameters['test number']
    var tables = {
        '0' : 'progression_compound_maj',
        '1' : 'progression_compound_min',
        '2' : 'progression_ngram_maj',
        '3' : 'progression_ngram_min',
        '4' : 'progression_hmm_maj',
        '5' : 'progression_hmm_min',
        '6' : 'progression_rnn_maj',
        '7' : 'progression_rnn_min'
    };

    // TODO: Execution
    var self = this;
    var url = '/api/progression/' + tables[parameters['test number']];
    self.api_caller.call_api(url,'GET', {}).then(function(result){
        // $.notify({message: 'Downloaded chords'}, {allow_dismiss: true, type: 'success'});
        solve({
            event: 'begin-test-event', // Begin Test
            data: {
                'progressions': result.progressions,
                'survey answers': parameters['survey answers'],
                'user id': parameters['user id'],
                'values history': parameters['values history'] || {
                    0 : [],
                    1 : [],
                    2 : [],
                    3 : [],
                    4 : [],
                    5 : [],
                    6 : [],
                    7 : []
                },
                'test number' : parameters['test number']
            }
        });
    }).catch(function(result){
        solve({
            event: 'begin-test-error-event',
            data: {}
        });
    });
    // THIS CAN BE REMOVED (BEGIN)
    // $.notify({message: 'Compute first chords'}, {allow_dismiss: true, type: 'success'});
    // solve({
    //     event: 'begin-test-event', // Begin Test
    //     data: {
    //         'chords': '0',
    //         'survey answers': parameters['survey answers'],
    //         'user id': parameters['user id'],
    //     }
    // });
    // THIS CAN BE REMOVED (END)
};

exports.createAction = function (options) {
    var action = new Action(options);
    return function (data) {
        return new Promise(function (solve, reject, onCancel) {
            var parameters = (data && data.filters) || {};
            action.run(parameters, solve, onCancel);
        });
    };
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],2:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
/*globals $*/
"use strict";

var Promise = (typeof window !== "undefined" ? window['Promise'] : typeof global !== "undefined" ? global['Promise'] : null);

function Action(options) { // add "options" parameters if needed
    // TODO: Global Initialization
    this.api_caller = options.repositories.api_caller;
}

Action.prototype.run = function (parameters, solve) { // add "onCancel" parameters if needed
    // Parameters:

    // TODO: Execution
    var self = this;
    self.api_caller.call_api('/api/auth','GET', {}).then(function(result){
        // $.notify({message: 'Get Survey'}, {allow_dismiss: true, type: 'success'});
        solve({
            event: 'have-id-event', // Done
            data: {
                'user id':result['user-token'],
            }
        });
    }).catch(function(result){
        solve({
            event: 'have-id-error-event',
            data: {}
        });

    });

    // THIS CAN BE REMOVED (BEGIN)
    // $.notify({message: 'Get Id'}, {allow_dismiss: true, type: 'success'});
    // solve({
    //     event: 'have-id-event', // Done
    //     data: {
    //         'user id': '0',
    //     }
    // });
    // THIS CAN BE REMOVED (END)
};

exports.createAction = function (options) {
    var action = new Action(options);
    return function (data) {
        return new Promise(function (solve, reject, onCancel) {
            var parameters = (data && data.filters) || {};
            action.run(parameters, solve, onCancel);
        });
    };
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],3:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createActions = function (options) {
    return {
        'send-mail': require('./send-mail').createAction(options),
        'compute-first-chords': require('./compute-first-chords').createAction(options),
        'send-preference': require('./send-preference').createAction(options),
        'get-id': require('./get-id').createAction(options),
    };
};

},{"./compute-first-chords":1,"./get-id":2,"./send-mail":4,"./send-preference":5}],4:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
"use strict";

var Promise = (typeof window !== "undefined" ? window['Promise'] : typeof global !== "undefined" ? global['Promise'] : null);

function Action(options) { // add "options" parameters if needed
    // TODO: Global Initialization
    this.api_caller = options.repositories.api_caller;
}

Action.prototype.run = function (parameters, solve) { // add "onCancel" parameters if needed
    // Parameters:
    // parameters['comments']
    // parameters['mail']
    // parameters['user id']

    // TODO: Execution
    var self = this;
    var data = JSON.stringify({

        comments : parameters['comments'],
        email : parameters['mail'],
        user_id : parameters['user id'],
    });
    self.api_caller.call_api('/api/comment','POST', data).then(function(result){
        // $.notify({message: 'Send Mail'}, {allow_dismiss: true, type: 'success'});
        solve({
            event: 'close-page-event', // Close Page
            data: {
            }
        });
    });
    // THIS CAN BE REMOVED (BEGIN)
    // $.notify({message: 'Send Mail'}, {allow_dismiss: true, type: 'success'});
    // solve({
    //     data: {
    //     }
    // });
    // THIS CAN BE REMOVED (END)
};

exports.createAction = function (options) {
    var action = new Action(options);
    return function (data) {
        return new Promise(function (solve, reject, onCancel) {
            var parameters = (data && data.filters) || {};
            action.run(parameters, solve, onCancel);
        });
    };
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],5:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
/*globals $*/
"use strict";

var Promise = (typeof window !== "undefined" ? window['Promise'] : typeof global !== "undefined" ? global['Promise'] : null);

function Action(options) { // add "options" parameters if needed
    // TODO: Global Initialization
    this.api_caller = options.repositories.api_caller;
}

Action.prototype.run = function (parameters, solve) { // add "onCancel" parameters if needed
    // Parameters:
    // parameters['complexity']
    // parameters['survey answers']
    // parameters['user id']
    // parameters['values history']

    // TODO: Execution
    var self = this;
    var data = JSON.stringify({
        survey_answers : parameters['survey answers'],
        user_id : parameters['user id'],
        values_history : parameters['values history']
    });
    self.api_caller.call_api('/api/result','POST', data).then(function(result){
        // $.notify({message: 'Send Preferences'}, {allow_dismiss: true, type: 'success'});
        solve({
            event: 'final-page-event', // Final Page
            data: {
                'user id': parameters['user id'],
            }
        });
    }).catch(function(result){
        solve({
            event: 'final-page-error-event',
            data: {}
        });
    });
    // THIS CAN BE REMOVED (BEGIN)
    // $.notify({message: 'Send Preferences'}, {allow_dismiss: true, type: 'success'});
    // solve({
    //     event: 'final-page-event', // Final Page
    //     data: {
    //         'user id': parameters['user id'],
    //     }
    // });
    // THIS CAN BE REMOVED (END)
};

exports.createAction = function (options) {
    var action = new Action(options);
    return function (data) {
        return new Promise(function (solve, reject, onCancel) {
            var parameters = (data && data.filters) || {};
            action.run(parameters, solve, onCancel);
        });
    };
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],6:[function(require,module,exports){
module.exports = "<h3>Harmonic Complexity Test</h3>\n\n<p>\n    Thank you for your time, feel free to close this tab.\n</p>";

},{}],7:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null);

function ViewModel(params) {
    var self = this;
    self.context = params.context;

    self.init = function (options) {
        options = options || {};
        self.children.forEach(function (child){
            if (child === options.mask) {
                return;
            }
            self.context.vms[child].init(options);
        });
    };

    self.trigger = function (id) {
        self.context.events[id](self.context);
    };
}

ViewModel.prototype.id = 'closing-page';
ViewModel.prototype.children = [
];

exports.register = function () {
    ko.components.register('c-closing-page', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                var vm = new ViewModel(params);
                params.context.vms[vm.id] = vm;
                params.context.runningActionsByContainer[vm.id] = [];
                ko.utils.domNodeDisposal.addDisposeCallback(componentInfo.element, function () {
                    params.context.runningActionsByContainer[vm.id].forEach(function (promise) {
                        promise.cancel();
                    })
                    delete params.context.runningActionsByContainer[vm.id];
                    delete params.context.vms[vm.id];
                });
                return vm;
            }
        },
        template: require('./index.html'),
        synchronous: true
    });
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./index.html":6}],8:[function(require,module,exports){
module.exports = "<h3>Thank you for your time</h3>\n<p>\n    Add your e-mail address if you want to be informed about the results of this test, otherwise simply close this page.\n    If you have some comments or suggestions, write them in the section below.\n</p>\n<p>\n    Francesco Foscarin\n</p>\n\n<form>\n    <div class=\"form-group\" data-bind=\"css: {'has-error': errors()['email']}\">\n        <label for=\"email-form_field_1\" data-bind=\"css: {active: fields()['email']}, attr: {'data-error': errors()['email']}\" class=\"control-label\">email</label>\n        <input id=\"email-form_field_1\" data-bind=\"value: fields()['email']\" type=\"text\" class=\"form-control validate\" aria-describedby=\"email-form_field_1_error\">\n        <span id=\"email-form_field_1_error\" class=\"help-block\" data-bind=\"text: errors()['email']\"></span>\n    </div>\n\n    <div class=\"form-group\" data-bind=\"css: {'has-error': errors()['comments']}\">\n        <label for=\"email-form_field_0\" data-bind=\"css: {active: fields()['comments']}, attr: {'data-error': errors()['comments']}\" class=\"control-label\">comments</label>\n        <!-- <input id=\"email-form_field_0\" data-bind=\"value: fields()['comments']\" type=\"text\" class=\"form-control validate\" aria-describedby=\"email-form_field_0_error\"> -->\n        <textarea data-bind=\"value: fields()['comments']\" placeholder=\"Comments and suggestions\"></textarea>\n        <span id=\"email-form_field_0_error\" class=\"help-block\" data-bind=\"text: errors()['comments']\"></span>\n    </div>\n\n    <!-- <div class=\"form-group\" data-bind=\"css: {'has-error': errors()['user id']}\">\n        <label for=\"email-form_field_2\" data-bind=\"css: {active: fields()['user id']}, attr: {'data-error': errors()['user id']}\" class=\"control-label\">user id</label>\n        <input id=\"email-form_field_2\" data-bind=\"value: fields()['user id']\" type=\"text\" class=\"form-control validate\" aria-describedby=\"email-form_field_2_error\">\n        <span id=\"email-form_field_2_error\" class=\"help-block\" data-bind=\"text: errors()['user id']\"></span>\n    </div> -->\n</form>\n\n\n<div class=\"row\">\n    <a class=\"col-xs-2 btn btn-primary\" data-bind=\"click: trigger.bind($data, 'send-mail-event')\">Send</a>\n</div>";

},{}],9:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null),
    Promise = (typeof window !== "undefined" ? window['Promise'] : typeof global !== "undefined" ? global['Promise'] : null);

function ViewModel(params) {
    var self = this;
    self.context = params.context;
    self.status = ko.observable('');
    self.fields = ko.observable({});
    self.errors = ko.observable({});

    self.trigger = function (id) {
        self.context.events[id](self.context, self.output);
    };
}

ViewModel.prototype.id = 'email-form';

ViewModel.prototype.waitForStatusChange = function () {
    return this._initializing ||
           Promise.resolve();
};

ViewModel.prototype._compute = function () {
    this.output = {
        'comments': this.input['comments'],
        'email': this.input['email'],
        'user id': this.input['user id'],
    }
    var self = this,
        fields = {
            'comments': ko.observable(this.input['comments']),
            'email': ko.observable(this.input['email']),
            'user id': ko.observable(this.input['user id']),
        },
        errors = {
            'comments': ko.observable(this.input['comments-error']),
            'email': ko.observable(this.input['email-error']),
            'user id': ko.observable(this.input['user id-error']),
        };
    fields['comments'].subscribe(function (value) {
        self.output['comments'] = value;
        self.errors()['comments'](undefined);
    });
    fields['email'].subscribe(function (value) {
        self.output['email'] = value;
        self.errors()['email'](undefined);
    });
    fields['user id'].subscribe(function (value) {
        self.output['user id'] = value;
        self.errors()['user id'](undefined);
    });
    this.fields(fields);
    this.errors(errors);
    this.status('computed');
};


ViewModel.prototype.init = function (options) {
    options = options || {};
    this.output = undefined;
    this.fields({});
    this.errors({});
    this.input = options.input || {};
    this.status('ready');
    var self = this;
    this._initializing = new Promise(function (resolve) {
        setTimeout(function () {
            self._compute();
            resolve();
            self._initializing = undefined;
        }, 1);
    });
};

exports.register = function () {
    ko.components.register('c-email-form', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                var vm = new ViewModel(params);
                params.context.vms[vm.id] = vm;
                ko.utils.domNodeDisposal.addDisposeCallback(componentInfo.element, function () { delete params.context.vms[vm.id]; });
                return vm;
            }
        },
        template: require('./index.html'),
        synchronous: true
    });
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./index.html":8}],10:[function(require,module,exports){
module.exports = "<h3>Error</h3>\n\n<p>\n    Something went wrong with the connection to server.\n    Sorry for the inconvenience, you can try again in some minuts.\n</p>";

},{}],11:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null);

function ViewModel(params) {
    var self = this;
    self.context = params.context;

    self.init = function (options) {
        options = options || {};
        self.children.forEach(function (child){
            if (child === options.mask) {
                return;
            }
            self.context.vms[child].init(options);
        });
    };

    self.trigger = function (id) {
        self.context.events[id](self.context);
    };
}

ViewModel.prototype.id = 'error-view';
ViewModel.prototype.children = [
];

exports.register = function () {
    ko.components.register('c-error-view', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                var vm = new ViewModel(params);
                params.context.vms[vm.id] = vm;
                params.context.runningActionsByContainer[vm.id] = [];
                ko.utils.domNodeDisposal.addDisposeCallback(componentInfo.element, function () {
                    params.context.runningActionsByContainer[vm.id].forEach(function (promise) {
                        promise.cancel();
                    })
                    delete params.context.runningActionsByContainer[vm.id];
                    delete params.context.vms[vm.id];
                });
                return vm;
            }
        },
        template: require('./index.html'),
        synchronous: true
    });
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./index.html":10}],12:[function(require,module,exports){
module.exports = "<span>\n    <!-- Email -->\n    <c-email-form params=\"context: context\"></c-email-form>\n</span>";

},{}],13:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null);

function ViewModel(params) {
    var self = this;
    self.context = params.context;

    self.init = function (options) {
        options = options || {};
        self.children.forEach(function (child){
            if (child === options.mask) {
                return;
            }
            self.context.vms[child].init(options);
        });
    };

    self.trigger = function (id) {
        self.context.events[id](self.context);
    };
}

ViewModel.prototype.id = 'final-page-view';
ViewModel.prototype.children = [
    'email-form' // Email
];

exports.register = function () {
    ko.components.register('c-final-page-view', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                var vm = new ViewModel(params);
                params.context.vms[vm.id] = vm;
                params.context.runningActionsByContainer[vm.id] = [];
                ko.utils.domNodeDisposal.addDisposeCallback(componentInfo.element, function () {
                    params.context.runningActionsByContainer[vm.id].forEach(function (promise) {
                        promise.cancel();
                    })
                    delete params.context.runningActionsByContainer[vm.id];
                    delete params.context.vms[vm.id];
                });
                return vm;
            }
        },
        template: require('./index.html'),
        synchronous: true
    });
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./index.html":12}],14:[function(require,module,exports){
module.exports = "<h3>Harmonic Complexity Test</h3>\n\n<p>\n    This is a test that I will use to extract data on how we perceive the harmonic complexity.\n    You have to complete a short survey and then some tests. All the instructions will be provided.\n</p>\n<p>\n    The site is not optimized for mobile phones, so use your laptop for a better user experience.\n    Prepare your <b>headphones</b> and when you are ready click on the button below.\n</p>\n\n\n<div class=\"row\" id=\"button-row-home\">\n    <a class=\"col-xs-2 btn btn-primary pull-right\" data-bind=\"click: trigger.bind($data,'start-survey-event')\">Start Survey</a>\n</div>";

},{}],15:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null);

function ViewModel(params) {
    var self = this;
    self.context = params.context;

    self.init = function (options) {
        options = options || {};
        self.children.forEach(function (child){
            if (child === options.mask) {
                return;
            }
            self.context.vms[child].init(options);
        });
    };

    self.trigger = function (id) {
        self.context.events[id](self.context);
    };
}

ViewModel.prototype.id = 'home-page';
ViewModel.prototype.children = [
];

exports.register = function () {
    ko.components.register('c-home-page', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                var vm = new ViewModel(params);
                params.context.vms[vm.id] = vm;
                params.context.runningActionsByContainer[vm.id] = [];
                ko.utils.domNodeDisposal.addDisposeCallback(componentInfo.element, function () {
                    params.context.runningActionsByContainer[vm.id].forEach(function (promise) {
                        promise.cancel();
                    })
                    delete params.context.runningActionsByContainer[vm.id];
                    delete params.context.vms[vm.id];
                });
                return vm;
            }
        },
        template: require('./index.html'),
        synchronous: true
    });
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./index.html":14}],16:[function(require,module,exports){
module.exports = "<div data-bind=\"visible: !surveyCompleted()\">\n    <h3>Survey</h3>\n    <survey params=\"survey: model\"></survey>\n</div>\n\n\n<div class=\"row\" data-bind=\"visible: surveyCompleted\">\n<!-- <div class=\"row\"> -->\n    <p>\n        You have completed the Survey. Click on the button to begin the test.\n    </p>\n    <a class=\"col-xs-2 btn btn-primary pull-right\" data-bind=\"click: trigger.bind($data, 'send-survey-event')\">Begin Test</a>\n</div>";

},{}],17:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
/*globals Survey*/
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null),
    Promise = (typeof window !== "undefined" ? window['Promise'] : typeof global !== "undefined" ? global['Promise'] : null);

function ViewModel(params) {
    var self = this;
    self.context = params.context;
    self.status = ko.observable('');
    self.fields = ko.observable({});
    self.errors = ko.observable({});
    self.surveyCompleted = ko.observable(false);
    self.surveyJSON = {
        pages:[{elements:[{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"1",title:"I spend a lot of my free time doing music-related activities."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"2",title:"I sometimes choose music that can trigger shivers down my spine."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"3",title:"I enjoy writing about music, for example on blogs and forums."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"4",title:"If somebody starts singing a song I don’t know, I can usually join in."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"5",title:"I am able to judge whether someone is a good singer or not."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"6",title:"I usually know when I’m hearing a song for the first time."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"7",title:"I can sing or play music from memory."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"8",title:"I’m intrigued by musical styles I’m not familiar with and want to find out more."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"9",title:"Pieces of music rarely evoke emotions for me."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"10",title:"I am able to hit the right notes when I sing along with a recording."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"11",title:"I find it difficult to spot mistakes in a performance of a song even if I know the tune."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"12",title:"I can compare and discuss differences between two performances or versions of the same piece of music."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"13",title:"I have trouble recognizing a familiar song when played in a different way or by a different performer."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"14",title:"I have never been complimented for my talents as a musical performer."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"15",title:"I often read or search the internet for things related to music."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"16",title:"I often pick certain music to motivate or excite me."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"17",title:"I am not able to sing in harmony when somebody is singing a familiar tune."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"18",title:"I can tell when people sing or play out of time with the beat."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"19",title:"I am able to identify what is special about a given musical piece."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"20",title:"I am able to talk about the emotions that a piece of music evokes for me."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"21",title:"I don’t spend much of my disposable income on music."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"22",title:"I can tell when people sing or play out of tune."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"23",title:"When I sing, I have no idea whether I’m in tune or not."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"24",title:"Music is kind of an addiction for me - I couldn’t live without it."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"25",title:"I don’t like singing in public because I’m afraid that I would sing wrong notes."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"26",title:"When I hear a piece of music I can usually identify its genre."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"27",title:"I would not consider myself a musi- cian."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"28",title:"I keep track of new music that I come across (e.g. new artists or recordings)."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"29",title:"After hearing a new song two or three times, I can usually sing it by myself."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"30",title:"I only need to hear a new tune once and I can sing it back hours later."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"31",title:"Music can evoke my memories of past people and places."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"1"},{value:"3",text:"2"},{value:"4",text:"3"},{value:"5",text:"4 - 5"},{value:"6",text:"6 - 9"},{value:"7",text:"10 or more"}],isRequired:true,name:"32",title:"I engaged in regular, daily practice of a musical instrument (including voice) for ___ years."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"0.5"},{value:"3",text:"1"},{value:"4",text:"1.5"},{value:"5",text:"2"},{value:"6",text:"3 - 4"},{value:"7",text:"5 or more"}],isRequired:true,name:"33",title:"At the peak of my interest, I practiced ___ hours per day on my primary instrument."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"1"},{value:"3",text:"2"},{value:"4",text:"3"},{value:"5",text:"4 - 6"},{value:"6",text:"7 - 10"},{value:"7",text:"11 or more"}],isRequired:true,name:"34",title:"I have attended ___ live music events as an audience member in the past twelve months."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"0.5"},{value:"3",text:"1"},{value:"4",text:"2"},{value:"5",text:"3"},{value:"6",text:"4 - 6"},{value:"7",text:"7 or more"}],isRequired:true,name:"35",title:"I have had formal training in music theory for ___ years."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"0.5"},{value:"3",text:"1"},{value:"4",text:"2"},{value:"5",text:"3 -5"},{value:"6",text:"6 - 9 "},{value:"7",text:"10 or more"}],isRequired:true,name:"36",title:"I have had ___ years of formal training on a musical instrument (including voice) during my lifetime."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"1"},{value:"3",text:"2"},{value:"4",text:"3"},{value:"5",text:"4"},{value:"6",text:"5"},{value:"7",text:"6 or more"}],isRequired:true,name:"37",title:"I can play ___ musical instruments."},{type:"radiogroup",choices:[{value:"1",text:"0 - 15 min"},{value:"2",text:"15 - 30 min"},{value:"3",text:"30 - 60 min"},{value:"4",text:"60 - 90 min"},{value:"5",text:"2 hrs"},{value:"6",text:"2 - 3 hrs"},{value:"7",text:"4 hrs or more"}],isRequired:true,name:"38",title:"I listen attentively to music for ___ per day."},{type:"radiogroup",choices:[{value:"1",text:"Pop/Rock Music"},{value:"2",text:"Jazz Music"},{value:"3",text:"Classical Music"},{value:"4",text:"Everything"}],isRequired:true,name:"I usually listen to ___"}],name:"page1"}]
    };

    Survey.defaultBootstrapCss.navigationButton = 'btn btn-primary';
    Survey.Survey.cssType = 'bootstrap';
    self.model = new Survey.Model(self.surveyJSON);
    self.saveSurveyResults = function (survey){
        // var resultAsString = JSON.stringify(survey.data);
        // alert(resultAsString); //send Ajax request to your web server.
        self.fields()['survey answers'](survey.data);
        self.surveyCompleted(true);
    };
    //Use onComplete event to save the data
    self.model.onComplete.add(self.saveSurveyResults);

    self.trigger = function (id) {
        self.context.events[id](self.context, self.output);
    };
}

ViewModel.prototype.id = 'survey-form';

ViewModel.prototype.waitForStatusChange = function () {
    return this._initializing ||
           Promise.resolve();
};

ViewModel.prototype._compute = function () {
    this.output = {
        'survey answers': this.input['survey answers'],
        'user id': this.input['user id'],
    }
    var self = this,
        fields = {
            'survey answers': ko.observable(this.input['survey answers']),
            'user id': ko.observable(this.input['user id']),
        },
        errors = {
            'survey answers': ko.observable(this.input['survey answers-error']),
            'user id': ko.observable(this.input['user id-error']),
        };
    fields['survey answers'].subscribe(function (value) {
        self.output['survey answers'] = value;
        self.errors()['survey answers'](undefined);
    });
    fields['user id'].subscribe(function (value) {
        self.output['user id'] = value;
        self.errors()['user id'](undefined);
    });
    this.fields(fields);
    this.errors(errors);
    this.status('computed');
};


ViewModel.prototype.init = function (options) {
    options = options || {};
    this.output = undefined;
    this.fields({});
    this.errors({});
    this.input = options.input || {};
    this.status('ready');
    var self = this;
    this._initializing = new Promise(function (resolve) {
        setTimeout(function () {
            self._compute();
            resolve();
            self._initializing = undefined;
        }, 1);
    });
};

exports.register = function () {
    ko.components.register('c-survey-form', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                var vm = new ViewModel(params);
                params.context.vms[vm.id] = vm;
                ko.utils.domNodeDisposal.addDisposeCallback(componentInfo.element, function () { delete params.context.vms[vm.id]; });
                return vm;
            }
        },
        template: require('./index.html'),
        synchronous: true
    });
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./index.html":16}],18:[function(require,module,exports){
module.exports = "<span>\n    <!-- Survey -->\n    <c-survey-form params=\"context: context\"></c-survey-form>\n</span>";

},{}],19:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null);

function ViewModel(params) {
    var self = this;
    self.context = params.context;

    self.init = function (options) {
        options = options || {};
        self.children.forEach(function (child){
            if (child === options.mask) {
                return;
            }
            self.context.vms[child].init(options);
        });
    };

    self.trigger = function (id) {
        self.context.events[id](self.context);
    };
}

ViewModel.prototype.id = 'survey-view';
ViewModel.prototype.children = [
    'survey-form' // Survey
];

exports.register = function () {
    ko.components.register('c-survey-view', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                var vm = new ViewModel(params);
                params.context.vms[vm.id] = vm;
                params.context.runningActionsByContainer[vm.id] = [];
                ko.utils.domNodeDisposal.addDisposeCallback(componentInfo.element, function () {
                    params.context.runningActionsByContainer[vm.id].forEach(function (promise) {
                        promise.cancel();
                    })
                    delete params.context.runningActionsByContainer[vm.id];
                    delete params.context.vms[vm.id];
                });
                return vm;
            }
        },
        template: require('./index.html'),
        synchronous: true
    });
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./index.html":18}],20:[function(require,module,exports){
module.exports = "<div id=\"loading-overlay\" data-bind=\"visible: loading\">loading...</div>\n\n<div data-bind=\"visible: ready\">\n\n    <div data-bind=\"progress: { value: percentageValue, type: 'info', text: 'Complete', textHidden: false, animated: true, striped: true }\"></div>\n\n    <h3>Choose your favourite complexity value! (Test n. <span data-bind=\"text: testNumber\"></span> ) </h3>\n    <p>\n        Every position of the slider is associated with sequences of chords that you will find less or more complex. Press the \"play\" button and move the slider below until you find the set of chord progressions that you like most.\n        Then click the button \"Next Test\" and wait for the new test to load.\n    </p>\n    <p>\n        When you move the slider the progression will stop and you need to press \"play\" to listen to the next progression.\n    </p>\n\n    <div class=\"col-xs-12\" style=\"height:50px\"></div>\n\n    <div class=\"row\">\n        <div id=\"icon1\">\n            <div class=\"icon1\" data-bind=\"visible: !isPlaying()\">\n            <!-- <a id=\"play-icon\" class=\"glyphicon glyphicon-play\" data-bind=\"click: play\">Play</a> -->\n                <!-- <a type=\"button\" class=\"btn btn-default btn-circle btn-lg\" data-bind=\"click: play, enable: playButtonEnabled\"><i class=\"fa fa-play fa-4x\"></i></a> -->\n                <button class=\"btn btn-md btn-default\" data-bind=\"click: play, enable: playButtonEnabled\"><i class=\"fa fa-play fa-3x\"></i></button>\n\n            </div>\n            <div class=\"icon1\" data-bind=\"visible: isPlaying()\">\n            <!-- <a class=\"glyphicon glyphicon-pause\" data-bind=\"click: pause\">Pause</a> -->\n                <!-- <button data-bind=\"click: pause\"><i class=\"fa fa-pause-circle fa-4x\" aria-hidden=\"true\"></i></a> -->\n                <button class=\"btn btn-md btn-default\" data-bind=\"click: pause\"><i class=\"fa fa-pause fa-3x\"></i></button>\n            </div>\n        </div>\n    </div>\n\n    <div class=\"row\">\n        <div id=\"test-row\">\n            <b>simple</b>\n            <input data-slider-id=\"slider1\" data-slider-tooltip=\"hide\" type=\"text\" data-slider-min=\"0\" data-slider-max=\"29\" data-slider-step=\"1\" data-slider-value=\"0\" data-bind=\"sliderValue:{value: sliderValue}\">\n            <b>complex</b>\n        </div>\n    </div>\n\n    <div class=\"col-xs-12\" style=\"height:50px\"></div>\n\n    <div class=\"stepwizard\">\n        <div class=\"stepwizard-row\">\n            <div class=\"stepwizard-step\">\n                <a type=\"button\" class=\"btn btn-default btn-circle\" data-bind=\"css: button0Status\" disabled=\"disabled\">1</a>\n                <!-- <p></p> -->\n            </div>\n            <div class=\"stepwizard-step\">\n                <a type=\"button\" class=\"btn btn-default btn-circle\" data-bind=\"css: button1Status\" disabled=\"disabled\">2</a>\n                <p></p>\n            </div>\n            <div class=\"stepwizard-step\">\n                <a type=\"button\" class=\"btn btn-default btn-circle\" data-bind=\"css:button2Status\" disabled=\"disabled\">3</a>\n                <p></p>\n            </div>\n            <div class=\"stepwizard-step\">\n                <a type=\"button\" class=\"btn btn-default btn-circle\" data-bind=\"css: button3Status\" disabled=\"disabled\">4</a>\n                <p></p>\n            </div>\n            <div class=\"stepwizard-step\">\n                <a type=\"button\" class=\"btn btn-default btn-circle\" data-bind=\"css: button4Status\" disabled=\"disabled\">5</a>\n                <p></p>\n            </div>\n        </div>\n    </div>\n\n\n    <!-- <div class =\"row\">\n        <div data-bind=\"visible: !isPlaying()\">\n            <!-- <a id=\"play-icon\" class=\"glyphicon glyphicon-play\" data-bind=\"click: play\">Play</a>\n            <a data-bind=\"click: play\"><i class=\"fa fa-play-circle fa-4x\" aria-hidden=\"true\"></i></a>\n        </div>\n        <div data-bind=\"visible: isPlaying()\">\n            <!-- <a class=\"glyphicon glyphicon-pause\" data-bind=\"click: pause\">Pause</a>\n            <a data-bind=\"click: play\"><i class=\"fa fa-pause-circle fa-4x\" aria-hidden=\"true\"></i></a>\n        </div>\n    </div> -->\n\n    <div class=\"row\" id=\"button-row\">\n        <div data-bind=\"visible: isLastTest()\">\n            <button class=\"col-xs-2 btn btn-primary pull-right\" data-bind=\"click: sendPreference, disable : disableNextButton\">Send Preference</button>\n        </div>\n        <div data-bind=\"visible: !isLastTest()\">\n            <button class=\"col-xs-2 btn btn-primary pull-right\" data-bind=\"click: nextTest, disable : disableNextButton\">Next Test</button>\n        </div>\n    </div>\n\n\n</div>";

},{}],21:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
/*jshint sub:true*/
/*globals AudioContext*/
/*globals Soundfont*/
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null),
    Promise = (typeof window !== "undefined" ? window['Promise'] : typeof global !== "undefined" ? global['Promise'] : null);

function ViewModel(params) {
    var self = this;
    self.context = params.context;
    self.status = ko.observable('');
    self.fields = ko.observable({});
    self.errors = ko.observable({});
    self.piano = undefined;
    self.loading = ko.observable(true);  //used to display "Loading" text
    self.ready = ko.observable(false); //used to display the page when the samples are loaded
    self.ac = new AudioContext();
    self.sliderValue = ko.observable(0);
    self.isPlaying =  ko.observable(false); //used to stop the infinite playing when we want to left the page
    self.playButtonEnabled = ko.observable(true); //used to avoid rapid double click on play
    self.isLastTest = ko.observable(false);
    self.percentageValue = ko.observable(0);
    self.testNumber = ko.observable(1);
    self.isSync = ko.observable(true);
    self.isChordPlayed = [ko.observable(false),ko.observable(false),ko.observable(false),ko.observable(false),ko.observable(false)];
    self.disableNextButton = ko.observable(true);
    self.button0Status = ko.pureComputed(function() {
        if (self.isChordPlayed[0]() == false){
            return 'btn-default'
        }
        else if (self.isSync()) return 'btn-success'
        else return 'btn-warning';

    });
    self.button1Status = ko.pureComputed(function() {
        if (self.isChordPlayed[1]() == false){
            return 'btn-default'
        }
        else if (self.isSync()) return 'btn-success';
        else return 'btn-warning';
    });
    self.button2Status = ko.pureComputed(function() {
        if (self.isChordPlayed[2]() == false){
            return 'btn-default';
        }
        else if (self.isSync()) return 'btn-success';
        else return 'btn-warning';
    });
    self.button3Status = ko.pureComputed(function() {
        if (self.isChordPlayed[3]() == false){
            return 'btn-default';
        }
        else if (self.isSync()) return 'btn-success';
        else return 'btn-warning';
    });
    self.button4Status = ko.pureComputed(function() {
        if (self.isChordPlayed[4]() == false){
            return 'btn-default';
        }
        else if (self.isSync()) return 'btn-success';
        else return 'btn-warning';
    });

    self.trigger = function (id) {
        self.context.events[id](self.context, self.output);
    };

    self.updateIsLastTest = function(){
        self.percentageValue(Math.round(self.fields()['test number']() *100/8));
        if (self.fields()['test number']() == 7){
            self.isLastTest(true);
            self.testNumber(self.fields()['test number']() +1);
        }
        else{
            self.isLastTest(false);
            self.testNumber(self.fields()['test number']() +1);
        }

    };

    self.loadSamples = function() {
        Soundfont.instrument(self.ac, 'acoustic_grand_piano').then(function (piano) {
            self.piano = piano;
            self.loading(false);
            self.ready(true);
            // self.playChords();
        });
    };
    self.loadSamples();  //load the samples when the page is loading

    //simply play the input array
    self.playNotes = function(noteArray){
        for (var n in noteArray){
            self.piano.play(noteArray[n], self.ac.currentTime, { duration: 1});
        }
    };

    //first call to the recursive function to play fields()['chords']()
    self.playChords = function(){
        self.disableNextButton(false); //enable to change test
        //reset the colors
        for (var ii in self.isChordPlayed){
            self.isChordPlayed[ii](false);
        }
        var index = 0;
        self.isSync(true);
        self.fields()['complexity'](self.sliderValue() || 0);
        var progressions = self.fields()['progressions']()[self.fields()['complexity']()];
        var random_index = Math.floor(Math.random() * (progressions.length-1));
        var chords = progressions[random_index].chords;
        self.recursivePlay(index,chords);
    };
    //recursively play chords in order to wait 1 second between chords in non-blocking way
    self.recursivePlay = function(index, chords){
        //play the first 4 chords
        if (index < Object.keys(chords).length -1 && self.isPlaying()){
            self.isChordPlayed[index](true);
            self.playNotes(chords[index]);
            var newIndex = index +1;
            if (self.isPlaying()){ //add a control to avoid play-click during the wait second to restart the progression
                setTimeout(function() {
                    self.recursivePlay(newIndex,chords);
                }, 1000);
            }
        }
        else{ //play the last chord
            if (self.isPlaying()){
                self.isChordPlayed[index](true);
                self.playNotes(chords[index]);
                //save the complexity value
                self.fields()['values history']()[self.fields()['test number']()].push(self.fields()['complexity']());
                if (self.isPlaying()){ //add a control to avoid play-click during the wait second to restart the progression
                    //wait 1 s for the chord and play the new progression after another 1 second
                    setTimeout(function() {
                        //reset the colors
                        for (var ii in self.isChordPlayed){
                            self.isChordPlayed[ii](false);
                        }
                        if (self.isPlaying()){ //add a control to avoid play-click during the wait second to restart the progression
                            setTimeout(function() {
                                self.playChords();
                            }, 1000);
                        }
                    }, 1000);
                }
            }
        }
    };

    //stop the infinite sound and go to comments
    self.sendPreference = function(){
        self.disableNextButton(true); //disable  to avoid double clicks
        //store the current complexity
        self.fields()['values history']()[self.fields()['test number']()].push(self.fields()['complexity']());
        self.isPlaying(false);
        self.trigger('send-preference-event');
    };

    //stop the infinite sound and go to next test
    self.nextTest = function(){
        self.disableNextButton(true); //disable  to avoid double clicks
        //reset the colors
        for (var ii in self.isChordPlayed){
            self.isChordPlayed[ii](false);
        }
        //store the current complexity
        self.fields()['values history']()[self.fields()['test number']()].push(self.fields()['complexity']());
        self.isPlaying(false);
        self.fields()['complexity'](0);
        self.sliderValue(0);
        self.trigger('next-test-event');
    };

    self.play =function(){
        if(!self.isPlaying()){
            self.isPlaying(true);
            self.playChords();
        }

    };

    self.pause = function(){
        self.playButtonEnabled(false);
        self.isPlaying(false);
        setTimeout(function() {
            self.playButtonEnabled(true);
        }, 1010);
    };


    self.sliderValue.subscribe(function (value) {
        self.playButtonEnabled(false); //to avoid fast double click before the progresion stop
        self.isPlaying(false);
        self.isSync(false);
        setTimeout(function() {
            self.playButtonEnabled(true);
        }, 1010);
    });

}

ViewModel.prototype.id = 'test-form';

ViewModel.prototype.waitForStatusChange = function () {
    return this._initializing ||
           Promise.resolve();
};

ViewModel.prototype._compute = function () {
    this.output = {
        'progressions': this.input['progressions'],
        'complexity': this.input['complexity'],
        'survey answers': this.input['survey answers'],
        'user id': this.input['user id'],
        'values history': this.input['values history'],
        'test number' : this.input['test number'],
    };
    var self = this,
        fields = {
            'progressions': ko.observable(this.input['progressions']),
            'complexity': ko.observable(this.input['complexity']),
            'survey answers': ko.observable(this.input['survey answers']),
            'user id': ko.observable(this.input['user id']),
            'values history': ko.observable(this.input['values history']),
            'test number' : ko.observable(this.input['test number']),
        },
        errors = {
            'progressions': ko.observable(this.input['progressions-error']),
            'complexity': ko.observable(this.input['complexity-error']),
            'survey answers': ko.observable(this.input['survey answers-error']),
            'user id': ko.observable(this.input['user id-error']),
            'values history': ko.observable(this.input['values history-error']),
            'test number' : ko.observable(this.input['test number-error'])
        };
    fields['progressions'].subscribe(function (value) {
        self.output['chords'] = value;
        self.errors()['chords'](undefined);
    });
    fields['complexity'].subscribe(function (value) {
        self.output['complexity'] = value;
        self.errors()['complexity'](undefined);
    });
    fields['survey answers'].subscribe(function (value) {
        self.output['survey answers'] = value;
        self.errors()['survey answers'](undefined);
    });
    fields['user id'].subscribe(function (value) {
        self.output['user id'] = value;
        self.errors()['user id'](undefined);
    });
    fields['values history'].subscribe(function (value) {
        self.output['values history'] = value;
        self.errors()['values history'](undefined);
    });
    fields['test number'].subscribe(function (value) {
        self.output['test number'] = value;
        self.errors()['test number'](undefined);
    });
    this.fields(fields);
    self.updateIsLastTest();
    this.errors(errors);
    this.status('computed');
};


ViewModel.prototype.init = function (options) {
    options = options || {};
    this.output = undefined;
    this.fields({});
    this.errors({});
    this.input = options.input || {};
    this.status('ready');
    var self = this;
    this._initializing = new Promise(function (resolve) {
        setTimeout(function () {
            self._compute();
            resolve();
            self._initializing = undefined;
        }, 1);
    });
};

exports.register = function () {
    ko.components.register('c-test-form', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                var vm = new ViewModel(params);
                params.context.vms[vm.id] = vm;
                ko.utils.domNodeDisposal.addDisposeCallback(componentInfo.element, function () { delete params.context.vms[vm.id]; });
                return vm;
            }
        },
        template: require('./index.html'),
        synchronous: true
    });
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./index.html":20}],22:[function(require,module,exports){
module.exports = "<span>\n    <!-- Test Form -->\n    <c-test-form params=\"context: context\"></c-test-form>\n</span>";

},{}],23:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null);

function ViewModel(params) {
    var self = this;
    self.context = params.context;

    self.init = function (options) {
        options = options || {};
        self.children.forEach(function (child){
            if (child === options.mask) {
                return;
            }
            self.context.vms[child].init(options);
        });
    };

    self.trigger = function (id) {
        self.context.events[id](self.context);
    };
}

ViewModel.prototype.id = 'test-view';
ViewModel.prototype.children = [
    'test-form' // Test Form
];

exports.register = function () {
    ko.components.register('c-test-view', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                var vm = new ViewModel(params);
                params.context.vms[vm.id] = vm;
                params.context.runningActionsByContainer[vm.id] = [];
                ko.utils.domNodeDisposal.addDisposeCallback(componentInfo.element, function () {
                    params.context.runningActionsByContainer[vm.id].forEach(function (promise) {
                        promise.cancel();
                    })
                    delete params.context.runningActionsByContainer[vm.id];
                    delete params.context.vms[vm.id];
                });
                return vm;
            }
        },
        template: require('./index.html'),
        synchronous: true
    });
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./index.html":22}],24:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null);

exports.register = function () {
    require('./main-application').register();
    require('./c-final-page-view').register();
    require('./c-home-page').register();
    require('./c-survey-view').register();
    require('./c-test-view').register();
    require('./c-survey-form').register();
    require('./c-test-form').register();
    require('./c-email-form').register();
    require('./c-closing-page').register();
    require('./c-error-view').register();
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./c-closing-page":7,"./c-email-form":9,"./c-error-view":11,"./c-final-page-view":13,"./c-home-page":15,"./c-survey-form":17,"./c-survey-view":19,"./c-test-form":21,"./c-test-view":23,"./main-application":26}],25:[function(require,module,exports){
module.exports = "<nav class=\"navbar navbar-default\">\n    <div class=\"container\">\n        <div class=\"navbar-header\">\n            <button class=\"navbar-toggle collapsed\" type=\"button\" data-toggle=\"collapse\" data-target=\"#landmark-menu\" aria-expanded=\"false\">\n                <span class=\"sr-only\">Toggle navigation</span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n                <span class=\"icon-bar\"></span>\n            </button>\n        </div>\n        <div id=\"landmark-menu\" class=\"collapse navbar-collapse\">\n            <ul class=\"nav navbar-nav\">\n            </ul>\n        </div>\n    </div>\n</nav>\n<div class=\"container\">\n    <div class=\"row\">\n        <span data-bind=\"if: active() === 'final-page-view'\">\n            <c-final-page-view params=\"context: context\" class=\"container\"></c-final-page-view>\n        </span>\n        <span data-bind=\"if: active() === 'home-page'\">\n            <c-home-page params=\"context: context\" class=\"container\"></c-home-page>\n        </span>\n        <span data-bind=\"if: active() === 'survey-view'\">\n            <c-survey-view params=\"context: context\" class=\"container\"></c-survey-view>\n        </span>\n        <span data-bind=\"if: active() === 'test-view'\">\n            <c-test-view params=\"context: context\" class=\"container\"></c-test-view>\n        </span>\n        <span data-bind=\"if: active() === 'closing-page'\">\n            <c-closing-page params=\"context: context\" class=\"container\"></c-closing-page>\n        </span>\n        <span data-bind=\"if: active() === 'error-view'\">\n            <c-error-view params=\"context: context\" class=\"container\"></c-error-view>\n        </span>\n    </div>\n</div>";

},{}],26:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null),
    $ = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null);

exports.register = function () {
    ko.components.register('main-application', {
        viewModel: function(params) {
            var self = this,
                defaultChild = 'home-page';
            self.context = params.context;
            self.active = ko.observable(undefined);

            self.landmark = function (id) {
                self.active(id);
                self.context.vms[id].init();
            };
            self.init = function () {
                self.active(defaultChild);
                if (self.context.vms[defaultChild]) {
                    self.context.vms[defaultChild].init();
                }
            };

            self.context.top = self;
        },
        template: require('./index.html'),
        synchronous: true
    });
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./index.html":25}],27:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context) {
        if (!context.vms['error-view']) {
            context.top.active('error-view');
        }
        context.vms['error-view'].init();
    };
};

},{}],28:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        if (!context.vms['test-view']) {
            context.top.active('test-view');
            context.vms['test-view'].init({mask: 'test-form'});
        }
        data = data || {};
        var packet = {
            'user id' : data['user id'],
            'progressions' : data['progressions'],
            'survey answers' : data['survey answers'],
            'values history' : data['values history'],
            'test number' : data['test number']
        };
        context.vms['test-form'].init({input: packet});
    };
};

},{}],29:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context) {
        if (!context.vms['closing-page']) {
            context.top.active('closing-page');
        }
        context.vms['closing-page'].init();
    };
};

},{}],30:[function(require,module,exports){
arguments[4][27][0].apply(exports,arguments)
},{"dup":27}],31:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        if (!context.vms['final-page-view']) {
            context.top.active('final-page-view');
            context.vms['final-page-view'].init({mask: 'email-form'});
        }
        data = data || {};
        var packet = {
            'user id' : data['user id']
        };
        context.vms['email-form'].init({input: packet});
    };
};

},{}],32:[function(require,module,exports){
arguments[4][27][0].apply(exports,arguments)
},{"dup":27}],33:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        if (!context.vms['survey-view']) {
            context.top.active('survey-view');
            context.vms['survey-view'].init({mask: 'survey-form'});
        }
        data = data || {};
        var packet = {
            'user id' : data['user id']
        };
        context.vms['survey-form'].init({input: packet});
    };
};

},{}],34:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createEvents = function (options) {
    return {
        'start-survey-event': require('./start-survey-event').createEvent(options)
        ,'send-survey-event': require('./send-survey-event').createEvent(options)
        ,'begin-test-event': require('./begin-test-event').createEvent(options)
        ,'final-page-event': require('./final-page-event').createEvent(options)
        ,'have-id-event': require('./have-id-event').createEvent(options)
        ,'send-preference-event': require('./send-preference-event').createEvent(options)
        ,'send-mail-event': require('./send-mail-event').createEvent(options)
        ,'next-test-event' : require('./next-test-event').createEvent(options)
        ,'close-page-event': require('./close-page-event').createEvent(options)
        ,'have-id-error-event': require('./have-id-error-event').createEvent(options)
        ,'begin-test-error-event': require('./begin-test-error-event').createEvent(options)
        ,'final-page-error-event': require('./final-page-error-event').createEvent(options)
    };
};

},{"./begin-test-error-event":27,"./begin-test-event":28,"./close-page-event":29,"./final-page-error-event":30,"./final-page-event":31,"./have-id-error-event":32,"./have-id-event":33,"./next-test-event":35,"./send-mail-event":36,"./send-preference-event":37,"./send-survey-event":38,"./start-survey-event":39}],35:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        data = data || {};
        var packet = {
            'survey answers' : data['survey answers']
            ,'user id' : data['user id']
            ,'values history' : data['values history']
            ,'test number' : data['test number'] +1
        };
        var promise = context.actions['compute-first-chords']({filters: packet});
        context.runningActionsByContainer['test-view'].push(promise);
        promise.then(function (result) {
            context.runningActionsByContainer['test-view'].splice(
                context.runningActionsByContainer['test-view'].indexOf(promise), 1
            );
            if (result.event) {
                context.events[result.event](context, result.data);
            }
        });
    };
};

},{}],36:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        data = data || {};
        var packet = {
            'mail' : data['email']
            ,'user id' : data['user id']
            ,'comments' : data['comments']
        };
        var promise = context.actions['send-mail']({filters: packet});
        context.runningActionsByContainer['final-page-view'].push(promise);
        promise.then(function (result) {
            context.runningActionsByContainer['final-page-view'].splice(
                context.runningActionsByContainer['final-page-view'].indexOf(promise), 1
            );
            if (result.event) {
                context.events[result.event](context, result.data);
            }
        });
    };
};

},{}],37:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        data = data || {};
        var packet = {
            'user id' : data['user id']
            ,'values history' : data['values history']
            ,'complexity' : data['complexity']
            ,'survey answers' : data['survey answers']
        };
        var promise = context.actions['send-preference']({filters: packet});
        context.runningActionsByContainer['test-view'].push(promise);
        promise.then(function (result) {
            context.runningActionsByContainer['test-view'].splice(
                context.runningActionsByContainer['test-view'].indexOf(promise), 1
            );
            if (result.event) {
                context.events[result.event](context, result.data);
            }
        });
    };
};

},{}],38:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        data = data || {};
        var packet = {
            'test number' : 0,
            'user id' : data['user id'],
            'survey answers' : data['survey answers']
        };
        var promise = context.actions['compute-first-chords']({filters: packet});
        context.runningActionsByContainer['survey-view'].push(promise);
        promise.then(function (result) {
            context.runningActionsByContainer['survey-view'].splice(
                context.runningActionsByContainer['survey-view'].indexOf(promise), 1
            );
            if (result.event) {
                context.events[result.event](context, result.data);
            }
        });
    };
};

},{}],39:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context) {
        var promise = context.actions['get-id']();
        context.runningActionsByContainer['home-page'].push(promise);
        promise.then(function (result) {
            context.runningActionsByContainer['home-page'].splice(
                context.runningActionsByContainer['home-page'].indexOf(promise), 1
            );
            if (result.event) {
                context.events[result.event](context, result.data);
            }
        });
    };
};

},{}],40:[function(require,module,exports){
(function (global){
/*jslint node: true, nomen: true */
"use strict";

var ko = (typeof window !== "undefined" ? window['ko'] : typeof global !== "undefined" ? global['ko'] : null),
    repositories = require('./repositories'),
    controls = require('./controls'),
    events = require('./events'),
    actions = require('./actions'),
    Promise = (typeof window !== "undefined" ? window['Promise'] : typeof global !== "undefined" ? global['Promise'] : null);

Promise.config({cancellation: true});

controls.register();
// TODO: register any custom control

function ApplicationViewModel() {
    // TODO: initialize global state
    var repos = repositories.createRepositories({});
    this.context = {
        repositories: repos,
        events: events.createEvents({}),
        actions: actions.createActions({repositories: repos}),
        vms: {},
        runningActionsByContainer: {}
    };
}

var application = new ApplicationViewModel();

ko.applyBindings(application);

application.context.top.init();

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./actions":3,"./controls":24,"./events":34,"./repositories":42}],41:[function(require,module,exports){
(function (global){
/*jslint node:true, nomen: true */
"use strict";

var $ = (typeof window !== "undefined" ? window['$'] : typeof global !== "undefined" ? global['$'] : null),
Promise = (typeof window !== "undefined" ? window['Promise'] : typeof global !== "undefined" ? global['Promise'] : null);


function API_caller() {
	if (!(this instanceof API_caller)) {
		return new API_caller();
	}
	this.server_url = 'https://harmoniccomplexity.herokuapp.com';
}

API_caller.prototype.call_api = function(url_end, call_type, call_data) {
	var self = this;
	return new Promise(function(resolve, reject) {
		$.ajax({
			url: self.server_url + url_end,
			type: call_type,
			headers: {
				'Content-Type': 'application/json; charset=utf-8'
			},
			contentType: "application/json",
			data: call_data
		}).done(function(result) {
			resolve(result);
		}).fail(function(jqXHR, textStatus, errorThrown) {
			var error = new Error(errorThrown);
			error.textStatus = textStatus;
			error.jqXHR = jqXHR;
			reject(error);
		});
	});
};

exports.API_caller = API_caller;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],42:[function(require,module,exports){
/*jslint node: true, nomen: true */
"use strict";

exports.createRepositories = function (options) {
    var repositories = {}
    repositories['api_caller'] = require('./api_caller').API_caller(options)
    return repositories;
};

},{"./api_caller":41}]},{},[40])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvanMvYWN0aW9ucy9jb21wdXRlLWZpcnN0LWNob3Jkcy5qcyIsInNyYy9qcy9hY3Rpb25zL2dldC1pZC5qcyIsInNyYy9qcy9hY3Rpb25zL2luZGV4LmpzIiwic3JjL2pzL2FjdGlvbnMvc2VuZC1tYWlsLmpzIiwic3JjL2pzL2FjdGlvbnMvc2VuZC1wcmVmZXJlbmNlLmpzIiwic3JjL2pzL2NvbnRyb2xzL2MtY2xvc2luZy1wYWdlL2luZGV4Lmh0bWwiLCJzcmMvanMvY29udHJvbHMvYy1jbG9zaW5nLXBhZ2UvaW5kZXguanMiLCJzcmMvanMvY29udHJvbHMvYy1lbWFpbC1mb3JtL2luZGV4Lmh0bWwiLCJzcmMvanMvY29udHJvbHMvYy1lbWFpbC1mb3JtL2luZGV4LmpzIiwic3JjL2pzL2NvbnRyb2xzL2MtZXJyb3Itdmlldy9pbmRleC5odG1sIiwic3JjL2pzL2NvbnRyb2xzL2MtZXJyb3Itdmlldy9pbmRleC5qcyIsInNyYy9qcy9jb250cm9scy9jLWZpbmFsLXBhZ2Utdmlldy9pbmRleC5odG1sIiwic3JjL2pzL2NvbnRyb2xzL2MtZmluYWwtcGFnZS12aWV3L2luZGV4LmpzIiwic3JjL2pzL2NvbnRyb2xzL2MtaG9tZS1wYWdlL2luZGV4Lmh0bWwiLCJzcmMvanMvY29udHJvbHMvYy1ob21lLXBhZ2UvaW5kZXguanMiLCJzcmMvanMvY29udHJvbHMvYy1zdXJ2ZXktZm9ybS9pbmRleC5odG1sIiwic3JjL2pzL2NvbnRyb2xzL2Mtc3VydmV5LWZvcm0vaW5kZXguanMiLCJzcmMvanMvY29udHJvbHMvYy1zdXJ2ZXktdmlldy9pbmRleC5odG1sIiwic3JjL2pzL2NvbnRyb2xzL2Mtc3VydmV5LXZpZXcvaW5kZXguanMiLCJzcmMvanMvY29udHJvbHMvYy10ZXN0LWZvcm0vaW5kZXguaHRtbCIsInNyYy9qcy9jb250cm9scy9jLXRlc3QtZm9ybS9pbmRleC5qcyIsInNyYy9qcy9jb250cm9scy9jLXRlc3Qtdmlldy9pbmRleC5odG1sIiwic3JjL2pzL2NvbnRyb2xzL2MtdGVzdC12aWV3L2luZGV4LmpzIiwic3JjL2pzL2NvbnRyb2xzL2luZGV4LmpzIiwic3JjL2pzL2NvbnRyb2xzL21haW4tYXBwbGljYXRpb24vaW5kZXguaHRtbCIsInNyYy9qcy9jb250cm9scy9tYWluLWFwcGxpY2F0aW9uL2luZGV4LmpzIiwic3JjL2pzL2V2ZW50cy9iZWdpbi10ZXN0LWVycm9yLWV2ZW50LmpzIiwic3JjL2pzL2V2ZW50cy9iZWdpbi10ZXN0LWV2ZW50LmpzIiwic3JjL2pzL2V2ZW50cy9jbG9zZS1wYWdlLWV2ZW50LmpzIiwic3JjL2pzL2V2ZW50cy9maW5hbC1wYWdlLWV2ZW50LmpzIiwic3JjL2pzL2V2ZW50cy9oYXZlLWlkLWV2ZW50LmpzIiwic3JjL2pzL2V2ZW50cy9pbmRleC5qcyIsInNyYy9qcy9ldmVudHMvbmV4dC10ZXN0LWV2ZW50LmpzIiwic3JjL2pzL2V2ZW50cy9zZW5kLW1haWwtZXZlbnQuanMiLCJzcmMvanMvZXZlbnRzL3NlbmQtcHJlZmVyZW5jZS1ldmVudC5qcyIsInNyYy9qcy9ldmVudHMvc2VuZC1zdXJ2ZXktZXZlbnQuanMiLCJzcmMvanMvZXZlbnRzL3N0YXJ0LXN1cnZleS1ldmVudC5qcyIsInNyYy9qcy9pbmRleC5qcyIsInNyYy9qcy9yZXBvc2l0b3JpZXMvYXBpX2NhbGxlci5qcyIsInNyYy9qcy9yZXBvc2l0b3JpZXMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUMvRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQ3BEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQ1hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUNsREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FDM0RBO0FBQ0E7OztBQ0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUNqREE7QUFDQTs7O0FDREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQzNGQTtBQUNBOzs7QUNEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FDakRBO0FBQ0E7OztBQ0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQ2xEQTtBQUNBOzs7QUNEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FDakRBO0FBQ0E7OztBQ0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQ3JHQTtBQUNBOzs7QUNEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUNsREE7QUFDQTs7O0FDREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQzFTQTtBQUNBOzs7QUNEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FDbERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQ2pCQTtBQUNBOzs7QUNEQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FDL0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNYQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDcEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQ1hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUNoQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNoQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN2QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDeEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUN2QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUNqQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQ2hDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FDckNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIvKmpzbGludCBub2RlOiB0cnVlLCBub21lbjogdHJ1ZSAqL1xuLypnbG9iYWxzICQqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBQcm9taXNlID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJ1Byb21pc2UnXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ1Byb21pc2UnXSA6IG51bGwpO1xuXG5mdW5jdGlvbiBBY3Rpb24ob3B0aW9ucykgeyAvLyBhZGQgXCJvcHRpb25zXCIgcGFyYW1ldGVycyBpZiBuZWVkZWRcbiAgICAvLyBUT0RPOiBHbG9iYWwgSW5pdGlhbGl6YXRpb25cbiAgICB0aGlzLmFwaV9jYWxsZXIgPSBvcHRpb25zLnJlcG9zaXRvcmllcy5hcGlfY2FsbGVyO1xufVxuXG5BY3Rpb24ucHJvdG90eXBlLnJ1biA9IGZ1bmN0aW9uIChwYXJhbWV0ZXJzLCBzb2x2ZSkgeyAvLyBhZGQgXCJvbkNhbmNlbFwiIHBhcmFtZXRlcnMgaWYgbmVlZGVkXG4gICAgLy8gUGFyYW1ldGVyczpcbiAgICAvLyBwYXJhbWV0ZXJzWydzdXJ2ZXkgYW5zd2VycyddXG4gICAgLy8gcGFyYW1ldGVyc1sndXNlciBpZCddXG4gICAgLy8gcGFyYW1ldGVyc1sndGVzdCBudW1iZXInXVxuICAgIHZhciB0YWJsZXMgPSB7XG4gICAgICAgICcwJyA6ICdwcm9ncmVzc2lvbl9jb21wb3VuZF9tYWonLFxuICAgICAgICAnMScgOiAncHJvZ3Jlc3Npb25fY29tcG91bmRfbWluJyxcbiAgICAgICAgJzInIDogJ3Byb2dyZXNzaW9uX25ncmFtX21haicsXG4gICAgICAgICczJyA6ICdwcm9ncmVzc2lvbl9uZ3JhbV9taW4nLFxuICAgICAgICAnNCcgOiAncHJvZ3Jlc3Npb25faG1tX21haicsXG4gICAgICAgICc1JyA6ICdwcm9ncmVzc2lvbl9obW1fbWluJyxcbiAgICAgICAgJzYnIDogJ3Byb2dyZXNzaW9uX3Jubl9tYWonLFxuICAgICAgICAnNycgOiAncHJvZ3Jlc3Npb25fcm5uX21pbidcbiAgICB9O1xuXG4gICAgLy8gVE9ETzogRXhlY3V0aW9uXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHZhciB1cmwgPSAnL2FwaS9wcm9ncmVzc2lvbi8nICsgdGFibGVzW3BhcmFtZXRlcnNbJ3Rlc3QgbnVtYmVyJ11dO1xuICAgIHNlbGYuYXBpX2NhbGxlci5jYWxsX2FwaSh1cmwsJ0dFVCcsIHt9KS50aGVuKGZ1bmN0aW9uKHJlc3VsdCl7XG4gICAgICAgIC8vICQubm90aWZ5KHttZXNzYWdlOiAnRG93bmxvYWRlZCBjaG9yZHMnfSwge2FsbG93X2Rpc21pc3M6IHRydWUsIHR5cGU6ICdzdWNjZXNzJ30pO1xuICAgICAgICBzb2x2ZSh7XG4gICAgICAgICAgICBldmVudDogJ2JlZ2luLXRlc3QtZXZlbnQnLCAvLyBCZWdpbiBUZXN0XG4gICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgJ3Byb2dyZXNzaW9ucyc6IHJlc3VsdC5wcm9ncmVzc2lvbnMsXG4gICAgICAgICAgICAgICAgJ3N1cnZleSBhbnN3ZXJzJzogcGFyYW1ldGVyc1snc3VydmV5IGFuc3dlcnMnXSxcbiAgICAgICAgICAgICAgICAndXNlciBpZCc6IHBhcmFtZXRlcnNbJ3VzZXIgaWQnXSxcbiAgICAgICAgICAgICAgICAndmFsdWVzIGhpc3RvcnknOiBwYXJhbWV0ZXJzWyd2YWx1ZXMgaGlzdG9yeSddIHx8IHtcbiAgICAgICAgICAgICAgICAgICAgMCA6IFtdLFxuICAgICAgICAgICAgICAgICAgICAxIDogW10sXG4gICAgICAgICAgICAgICAgICAgIDIgOiBbXSxcbiAgICAgICAgICAgICAgICAgICAgMyA6IFtdLFxuICAgICAgICAgICAgICAgICAgICA0IDogW10sXG4gICAgICAgICAgICAgICAgICAgIDUgOiBbXSxcbiAgICAgICAgICAgICAgICAgICAgNiA6IFtdLFxuICAgICAgICAgICAgICAgICAgICA3IDogW11cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICd0ZXN0IG51bWJlcicgOiBwYXJhbWV0ZXJzWyd0ZXN0IG51bWJlciddXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pLmNhdGNoKGZ1bmN0aW9uKHJlc3VsdCl7XG4gICAgICAgIHNvbHZlKHtcbiAgICAgICAgICAgIGV2ZW50OiAnYmVnaW4tdGVzdC1lcnJvci1ldmVudCcsXG4gICAgICAgICAgICBkYXRhOiB7fVxuICAgICAgICB9KTtcbiAgICB9KTtcbiAgICAvLyBUSElTIENBTiBCRSBSRU1PVkVEIChCRUdJTilcbiAgICAvLyAkLm5vdGlmeSh7bWVzc2FnZTogJ0NvbXB1dGUgZmlyc3QgY2hvcmRzJ30sIHthbGxvd19kaXNtaXNzOiB0cnVlLCB0eXBlOiAnc3VjY2Vzcyd9KTtcbiAgICAvLyBzb2x2ZSh7XG4gICAgLy8gICAgIGV2ZW50OiAnYmVnaW4tdGVzdC1ldmVudCcsIC8vIEJlZ2luIFRlc3RcbiAgICAvLyAgICAgZGF0YToge1xuICAgIC8vICAgICAgICAgJ2Nob3Jkcyc6ICcwJyxcbiAgICAvLyAgICAgICAgICdzdXJ2ZXkgYW5zd2Vycyc6IHBhcmFtZXRlcnNbJ3N1cnZleSBhbnN3ZXJzJ10sXG4gICAgLy8gICAgICAgICAndXNlciBpZCc6IHBhcmFtZXRlcnNbJ3VzZXIgaWQnXSxcbiAgICAvLyAgICAgfVxuICAgIC8vIH0pO1xuICAgIC8vIFRISVMgQ0FOIEJFIFJFTU9WRUQgKEVORClcbn07XG5cbmV4cG9ydHMuY3JlYXRlQWN0aW9uID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICB2YXIgYWN0aW9uID0gbmV3IEFjdGlvbihvcHRpb25zKTtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChzb2x2ZSwgcmVqZWN0LCBvbkNhbmNlbCkge1xuICAgICAgICAgICAgdmFyIHBhcmFtZXRlcnMgPSAoZGF0YSAmJiBkYXRhLmZpbHRlcnMpIHx8IHt9O1xuICAgICAgICAgICAgYWN0aW9uLnJ1bihwYXJhbWV0ZXJzLCBzb2x2ZSwgb25DYW5jZWwpO1xuICAgICAgICB9KTtcbiAgICB9O1xufTtcbiIsIi8qanNsaW50IG5vZGU6IHRydWUsIG5vbWVuOiB0cnVlICovXG4vKmdsb2JhbHMgJCovXG5cInVzZSBzdHJpY3RcIjtcblxudmFyIFByb21pc2UgPSAodHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvd1snUHJvbWlzZSddIDogdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbFsnUHJvbWlzZSddIDogbnVsbCk7XG5cbmZ1bmN0aW9uIEFjdGlvbihvcHRpb25zKSB7IC8vIGFkZCBcIm9wdGlvbnNcIiBwYXJhbWV0ZXJzIGlmIG5lZWRlZFxuICAgIC8vIFRPRE86IEdsb2JhbCBJbml0aWFsaXphdGlvblxuICAgIHRoaXMuYXBpX2NhbGxlciA9IG9wdGlvbnMucmVwb3NpdG9yaWVzLmFwaV9jYWxsZXI7XG59XG5cbkFjdGlvbi5wcm90b3R5cGUucnVuID0gZnVuY3Rpb24gKHBhcmFtZXRlcnMsIHNvbHZlKSB7IC8vIGFkZCBcIm9uQ2FuY2VsXCIgcGFyYW1ldGVycyBpZiBuZWVkZWRcbiAgICAvLyBQYXJhbWV0ZXJzOlxuXG4gICAgLy8gVE9ETzogRXhlY3V0aW9uXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHNlbGYuYXBpX2NhbGxlci5jYWxsX2FwaSgnL2FwaS9hdXRoJywnR0VUJywge30pLnRoZW4oZnVuY3Rpb24ocmVzdWx0KXtcbiAgICAgICAgLy8gJC5ub3RpZnkoe21lc3NhZ2U6ICdHZXQgU3VydmV5J30sIHthbGxvd19kaXNtaXNzOiB0cnVlLCB0eXBlOiAnc3VjY2Vzcyd9KTtcbiAgICAgICAgc29sdmUoe1xuICAgICAgICAgICAgZXZlbnQ6ICdoYXZlLWlkLWV2ZW50JywgLy8gRG9uZVxuICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICd1c2VyIGlkJzpyZXN1bHRbJ3VzZXItdG9rZW4nXSxcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSkuY2F0Y2goZnVuY3Rpb24ocmVzdWx0KXtcbiAgICAgICAgc29sdmUoe1xuICAgICAgICAgICAgZXZlbnQ6ICdoYXZlLWlkLWVycm9yLWV2ZW50JyxcbiAgICAgICAgICAgIGRhdGE6IHt9XG4gICAgICAgIH0pO1xuXG4gICAgfSk7XG5cbiAgICAvLyBUSElTIENBTiBCRSBSRU1PVkVEIChCRUdJTilcbiAgICAvLyAkLm5vdGlmeSh7bWVzc2FnZTogJ0dldCBJZCd9LCB7YWxsb3dfZGlzbWlzczogdHJ1ZSwgdHlwZTogJ3N1Y2Nlc3MnfSk7XG4gICAgLy8gc29sdmUoe1xuICAgIC8vICAgICBldmVudDogJ2hhdmUtaWQtZXZlbnQnLCAvLyBEb25lXG4gICAgLy8gICAgIGRhdGE6IHtcbiAgICAvLyAgICAgICAgICd1c2VyIGlkJzogJzAnLFxuICAgIC8vICAgICB9XG4gICAgLy8gfSk7XG4gICAgLy8gVEhJUyBDQU4gQkUgUkVNT1ZFRCAoRU5EKVxufTtcblxuZXhwb3J0cy5jcmVhdGVBY3Rpb24gPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgIHZhciBhY3Rpb24gPSBuZXcgQWN0aW9uKG9wdGlvbnMpO1xuICAgIHJldHVybiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHNvbHZlLCByZWplY3QsIG9uQ2FuY2VsKSB7XG4gICAgICAgICAgICB2YXIgcGFyYW1ldGVycyA9IChkYXRhICYmIGRhdGEuZmlsdGVycykgfHwge307XG4gICAgICAgICAgICBhY3Rpb24ucnVuKHBhcmFtZXRlcnMsIHNvbHZlLCBvbkNhbmNlbCk7XG4gICAgICAgIH0pO1xuICAgIH07XG59O1xuIiwiLypqc2xpbnQgbm9kZTogdHJ1ZSwgbm9tZW46IHRydWUgKi9cblwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLmNyZWF0ZUFjdGlvbnMgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgIHJldHVybiB7XG4gICAgICAgICdzZW5kLW1haWwnOiByZXF1aXJlKCcuL3NlbmQtbWFpbCcpLmNyZWF0ZUFjdGlvbihvcHRpb25zKSxcbiAgICAgICAgJ2NvbXB1dGUtZmlyc3QtY2hvcmRzJzogcmVxdWlyZSgnLi9jb21wdXRlLWZpcnN0LWNob3JkcycpLmNyZWF0ZUFjdGlvbihvcHRpb25zKSxcbiAgICAgICAgJ3NlbmQtcHJlZmVyZW5jZSc6IHJlcXVpcmUoJy4vc2VuZC1wcmVmZXJlbmNlJykuY3JlYXRlQWN0aW9uKG9wdGlvbnMpLFxuICAgICAgICAnZ2V0LWlkJzogcmVxdWlyZSgnLi9nZXQtaWQnKS5jcmVhdGVBY3Rpb24ob3B0aW9ucyksXG4gICAgfTtcbn07XG4iLCIvKmpzbGludCBub2RlOiB0cnVlLCBub21lbjogdHJ1ZSAqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBQcm9taXNlID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJ1Byb21pc2UnXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ1Byb21pc2UnXSA6IG51bGwpO1xuXG5mdW5jdGlvbiBBY3Rpb24ob3B0aW9ucykgeyAvLyBhZGQgXCJvcHRpb25zXCIgcGFyYW1ldGVycyBpZiBuZWVkZWRcbiAgICAvLyBUT0RPOiBHbG9iYWwgSW5pdGlhbGl6YXRpb25cbiAgICB0aGlzLmFwaV9jYWxsZXIgPSBvcHRpb25zLnJlcG9zaXRvcmllcy5hcGlfY2FsbGVyO1xufVxuXG5BY3Rpb24ucHJvdG90eXBlLnJ1biA9IGZ1bmN0aW9uIChwYXJhbWV0ZXJzLCBzb2x2ZSkgeyAvLyBhZGQgXCJvbkNhbmNlbFwiIHBhcmFtZXRlcnMgaWYgbmVlZGVkXG4gICAgLy8gUGFyYW1ldGVyczpcbiAgICAvLyBwYXJhbWV0ZXJzWydjb21tZW50cyddXG4gICAgLy8gcGFyYW1ldGVyc1snbWFpbCddXG4gICAgLy8gcGFyYW1ldGVyc1sndXNlciBpZCddXG5cbiAgICAvLyBUT0RPOiBFeGVjdXRpb25cbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgdmFyIGRhdGEgPSBKU09OLnN0cmluZ2lmeSh7XG5cbiAgICAgICAgY29tbWVudHMgOiBwYXJhbWV0ZXJzWydjb21tZW50cyddLFxuICAgICAgICBlbWFpbCA6IHBhcmFtZXRlcnNbJ21haWwnXSxcbiAgICAgICAgdXNlcl9pZCA6IHBhcmFtZXRlcnNbJ3VzZXIgaWQnXSxcbiAgICB9KTtcbiAgICBzZWxmLmFwaV9jYWxsZXIuY2FsbF9hcGkoJy9hcGkvY29tbWVudCcsJ1BPU1QnLCBkYXRhKS50aGVuKGZ1bmN0aW9uKHJlc3VsdCl7XG4gICAgICAgIC8vICQubm90aWZ5KHttZXNzYWdlOiAnU2VuZCBNYWlsJ30sIHthbGxvd19kaXNtaXNzOiB0cnVlLCB0eXBlOiAnc3VjY2Vzcyd9KTtcbiAgICAgICAgc29sdmUoe1xuICAgICAgICAgICAgZXZlbnQ6ICdjbG9zZS1wYWdlLWV2ZW50JywgLy8gQ2xvc2UgUGFnZVxuICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTtcbiAgICAvLyBUSElTIENBTiBCRSBSRU1PVkVEIChCRUdJTilcbiAgICAvLyAkLm5vdGlmeSh7bWVzc2FnZTogJ1NlbmQgTWFpbCd9LCB7YWxsb3dfZGlzbWlzczogdHJ1ZSwgdHlwZTogJ3N1Y2Nlc3MnfSk7XG4gICAgLy8gc29sdmUoe1xuICAgIC8vICAgICBkYXRhOiB7XG4gICAgLy8gICAgIH1cbiAgICAvLyB9KTtcbiAgICAvLyBUSElTIENBTiBCRSBSRU1PVkVEIChFTkQpXG59O1xuXG5leHBvcnRzLmNyZWF0ZUFjdGlvbiA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgdmFyIGFjdGlvbiA9IG5ldyBBY3Rpb24ob3B0aW9ucyk7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAoc29sdmUsIHJlamVjdCwgb25DYW5jZWwpIHtcbiAgICAgICAgICAgIHZhciBwYXJhbWV0ZXJzID0gKGRhdGEgJiYgZGF0YS5maWx0ZXJzKSB8fCB7fTtcbiAgICAgICAgICAgIGFjdGlvbi5ydW4ocGFyYW1ldGVycywgc29sdmUsIG9uQ2FuY2VsKTtcbiAgICAgICAgfSk7XG4gICAgfTtcbn07XG4iLCIvKmpzbGludCBub2RlOiB0cnVlLCBub21lbjogdHJ1ZSAqL1xuLypnbG9iYWxzICQqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBQcm9taXNlID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJ1Byb21pc2UnXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ1Byb21pc2UnXSA6IG51bGwpO1xuXG5mdW5jdGlvbiBBY3Rpb24ob3B0aW9ucykgeyAvLyBhZGQgXCJvcHRpb25zXCIgcGFyYW1ldGVycyBpZiBuZWVkZWRcbiAgICAvLyBUT0RPOiBHbG9iYWwgSW5pdGlhbGl6YXRpb25cbiAgICB0aGlzLmFwaV9jYWxsZXIgPSBvcHRpb25zLnJlcG9zaXRvcmllcy5hcGlfY2FsbGVyO1xufVxuXG5BY3Rpb24ucHJvdG90eXBlLnJ1biA9IGZ1bmN0aW9uIChwYXJhbWV0ZXJzLCBzb2x2ZSkgeyAvLyBhZGQgXCJvbkNhbmNlbFwiIHBhcmFtZXRlcnMgaWYgbmVlZGVkXG4gICAgLy8gUGFyYW1ldGVyczpcbiAgICAvLyBwYXJhbWV0ZXJzWydjb21wbGV4aXR5J11cbiAgICAvLyBwYXJhbWV0ZXJzWydzdXJ2ZXkgYW5zd2VycyddXG4gICAgLy8gcGFyYW1ldGVyc1sndXNlciBpZCddXG4gICAgLy8gcGFyYW1ldGVyc1sndmFsdWVzIGhpc3RvcnknXVxuXG4gICAgLy8gVE9ETzogRXhlY3V0aW9uXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHZhciBkYXRhID0gSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICBzdXJ2ZXlfYW5zd2VycyA6IHBhcmFtZXRlcnNbJ3N1cnZleSBhbnN3ZXJzJ10sXG4gICAgICAgIHVzZXJfaWQgOiBwYXJhbWV0ZXJzWyd1c2VyIGlkJ10sXG4gICAgICAgIHZhbHVlc19oaXN0b3J5IDogcGFyYW1ldGVyc1sndmFsdWVzIGhpc3RvcnknXVxuICAgIH0pO1xuICAgIHNlbGYuYXBpX2NhbGxlci5jYWxsX2FwaSgnL2FwaS9yZXN1bHQnLCdQT1NUJywgZGF0YSkudGhlbihmdW5jdGlvbihyZXN1bHQpe1xuICAgICAgICAvLyAkLm5vdGlmeSh7bWVzc2FnZTogJ1NlbmQgUHJlZmVyZW5jZXMnfSwge2FsbG93X2Rpc21pc3M6IHRydWUsIHR5cGU6ICdzdWNjZXNzJ30pO1xuICAgICAgICBzb2x2ZSh7XG4gICAgICAgICAgICBldmVudDogJ2ZpbmFsLXBhZ2UtZXZlbnQnLCAvLyBGaW5hbCBQYWdlXG4gICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgJ3VzZXIgaWQnOiBwYXJhbWV0ZXJzWyd1c2VyIGlkJ10sXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pLmNhdGNoKGZ1bmN0aW9uKHJlc3VsdCl7XG4gICAgICAgIHNvbHZlKHtcbiAgICAgICAgICAgIGV2ZW50OiAnZmluYWwtcGFnZS1lcnJvci1ldmVudCcsXG4gICAgICAgICAgICBkYXRhOiB7fVxuICAgICAgICB9KTtcbiAgICB9KTtcbiAgICAvLyBUSElTIENBTiBCRSBSRU1PVkVEIChCRUdJTilcbiAgICAvLyAkLm5vdGlmeSh7bWVzc2FnZTogJ1NlbmQgUHJlZmVyZW5jZXMnfSwge2FsbG93X2Rpc21pc3M6IHRydWUsIHR5cGU6ICdzdWNjZXNzJ30pO1xuICAgIC8vIHNvbHZlKHtcbiAgICAvLyAgICAgZXZlbnQ6ICdmaW5hbC1wYWdlLWV2ZW50JywgLy8gRmluYWwgUGFnZVxuICAgIC8vICAgICBkYXRhOiB7XG4gICAgLy8gICAgICAgICAndXNlciBpZCc6IHBhcmFtZXRlcnNbJ3VzZXIgaWQnXSxcbiAgICAvLyAgICAgfVxuICAgIC8vIH0pO1xuICAgIC8vIFRISVMgQ0FOIEJFIFJFTU9WRUQgKEVORClcbn07XG5cbmV4cG9ydHMuY3JlYXRlQWN0aW9uID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICB2YXIgYWN0aW9uID0gbmV3IEFjdGlvbihvcHRpb25zKTtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uIChzb2x2ZSwgcmVqZWN0LCBvbkNhbmNlbCkge1xuICAgICAgICAgICAgdmFyIHBhcmFtZXRlcnMgPSAoZGF0YSAmJiBkYXRhLmZpbHRlcnMpIHx8IHt9O1xuICAgICAgICAgICAgYWN0aW9uLnJ1bihwYXJhbWV0ZXJzLCBzb2x2ZSwgb25DYW5jZWwpO1xuICAgICAgICB9KTtcbiAgICB9O1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gXCI8aDM+SGFybW9uaWMgQ29tcGxleGl0eSBUZXN0PC9oMz5cXG5cXG48cD5cXG4gICAgVGhhbmsgeW91IGZvciB5b3VyIHRpbWUsIGZlZWwgZnJlZSB0byBjbG9zZSB0aGlzIHRhYi5cXG48L3A+XCI7XG4iLCIvKmpzbGludCBub2RlOiB0cnVlLCBub21lbjogdHJ1ZSAqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBrbyA9ICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93WydrbyddIDogdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbFsna28nXSA6IG51bGwpO1xuXG5mdW5jdGlvbiBWaWV3TW9kZWwocGFyYW1zKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHNlbGYuY29udGV4dCA9IHBhcmFtcy5jb250ZXh0O1xuXG4gICAgc2VsZi5pbml0ID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICAgICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG4gICAgICAgIHNlbGYuY2hpbGRyZW4uZm9yRWFjaChmdW5jdGlvbiAoY2hpbGQpe1xuICAgICAgICAgICAgaWYgKGNoaWxkID09PSBvcHRpb25zLm1hc2spIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzZWxmLmNvbnRleHQudm1zW2NoaWxkXS5pbml0KG9wdGlvbnMpO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgc2VsZi50cmlnZ2VyID0gZnVuY3Rpb24gKGlkKSB7XG4gICAgICAgIHNlbGYuY29udGV4dC5ldmVudHNbaWRdKHNlbGYuY29udGV4dCk7XG4gICAgfTtcbn1cblxuVmlld01vZGVsLnByb3RvdHlwZS5pZCA9ICdjbG9zaW5nLXBhZ2UnO1xuVmlld01vZGVsLnByb3RvdHlwZS5jaGlsZHJlbiA9IFtcbl07XG5cbmV4cG9ydHMucmVnaXN0ZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAga28uY29tcG9uZW50cy5yZWdpc3RlcignYy1jbG9zaW5nLXBhZ2UnLCB7XG4gICAgICAgIHZpZXdNb2RlbDoge1xuICAgICAgICAgICAgY3JlYXRlVmlld01vZGVsOiBmdW5jdGlvbiAocGFyYW1zLCBjb21wb25lbnRJbmZvKSB7XG4gICAgICAgICAgICAgICAgdmFyIHZtID0gbmV3IFZpZXdNb2RlbChwYXJhbXMpO1xuICAgICAgICAgICAgICAgIHBhcmFtcy5jb250ZXh0LnZtc1t2bS5pZF0gPSB2bTtcbiAgICAgICAgICAgICAgICBwYXJhbXMuY29udGV4dC5ydW5uaW5nQWN0aW9uc0J5Q29udGFpbmVyW3ZtLmlkXSA9IFtdO1xuICAgICAgICAgICAgICAgIGtvLnV0aWxzLmRvbU5vZGVEaXNwb3NhbC5hZGREaXNwb3NlQ2FsbGJhY2soY29tcG9uZW50SW5mby5lbGVtZW50LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHBhcmFtcy5jb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbdm0uaWRdLmZvckVhY2goZnVuY3Rpb24gKHByb21pc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb21pc2UuY2FuY2VsKCk7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBwYXJhbXMuY29udGV4dC5ydW5uaW5nQWN0aW9uc0J5Q29udGFpbmVyW3ZtLmlkXTtcbiAgICAgICAgICAgICAgICAgICAgZGVsZXRlIHBhcmFtcy5jb250ZXh0LnZtc1t2bS5pZF07XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHZtO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB0ZW1wbGF0ZTogcmVxdWlyZSgnLi9pbmRleC5odG1sJyksXG4gICAgICAgIHN5bmNocm9ub3VzOiB0cnVlXG4gICAgfSk7XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxoMz5UaGFuayB5b3UgZm9yIHlvdXIgdGltZTwvaDM+XFxuPHA+XFxuICAgIEFkZCB5b3VyIGUtbWFpbCBhZGRyZXNzIGlmIHlvdSB3YW50IHRvIGJlIGluZm9ybWVkIGFib3V0IHRoZSByZXN1bHRzIG9mIHRoaXMgdGVzdCwgb3RoZXJ3aXNlIHNpbXBseSBjbG9zZSB0aGlzIHBhZ2UuXFxuICAgIElmIHlvdSBoYXZlIHNvbWUgY29tbWVudHMgb3Igc3VnZ2VzdGlvbnMsIHdyaXRlIHRoZW0gaW4gdGhlIHNlY3Rpb24gYmVsb3cuXFxuPC9wPlxcbjxwPlxcbiAgICBGcmFuY2VzY28gRm9zY2FyaW5cXG48L3A+XFxuXFxuPGZvcm0+XFxuICAgIDxkaXYgY2xhc3M9XFxcImZvcm0tZ3JvdXBcXFwiIGRhdGEtYmluZD1cXFwiY3NzOiB7J2hhcy1lcnJvcic6IGVycm9ycygpWydlbWFpbCddfVxcXCI+XFxuICAgICAgICA8bGFiZWwgZm9yPVxcXCJlbWFpbC1mb3JtX2ZpZWxkXzFcXFwiIGRhdGEtYmluZD1cXFwiY3NzOiB7YWN0aXZlOiBmaWVsZHMoKVsnZW1haWwnXX0sIGF0dHI6IHsnZGF0YS1lcnJvcic6IGVycm9ycygpWydlbWFpbCddfVxcXCIgY2xhc3M9XFxcImNvbnRyb2wtbGFiZWxcXFwiPmVtYWlsPC9sYWJlbD5cXG4gICAgICAgIDxpbnB1dCBpZD1cXFwiZW1haWwtZm9ybV9maWVsZF8xXFxcIiBkYXRhLWJpbmQ9XFxcInZhbHVlOiBmaWVsZHMoKVsnZW1haWwnXVxcXCIgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbCB2YWxpZGF0ZVxcXCIgYXJpYS1kZXNjcmliZWRieT1cXFwiZW1haWwtZm9ybV9maWVsZF8xX2Vycm9yXFxcIj5cXG4gICAgICAgIDxzcGFuIGlkPVxcXCJlbWFpbC1mb3JtX2ZpZWxkXzFfZXJyb3JcXFwiIGNsYXNzPVxcXCJoZWxwLWJsb2NrXFxcIiBkYXRhLWJpbmQ9XFxcInRleHQ6IGVycm9ycygpWydlbWFpbCddXFxcIj48L3NwYW4+XFxuICAgIDwvZGl2PlxcblxcbiAgICA8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIiBkYXRhLWJpbmQ9XFxcImNzczogeydoYXMtZXJyb3InOiBlcnJvcnMoKVsnY29tbWVudHMnXX1cXFwiPlxcbiAgICAgICAgPGxhYmVsIGZvcj1cXFwiZW1haWwtZm9ybV9maWVsZF8wXFxcIiBkYXRhLWJpbmQ9XFxcImNzczoge2FjdGl2ZTogZmllbGRzKClbJ2NvbW1lbnRzJ119LCBhdHRyOiB7J2RhdGEtZXJyb3InOiBlcnJvcnMoKVsnY29tbWVudHMnXX1cXFwiIGNsYXNzPVxcXCJjb250cm9sLWxhYmVsXFxcIj5jb21tZW50czwvbGFiZWw+XFxuICAgICAgICA8IS0tIDxpbnB1dCBpZD1cXFwiZW1haWwtZm9ybV9maWVsZF8wXFxcIiBkYXRhLWJpbmQ9XFxcInZhbHVlOiBmaWVsZHMoKVsnY29tbWVudHMnXVxcXCIgdHlwZT1cXFwidGV4dFxcXCIgY2xhc3M9XFxcImZvcm0tY29udHJvbCB2YWxpZGF0ZVxcXCIgYXJpYS1kZXNjcmliZWRieT1cXFwiZW1haWwtZm9ybV9maWVsZF8wX2Vycm9yXFxcIj4gLS0+XFxuICAgICAgICA8dGV4dGFyZWEgZGF0YS1iaW5kPVxcXCJ2YWx1ZTogZmllbGRzKClbJ2NvbW1lbnRzJ11cXFwiIHBsYWNlaG9sZGVyPVxcXCJDb21tZW50cyBhbmQgc3VnZ2VzdGlvbnNcXFwiPjwvdGV4dGFyZWE+XFxuICAgICAgICA8c3BhbiBpZD1cXFwiZW1haWwtZm9ybV9maWVsZF8wX2Vycm9yXFxcIiBjbGFzcz1cXFwiaGVscC1ibG9ja1xcXCIgZGF0YS1iaW5kPVxcXCJ0ZXh0OiBlcnJvcnMoKVsnY29tbWVudHMnXVxcXCI+PC9zcGFuPlxcbiAgICA8L2Rpdj5cXG5cXG4gICAgPCEtLSA8ZGl2IGNsYXNzPVxcXCJmb3JtLWdyb3VwXFxcIiBkYXRhLWJpbmQ9XFxcImNzczogeydoYXMtZXJyb3InOiBlcnJvcnMoKVsndXNlciBpZCddfVxcXCI+XFxuICAgICAgICA8bGFiZWwgZm9yPVxcXCJlbWFpbC1mb3JtX2ZpZWxkXzJcXFwiIGRhdGEtYmluZD1cXFwiY3NzOiB7YWN0aXZlOiBmaWVsZHMoKVsndXNlciBpZCddfSwgYXR0cjogeydkYXRhLWVycm9yJzogZXJyb3JzKClbJ3VzZXIgaWQnXX1cXFwiIGNsYXNzPVxcXCJjb250cm9sLWxhYmVsXFxcIj51c2VyIGlkPC9sYWJlbD5cXG4gICAgICAgIDxpbnB1dCBpZD1cXFwiZW1haWwtZm9ybV9maWVsZF8yXFxcIiBkYXRhLWJpbmQ9XFxcInZhbHVlOiBmaWVsZHMoKVsndXNlciBpZCddXFxcIiB0eXBlPVxcXCJ0ZXh0XFxcIiBjbGFzcz1cXFwiZm9ybS1jb250cm9sIHZhbGlkYXRlXFxcIiBhcmlhLWRlc2NyaWJlZGJ5PVxcXCJlbWFpbC1mb3JtX2ZpZWxkXzJfZXJyb3JcXFwiPlxcbiAgICAgICAgPHNwYW4gaWQ9XFxcImVtYWlsLWZvcm1fZmllbGRfMl9lcnJvclxcXCIgY2xhc3M9XFxcImhlbHAtYmxvY2tcXFwiIGRhdGEtYmluZD1cXFwidGV4dDogZXJyb3JzKClbJ3VzZXIgaWQnXVxcXCI+PC9zcGFuPlxcbiAgICA8L2Rpdj4gLS0+XFxuPC9mb3JtPlxcblxcblxcbjxkaXYgY2xhc3M9XFxcInJvd1xcXCI+XFxuICAgIDxhIGNsYXNzPVxcXCJjb2wteHMtMiBidG4gYnRuLXByaW1hcnlcXFwiIGRhdGEtYmluZD1cXFwiY2xpY2s6IHRyaWdnZXIuYmluZCgkZGF0YSwgJ3NlbmQtbWFpbC1ldmVudCcpXFxcIj5TZW5kPC9hPlxcbjwvZGl2PlwiO1xuIiwiLypqc2xpbnQgbm9kZTogdHJ1ZSwgbm9tZW46IHRydWUgKi9cblwidXNlIHN0cmljdFwiO1xuXG52YXIga28gPSAodHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvd1sna28nXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ2tvJ10gOiBudWxsKSxcbiAgICBQcm9taXNlID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJ1Byb21pc2UnXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ1Byb21pc2UnXSA6IG51bGwpO1xuXG5mdW5jdGlvbiBWaWV3TW9kZWwocGFyYW1zKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHNlbGYuY29udGV4dCA9IHBhcmFtcy5jb250ZXh0O1xuICAgIHNlbGYuc3RhdHVzID0ga28ub2JzZXJ2YWJsZSgnJyk7XG4gICAgc2VsZi5maWVsZHMgPSBrby5vYnNlcnZhYmxlKHt9KTtcbiAgICBzZWxmLmVycm9ycyA9IGtvLm9ic2VydmFibGUoe30pO1xuXG4gICAgc2VsZi50cmlnZ2VyID0gZnVuY3Rpb24gKGlkKSB7XG4gICAgICAgIHNlbGYuY29udGV4dC5ldmVudHNbaWRdKHNlbGYuY29udGV4dCwgc2VsZi5vdXRwdXQpO1xuICAgIH07XG59XG5cblZpZXdNb2RlbC5wcm90b3R5cGUuaWQgPSAnZW1haWwtZm9ybSc7XG5cblZpZXdNb2RlbC5wcm90b3R5cGUud2FpdEZvclN0YXR1c0NoYW5nZSA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5faW5pdGlhbGl6aW5nIHx8XG4gICAgICAgICAgIFByb21pc2UucmVzb2x2ZSgpO1xufTtcblxuVmlld01vZGVsLnByb3RvdHlwZS5fY29tcHV0ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLm91dHB1dCA9IHtcbiAgICAgICAgJ2NvbW1lbnRzJzogdGhpcy5pbnB1dFsnY29tbWVudHMnXSxcbiAgICAgICAgJ2VtYWlsJzogdGhpcy5pbnB1dFsnZW1haWwnXSxcbiAgICAgICAgJ3VzZXIgaWQnOiB0aGlzLmlucHV0Wyd1c2VyIGlkJ10sXG4gICAgfVxuICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgICAgZmllbGRzID0ge1xuICAgICAgICAgICAgJ2NvbW1lbnRzJzoga28ub2JzZXJ2YWJsZSh0aGlzLmlucHV0Wydjb21tZW50cyddKSxcbiAgICAgICAgICAgICdlbWFpbCc6IGtvLm9ic2VydmFibGUodGhpcy5pbnB1dFsnZW1haWwnXSksXG4gICAgICAgICAgICAndXNlciBpZCc6IGtvLm9ic2VydmFibGUodGhpcy5pbnB1dFsndXNlciBpZCddKSxcbiAgICAgICAgfSxcbiAgICAgICAgZXJyb3JzID0ge1xuICAgICAgICAgICAgJ2NvbW1lbnRzJzoga28ub2JzZXJ2YWJsZSh0aGlzLmlucHV0Wydjb21tZW50cy1lcnJvciddKSxcbiAgICAgICAgICAgICdlbWFpbCc6IGtvLm9ic2VydmFibGUodGhpcy5pbnB1dFsnZW1haWwtZXJyb3InXSksXG4gICAgICAgICAgICAndXNlciBpZCc6IGtvLm9ic2VydmFibGUodGhpcy5pbnB1dFsndXNlciBpZC1lcnJvciddKSxcbiAgICAgICAgfTtcbiAgICBmaWVsZHNbJ2NvbW1lbnRzJ10uc3Vic2NyaWJlKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICBzZWxmLm91dHB1dFsnY29tbWVudHMnXSA9IHZhbHVlO1xuICAgICAgICBzZWxmLmVycm9ycygpWydjb21tZW50cyddKHVuZGVmaW5lZCk7XG4gICAgfSk7XG4gICAgZmllbGRzWydlbWFpbCddLnN1YnNjcmliZShmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgc2VsZi5vdXRwdXRbJ2VtYWlsJ10gPSB2YWx1ZTtcbiAgICAgICAgc2VsZi5lcnJvcnMoKVsnZW1haWwnXSh1bmRlZmluZWQpO1xuICAgIH0pO1xuICAgIGZpZWxkc1sndXNlciBpZCddLnN1YnNjcmliZShmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgc2VsZi5vdXRwdXRbJ3VzZXIgaWQnXSA9IHZhbHVlO1xuICAgICAgICBzZWxmLmVycm9ycygpWyd1c2VyIGlkJ10odW5kZWZpbmVkKTtcbiAgICB9KTtcbiAgICB0aGlzLmZpZWxkcyhmaWVsZHMpO1xuICAgIHRoaXMuZXJyb3JzKGVycm9ycyk7XG4gICAgdGhpcy5zdGF0dXMoJ2NvbXB1dGVkJyk7XG59O1xuXG5cblZpZXdNb2RlbC5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG4gICAgdGhpcy5vdXRwdXQgPSB1bmRlZmluZWQ7XG4gICAgdGhpcy5maWVsZHMoe30pO1xuICAgIHRoaXMuZXJyb3JzKHt9KTtcbiAgICB0aGlzLmlucHV0ID0gb3B0aW9ucy5pbnB1dCB8fCB7fTtcbiAgICB0aGlzLnN0YXR1cygncmVhZHknKTtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgdGhpcy5faW5pdGlhbGl6aW5nID0gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUpIHtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBzZWxmLl9jb21wdXRlKCk7XG4gICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICBzZWxmLl9pbml0aWFsaXppbmcgPSB1bmRlZmluZWQ7XG4gICAgICAgIH0sIDEpO1xuICAgIH0pO1xufTtcblxuZXhwb3J0cy5yZWdpc3RlciA9IGZ1bmN0aW9uICgpIHtcbiAgICBrby5jb21wb25lbnRzLnJlZ2lzdGVyKCdjLWVtYWlsLWZvcm0nLCB7XG4gICAgICAgIHZpZXdNb2RlbDoge1xuICAgICAgICAgICAgY3JlYXRlVmlld01vZGVsOiBmdW5jdGlvbiAocGFyYW1zLCBjb21wb25lbnRJbmZvKSB7XG4gICAgICAgICAgICAgICAgdmFyIHZtID0gbmV3IFZpZXdNb2RlbChwYXJhbXMpO1xuICAgICAgICAgICAgICAgIHBhcmFtcy5jb250ZXh0LnZtc1t2bS5pZF0gPSB2bTtcbiAgICAgICAgICAgICAgICBrby51dGlscy5kb21Ob2RlRGlzcG9zYWwuYWRkRGlzcG9zZUNhbGxiYWNrKGNvbXBvbmVudEluZm8uZWxlbWVudCwgZnVuY3Rpb24gKCkgeyBkZWxldGUgcGFyYW1zLmNvbnRleHQudm1zW3ZtLmlkXTsgfSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHZtO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB0ZW1wbGF0ZTogcmVxdWlyZSgnLi9pbmRleC5odG1sJyksXG4gICAgICAgIHN5bmNocm9ub3VzOiB0cnVlXG4gICAgfSk7XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxoMz5FcnJvcjwvaDM+XFxuXFxuPHA+XFxuICAgIFNvbWV0aGluZyB3ZW50IHdyb25nIHdpdGggdGhlIGNvbm5lY3Rpb24gdG8gc2VydmVyLlxcbiAgICBTb3JyeSBmb3IgdGhlIGluY29udmVuaWVuY2UsIHlvdSBjYW4gdHJ5IGFnYWluIGluIHNvbWUgbWludXRzLlxcbjwvcD5cIjtcbiIsIi8qanNsaW50IG5vZGU6IHRydWUsIG5vbWVuOiB0cnVlICovXG5cInVzZSBzdHJpY3RcIjtcblxudmFyIGtvID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJ2tvJ10gOiB0eXBlb2YgZ2xvYmFsICE9PSBcInVuZGVmaW5lZFwiID8gZ2xvYmFsWydrbyddIDogbnVsbCk7XG5cbmZ1bmN0aW9uIFZpZXdNb2RlbChwYXJhbXMpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgc2VsZi5jb250ZXh0ID0gcGFyYW1zLmNvbnRleHQ7XG5cbiAgICBzZWxmLmluaXQgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgICAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgICAgICAgc2VsZi5jaGlsZHJlbi5mb3JFYWNoKGZ1bmN0aW9uIChjaGlsZCl7XG4gICAgICAgICAgICBpZiAoY2hpbGQgPT09IG9wdGlvbnMubWFzaykge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHNlbGYuY29udGV4dC52bXNbY2hpbGRdLmluaXQob3B0aW9ucyk7XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBzZWxmLnRyaWdnZXIgPSBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgc2VsZi5jb250ZXh0LmV2ZW50c1tpZF0oc2VsZi5jb250ZXh0KTtcbiAgICB9O1xufVxuXG5WaWV3TW9kZWwucHJvdG90eXBlLmlkID0gJ2Vycm9yLXZpZXcnO1xuVmlld01vZGVsLnByb3RvdHlwZS5jaGlsZHJlbiA9IFtcbl07XG5cbmV4cG9ydHMucmVnaXN0ZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAga28uY29tcG9uZW50cy5yZWdpc3RlcignYy1lcnJvci12aWV3Jywge1xuICAgICAgICB2aWV3TW9kZWw6IHtcbiAgICAgICAgICAgIGNyZWF0ZVZpZXdNb2RlbDogZnVuY3Rpb24gKHBhcmFtcywgY29tcG9uZW50SW5mbykge1xuICAgICAgICAgICAgICAgIHZhciB2bSA9IG5ldyBWaWV3TW9kZWwocGFyYW1zKTtcbiAgICAgICAgICAgICAgICBwYXJhbXMuY29udGV4dC52bXNbdm0uaWRdID0gdm07XG4gICAgICAgICAgICAgICAgcGFyYW1zLmNvbnRleHQucnVubmluZ0FjdGlvbnNCeUNvbnRhaW5lclt2bS5pZF0gPSBbXTtcbiAgICAgICAgICAgICAgICBrby51dGlscy5kb21Ob2RlRGlzcG9zYWwuYWRkRGlzcG9zZUNhbGxiYWNrKGNvbXBvbmVudEluZm8uZWxlbWVudCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBwYXJhbXMuY29udGV4dC5ydW5uaW5nQWN0aW9uc0J5Q29udGFpbmVyW3ZtLmlkXS5mb3JFYWNoKGZ1bmN0aW9uIChwcm9taXNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9taXNlLmNhbmNlbCgpO1xuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICBkZWxldGUgcGFyYW1zLmNvbnRleHQucnVubmluZ0FjdGlvbnNCeUNvbnRhaW5lclt2bS5pZF07XG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBwYXJhbXMuY29udGV4dC52bXNbdm0uaWRdO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHJldHVybiB2bTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdGVtcGxhdGU6IHJlcXVpcmUoJy4vaW5kZXguaHRtbCcpLFxuICAgICAgICBzeW5jaHJvbm91czogdHJ1ZVxuICAgIH0pO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gXCI8c3Bhbj5cXG4gICAgPCEtLSBFbWFpbCAtLT5cXG4gICAgPGMtZW1haWwtZm9ybSBwYXJhbXM9XFxcImNvbnRleHQ6IGNvbnRleHRcXFwiPjwvYy1lbWFpbC1mb3JtPlxcbjwvc3Bhbj5cIjtcbiIsIi8qanNsaW50IG5vZGU6IHRydWUsIG5vbWVuOiB0cnVlICovXG5cInVzZSBzdHJpY3RcIjtcblxudmFyIGtvID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJ2tvJ10gOiB0eXBlb2YgZ2xvYmFsICE9PSBcInVuZGVmaW5lZFwiID8gZ2xvYmFsWydrbyddIDogbnVsbCk7XG5cbmZ1bmN0aW9uIFZpZXdNb2RlbChwYXJhbXMpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgc2VsZi5jb250ZXh0ID0gcGFyYW1zLmNvbnRleHQ7XG5cbiAgICBzZWxmLmluaXQgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgICAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgICAgICAgc2VsZi5jaGlsZHJlbi5mb3JFYWNoKGZ1bmN0aW9uIChjaGlsZCl7XG4gICAgICAgICAgICBpZiAoY2hpbGQgPT09IG9wdGlvbnMubWFzaykge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHNlbGYuY29udGV4dC52bXNbY2hpbGRdLmluaXQob3B0aW9ucyk7XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBzZWxmLnRyaWdnZXIgPSBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgc2VsZi5jb250ZXh0LmV2ZW50c1tpZF0oc2VsZi5jb250ZXh0KTtcbiAgICB9O1xufVxuXG5WaWV3TW9kZWwucHJvdG90eXBlLmlkID0gJ2ZpbmFsLXBhZ2Utdmlldyc7XG5WaWV3TW9kZWwucHJvdG90eXBlLmNoaWxkcmVuID0gW1xuICAgICdlbWFpbC1mb3JtJyAvLyBFbWFpbFxuXTtcblxuZXhwb3J0cy5yZWdpc3RlciA9IGZ1bmN0aW9uICgpIHtcbiAgICBrby5jb21wb25lbnRzLnJlZ2lzdGVyKCdjLWZpbmFsLXBhZ2UtdmlldycsIHtcbiAgICAgICAgdmlld01vZGVsOiB7XG4gICAgICAgICAgICBjcmVhdGVWaWV3TW9kZWw6IGZ1bmN0aW9uIChwYXJhbXMsIGNvbXBvbmVudEluZm8pIHtcbiAgICAgICAgICAgICAgICB2YXIgdm0gPSBuZXcgVmlld01vZGVsKHBhcmFtcyk7XG4gICAgICAgICAgICAgICAgcGFyYW1zLmNvbnRleHQudm1zW3ZtLmlkXSA9IHZtO1xuICAgICAgICAgICAgICAgIHBhcmFtcy5jb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbdm0uaWRdID0gW107XG4gICAgICAgICAgICAgICAga28udXRpbHMuZG9tTm9kZURpc3Bvc2FsLmFkZERpc3Bvc2VDYWxsYmFjayhjb21wb25lbnRJbmZvLmVsZW1lbnQsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zLmNvbnRleHQucnVubmluZ0FjdGlvbnNCeUNvbnRhaW5lclt2bS5pZF0uZm9yRWFjaChmdW5jdGlvbiAocHJvbWlzZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJvbWlzZS5jYW5jZWwoKTtcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgZGVsZXRlIHBhcmFtcy5jb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbdm0uaWRdO1xuICAgICAgICAgICAgICAgICAgICBkZWxldGUgcGFyYW1zLmNvbnRleHQudm1zW3ZtLmlkXTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdm07XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHRlbXBsYXRlOiByZXF1aXJlKCcuL2luZGV4Lmh0bWwnKSxcbiAgICAgICAgc3luY2hyb25vdXM6IHRydWVcbiAgICB9KTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IFwiPGgzPkhhcm1vbmljIENvbXBsZXhpdHkgVGVzdDwvaDM+XFxuXFxuPHA+XFxuICAgIFRoaXMgaXMgYSB0ZXN0IHRoYXQgSSB3aWxsIHVzZSB0byBleHRyYWN0IGRhdGEgb24gaG93IHdlIHBlcmNlaXZlIHRoZSBoYXJtb25pYyBjb21wbGV4aXR5LlxcbiAgICBZb3UgaGF2ZSB0byBjb21wbGV0ZSBhIHNob3J0IHN1cnZleSBhbmQgdGhlbiBzb21lIHRlc3RzLiBBbGwgdGhlIGluc3RydWN0aW9ucyB3aWxsIGJlIHByb3ZpZGVkLlxcbjwvcD5cXG48cD5cXG4gICAgVGhlIHNpdGUgaXMgbm90IG9wdGltaXplZCBmb3IgbW9iaWxlIHBob25lcywgc28gdXNlIHlvdXIgbGFwdG9wIGZvciBhIGJldHRlciB1c2VyIGV4cGVyaWVuY2UuXFxuICAgIFByZXBhcmUgeW91ciA8Yj5oZWFkcGhvbmVzPC9iPiBhbmQgd2hlbiB5b3UgYXJlIHJlYWR5IGNsaWNrIG9uIHRoZSBidXR0b24gYmVsb3cuXFxuPC9wPlxcblxcblxcbjxkaXYgY2xhc3M9XFxcInJvd1xcXCIgaWQ9XFxcImJ1dHRvbi1yb3ctaG9tZVxcXCI+XFxuICAgIDxhIGNsYXNzPVxcXCJjb2wteHMtMiBidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCIgZGF0YS1iaW5kPVxcXCJjbGljazogdHJpZ2dlci5iaW5kKCRkYXRhLCdzdGFydC1zdXJ2ZXktZXZlbnQnKVxcXCI+U3RhcnQgU3VydmV5PC9hPlxcbjwvZGl2PlwiO1xuIiwiLypqc2xpbnQgbm9kZTogdHJ1ZSwgbm9tZW46IHRydWUgKi9cblwidXNlIHN0cmljdFwiO1xuXG52YXIga28gPSAodHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvd1sna28nXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ2tvJ10gOiBudWxsKTtcblxuZnVuY3Rpb24gVmlld01vZGVsKHBhcmFtcykge1xuICAgIHZhciBzZWxmID0gdGhpcztcbiAgICBzZWxmLmNvbnRleHQgPSBwYXJhbXMuY29udGV4dDtcblxuICAgIHNlbGYuaW5pdCA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgICAgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICAgICAgICBzZWxmLmNoaWxkcmVuLmZvckVhY2goZnVuY3Rpb24gKGNoaWxkKXtcbiAgICAgICAgICAgIGlmIChjaGlsZCA9PT0gb3B0aW9ucy5tYXNrKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc2VsZi5jb250ZXh0LnZtc1tjaGlsZF0uaW5pdChvcHRpb25zKTtcbiAgICAgICAgfSk7XG4gICAgfTtcblxuICAgIHNlbGYudHJpZ2dlciA9IGZ1bmN0aW9uIChpZCkge1xuICAgICAgICBzZWxmLmNvbnRleHQuZXZlbnRzW2lkXShzZWxmLmNvbnRleHQpO1xuICAgIH07XG59XG5cblZpZXdNb2RlbC5wcm90b3R5cGUuaWQgPSAnaG9tZS1wYWdlJztcblZpZXdNb2RlbC5wcm90b3R5cGUuY2hpbGRyZW4gPSBbXG5dO1xuXG5leHBvcnRzLnJlZ2lzdGVyID0gZnVuY3Rpb24gKCkge1xuICAgIGtvLmNvbXBvbmVudHMucmVnaXN0ZXIoJ2MtaG9tZS1wYWdlJywge1xuICAgICAgICB2aWV3TW9kZWw6IHtcbiAgICAgICAgICAgIGNyZWF0ZVZpZXdNb2RlbDogZnVuY3Rpb24gKHBhcmFtcywgY29tcG9uZW50SW5mbykge1xuICAgICAgICAgICAgICAgIHZhciB2bSA9IG5ldyBWaWV3TW9kZWwocGFyYW1zKTtcbiAgICAgICAgICAgICAgICBwYXJhbXMuY29udGV4dC52bXNbdm0uaWRdID0gdm07XG4gICAgICAgICAgICAgICAgcGFyYW1zLmNvbnRleHQucnVubmluZ0FjdGlvbnNCeUNvbnRhaW5lclt2bS5pZF0gPSBbXTtcbiAgICAgICAgICAgICAgICBrby51dGlscy5kb21Ob2RlRGlzcG9zYWwuYWRkRGlzcG9zZUNhbGxiYWNrKGNvbXBvbmVudEluZm8uZWxlbWVudCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICBwYXJhbXMuY29udGV4dC5ydW5uaW5nQWN0aW9uc0J5Q29udGFpbmVyW3ZtLmlkXS5mb3JFYWNoKGZ1bmN0aW9uIChwcm9taXNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9taXNlLmNhbmNlbCgpO1xuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICBkZWxldGUgcGFyYW1zLmNvbnRleHQucnVubmluZ0FjdGlvbnNCeUNvbnRhaW5lclt2bS5pZF07XG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBwYXJhbXMuY29udGV4dC52bXNbdm0uaWRdO1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHJldHVybiB2bTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdGVtcGxhdGU6IHJlcXVpcmUoJy4vaW5kZXguaHRtbCcpLFxuICAgICAgICBzeW5jaHJvbm91czogdHJ1ZVxuICAgIH0pO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gXCI8ZGl2IGRhdGEtYmluZD1cXFwidmlzaWJsZTogIXN1cnZleUNvbXBsZXRlZCgpXFxcIj5cXG4gICAgPGgzPlN1cnZleTwvaDM+XFxuICAgIDxzdXJ2ZXkgcGFyYW1zPVxcXCJzdXJ2ZXk6IG1vZGVsXFxcIj48L3N1cnZleT5cXG48L2Rpdj5cXG5cXG5cXG48ZGl2IGNsYXNzPVxcXCJyb3dcXFwiIGRhdGEtYmluZD1cXFwidmlzaWJsZTogc3VydmV5Q29tcGxldGVkXFxcIj5cXG48IS0tIDxkaXYgY2xhc3M9XFxcInJvd1xcXCI+IC0tPlxcbiAgICA8cD5cXG4gICAgICAgIFlvdSBoYXZlIGNvbXBsZXRlZCB0aGUgU3VydmV5LiBDbGljayBvbiB0aGUgYnV0dG9uIHRvIGJlZ2luIHRoZSB0ZXN0LlxcbiAgICA8L3A+XFxuICAgIDxhIGNsYXNzPVxcXCJjb2wteHMtMiBidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCIgZGF0YS1iaW5kPVxcXCJjbGljazogdHJpZ2dlci5iaW5kKCRkYXRhLCAnc2VuZC1zdXJ2ZXktZXZlbnQnKVxcXCI+QmVnaW4gVGVzdDwvYT5cXG48L2Rpdj5cIjtcbiIsIi8qanNsaW50IG5vZGU6IHRydWUsIG5vbWVuOiB0cnVlICovXG4vKmdsb2JhbHMgU3VydmV5Ki9cblwidXNlIHN0cmljdFwiO1xuXG52YXIga28gPSAodHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvd1sna28nXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ2tvJ10gOiBudWxsKSxcbiAgICBQcm9taXNlID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJ1Byb21pc2UnXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ1Byb21pc2UnXSA6IG51bGwpO1xuXG5mdW5jdGlvbiBWaWV3TW9kZWwocGFyYW1zKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHNlbGYuY29udGV4dCA9IHBhcmFtcy5jb250ZXh0O1xuICAgIHNlbGYuc3RhdHVzID0ga28ub2JzZXJ2YWJsZSgnJyk7XG4gICAgc2VsZi5maWVsZHMgPSBrby5vYnNlcnZhYmxlKHt9KTtcbiAgICBzZWxmLmVycm9ycyA9IGtvLm9ic2VydmFibGUoe30pO1xuICAgIHNlbGYuc3VydmV5Q29tcGxldGVkID0ga28ub2JzZXJ2YWJsZShmYWxzZSk7XG4gICAgc2VsZi5zdXJ2ZXlKU09OID0ge1xuICAgICAgICBwYWdlczpbe2VsZW1lbnRzOlt7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIkNvbXBsZXRlbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiMlwiLHRleHQ6XCJTdHJvbmdseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIkRpc2FncmVlXCJ9LHt2YWx1ZTpcIjRcIix0ZXh0OlwiTmVpdGhlciBBZ3JlZSBub3IgRGlzYWdyZWVcIn0se3ZhbHVlOlwiNVwiLHRleHQ6XCJBZ3JlZVwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIlN0cm9uZ2x5IEFncmVlXCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiQ29tcGxldGVseSBBZ3JlZVwifV0saXNSZXF1aXJlZDp0cnVlLG5hbWU6XCIxXCIsdGl0bGU6XCJJIHNwZW5kIGEgbG90IG9mIG15IGZyZWUgdGltZSBkb2luZyBtdXNpYy1yZWxhdGVkIGFjdGl2aXRpZXMuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiQ29tcGxldGVseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIlN0cm9uZ2x5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiRGlzYWdyZWVcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCJOZWl0aGVyIEFncmVlIG5vciBEaXNhZ3JlZVwifSx7dmFsdWU6XCI1XCIsdGV4dDpcIkFncmVlXCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiU3Ryb25nbHkgQWdyZWVcIn0se3ZhbHVlOlwiN1wiLHRleHQ6XCJDb21wbGV0ZWx5IEFncmVlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjJcIix0aXRsZTpcIkkgc29tZXRpbWVzIGNob29zZSBtdXNpYyB0aGF0IGNhbiB0cmlnZ2VyIHNoaXZlcnMgZG93biBteSBzcGluZS5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiM1wiLHRpdGxlOlwiSSBlbmpveSB3cml0aW5nIGFib3V0IG11c2ljLCBmb3IgZXhhbXBsZSBvbiBibG9ncyBhbmQgZm9ydW1zLlwifSx7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIkNvbXBsZXRlbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiMlwiLHRleHQ6XCJTdHJvbmdseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIkRpc2FncmVlXCJ9LHt2YWx1ZTpcIjRcIix0ZXh0OlwiTmVpdGhlciBBZ3JlZSBub3IgRGlzYWdyZWVcIn0se3ZhbHVlOlwiNVwiLHRleHQ6XCJBZ3JlZVwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIlN0cm9uZ2x5IEFncmVlXCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiQ29tcGxldGVseSBBZ3JlZVwifV0saXNSZXF1aXJlZDp0cnVlLG5hbWU6XCI0XCIsdGl0bGU6XCJJZiBzb21lYm9keSBzdGFydHMgc2luZ2luZyBhIHNvbmcgSSBkb27igJl0IGtub3csIEkgY2FuIHVzdWFsbHkgam9pbiBpbi5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiNVwiLHRpdGxlOlwiSSBhbSBhYmxlIHRvIGp1ZGdlIHdoZXRoZXIgc29tZW9uZSBpcyBhIGdvb2Qgc2luZ2VyIG9yIG5vdC5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiNlwiLHRpdGxlOlwiSSB1c3VhbGx5IGtub3cgd2hlbiBJ4oCZbSBoZWFyaW5nIGEgc29uZyBmb3IgdGhlIGZpcnN0IHRpbWUuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiQ29tcGxldGVseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIlN0cm9uZ2x5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiRGlzYWdyZWVcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCJOZWl0aGVyIEFncmVlIG5vciBEaXNhZ3JlZVwifSx7dmFsdWU6XCI1XCIsdGV4dDpcIkFncmVlXCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiU3Ryb25nbHkgQWdyZWVcIn0se3ZhbHVlOlwiN1wiLHRleHQ6XCJDb21wbGV0ZWx5IEFncmVlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjdcIix0aXRsZTpcIkkgY2FuIHNpbmcgb3IgcGxheSBtdXNpYyBmcm9tIG1lbW9yeS5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiOFwiLHRpdGxlOlwiSeKAmW0gaW50cmlndWVkIGJ5IG11c2ljYWwgc3R5bGVzIEnigJltIG5vdCBmYW1pbGlhciB3aXRoIGFuZCB3YW50IHRvIGZpbmQgb3V0IG1vcmUuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiQ29tcGxldGVseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIlN0cm9uZ2x5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiRGlzYWdyZWVcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCJOZWl0aGVyIEFncmVlIG5vciBEaXNhZ3JlZVwifSx7dmFsdWU6XCI1XCIsdGV4dDpcIkFncmVlXCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiU3Ryb25nbHkgQWdyZWVcIn0se3ZhbHVlOlwiN1wiLHRleHQ6XCJDb21wbGV0ZWx5IEFncmVlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjlcIix0aXRsZTpcIlBpZWNlcyBvZiBtdXNpYyByYXJlbHkgZXZva2UgZW1vdGlvbnMgZm9yIG1lLlwifSx7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIkNvbXBsZXRlbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiMlwiLHRleHQ6XCJTdHJvbmdseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIkRpc2FncmVlXCJ9LHt2YWx1ZTpcIjRcIix0ZXh0OlwiTmVpdGhlciBBZ3JlZSBub3IgRGlzYWdyZWVcIn0se3ZhbHVlOlwiNVwiLHRleHQ6XCJBZ3JlZVwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIlN0cm9uZ2x5IEFncmVlXCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiQ29tcGxldGVseSBBZ3JlZVwifV0saXNSZXF1aXJlZDp0cnVlLG5hbWU6XCIxMFwiLHRpdGxlOlwiSSBhbSBhYmxlIHRvIGhpdCB0aGUgcmlnaHQgbm90ZXMgd2hlbiBJIHNpbmcgYWxvbmcgd2l0aCBhIHJlY29yZGluZy5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiMTFcIix0aXRsZTpcIkkgZmluZCBpdCBkaWZmaWN1bHQgdG8gc3BvdCBtaXN0YWtlcyBpbiBhIHBlcmZvcm1hbmNlIG9mIGEgc29uZyBldmVuIGlmIEkga25vdyB0aGUgdHVuZS5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiMTJcIix0aXRsZTpcIkkgY2FuIGNvbXBhcmUgYW5kIGRpc2N1c3MgZGlmZmVyZW5jZXMgYmV0d2VlbiB0d28gcGVyZm9ybWFuY2VzIG9yIHZlcnNpb25zIG9mIHRoZSBzYW1lIHBpZWNlIG9mIG11c2ljLlwifSx7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIkNvbXBsZXRlbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiMlwiLHRleHQ6XCJTdHJvbmdseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIkRpc2FncmVlXCJ9LHt2YWx1ZTpcIjRcIix0ZXh0OlwiTmVpdGhlciBBZ3JlZSBub3IgRGlzYWdyZWVcIn0se3ZhbHVlOlwiNVwiLHRleHQ6XCJBZ3JlZVwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIlN0cm9uZ2x5IEFncmVlXCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiQ29tcGxldGVseSBBZ3JlZVwifV0saXNSZXF1aXJlZDp0cnVlLG5hbWU6XCIxM1wiLHRpdGxlOlwiSSBoYXZlIHRyb3VibGUgcmVjb2duaXppbmcgYSBmYW1pbGlhciBzb25nIHdoZW4gcGxheWVkIGluIGEgZGlmZmVyZW50IHdheSBvciBieSBhIGRpZmZlcmVudCBwZXJmb3JtZXIuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiQ29tcGxldGVseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIlN0cm9uZ2x5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiRGlzYWdyZWVcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCJOZWl0aGVyIEFncmVlIG5vciBEaXNhZ3JlZVwifSx7dmFsdWU6XCI1XCIsdGV4dDpcIkFncmVlXCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiU3Ryb25nbHkgQWdyZWVcIn0se3ZhbHVlOlwiN1wiLHRleHQ6XCJDb21wbGV0ZWx5IEFncmVlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjE0XCIsdGl0bGU6XCJJIGhhdmUgbmV2ZXIgYmVlbiBjb21wbGltZW50ZWQgZm9yIG15IHRhbGVudHMgYXMgYSBtdXNpY2FsIHBlcmZvcm1lci5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiMTVcIix0aXRsZTpcIkkgb2Z0ZW4gcmVhZCBvciBzZWFyY2ggdGhlIGludGVybmV0IGZvciB0aGluZ3MgcmVsYXRlZCB0byBtdXNpYy5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiMTZcIix0aXRsZTpcIkkgb2Z0ZW4gcGljayBjZXJ0YWluIG11c2ljIHRvIG1vdGl2YXRlIG9yIGV4Y2l0ZSBtZS5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiMTdcIix0aXRsZTpcIkkgYW0gbm90IGFibGUgdG8gc2luZyBpbiBoYXJtb255IHdoZW4gc29tZWJvZHkgaXMgc2luZ2luZyBhIGZhbWlsaWFyIHR1bmUuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiQ29tcGxldGVseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIlN0cm9uZ2x5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiRGlzYWdyZWVcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCJOZWl0aGVyIEFncmVlIG5vciBEaXNhZ3JlZVwifSx7dmFsdWU6XCI1XCIsdGV4dDpcIkFncmVlXCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiU3Ryb25nbHkgQWdyZWVcIn0se3ZhbHVlOlwiN1wiLHRleHQ6XCJDb21wbGV0ZWx5IEFncmVlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjE4XCIsdGl0bGU6XCJJIGNhbiB0ZWxsIHdoZW4gcGVvcGxlIHNpbmcgb3IgcGxheSBvdXQgb2YgdGltZSB3aXRoIHRoZSBiZWF0LlwifSx7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIkNvbXBsZXRlbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiMlwiLHRleHQ6XCJTdHJvbmdseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIkRpc2FncmVlXCJ9LHt2YWx1ZTpcIjRcIix0ZXh0OlwiTmVpdGhlciBBZ3JlZSBub3IgRGlzYWdyZWVcIn0se3ZhbHVlOlwiNVwiLHRleHQ6XCJBZ3JlZVwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIlN0cm9uZ2x5IEFncmVlXCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiQ29tcGxldGVseSBBZ3JlZVwifV0saXNSZXF1aXJlZDp0cnVlLG5hbWU6XCIxOVwiLHRpdGxlOlwiSSBhbSBhYmxlIHRvIGlkZW50aWZ5IHdoYXQgaXMgc3BlY2lhbCBhYm91dCBhIGdpdmVuIG11c2ljYWwgcGllY2UuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiQ29tcGxldGVseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIlN0cm9uZ2x5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiRGlzYWdyZWVcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCJOZWl0aGVyIEFncmVlIG5vciBEaXNhZ3JlZVwifSx7dmFsdWU6XCI1XCIsdGV4dDpcIkFncmVlXCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiU3Ryb25nbHkgQWdyZWVcIn0se3ZhbHVlOlwiN1wiLHRleHQ6XCJDb21wbGV0ZWx5IEFncmVlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjIwXCIsdGl0bGU6XCJJIGFtIGFibGUgdG8gdGFsayBhYm91dCB0aGUgZW1vdGlvbnMgdGhhdCBhIHBpZWNlIG9mIG11c2ljIGV2b2tlcyBmb3IgbWUuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiQ29tcGxldGVseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIlN0cm9uZ2x5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiRGlzYWdyZWVcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCJOZWl0aGVyIEFncmVlIG5vciBEaXNhZ3JlZVwifSx7dmFsdWU6XCI1XCIsdGV4dDpcIkFncmVlXCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiU3Ryb25nbHkgQWdyZWVcIn0se3ZhbHVlOlwiN1wiLHRleHQ6XCJDb21wbGV0ZWx5IEFncmVlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjIxXCIsdGl0bGU6XCJJIGRvbuKAmXQgc3BlbmQgbXVjaCBvZiBteSBkaXNwb3NhYmxlIGluY29tZSBvbiBtdXNpYy5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiMjJcIix0aXRsZTpcIkkgY2FuIHRlbGwgd2hlbiBwZW9wbGUgc2luZyBvciBwbGF5IG91dCBvZiB0dW5lLlwifSx7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIkNvbXBsZXRlbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiMlwiLHRleHQ6XCJTdHJvbmdseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIkRpc2FncmVlXCJ9LHt2YWx1ZTpcIjRcIix0ZXh0OlwiTmVpdGhlciBBZ3JlZSBub3IgRGlzYWdyZWVcIn0se3ZhbHVlOlwiNVwiLHRleHQ6XCJBZ3JlZVwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIlN0cm9uZ2x5IEFncmVlXCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiQ29tcGxldGVseSBBZ3JlZVwifV0saXNSZXF1aXJlZDp0cnVlLG5hbWU6XCIyM1wiLHRpdGxlOlwiV2hlbiBJIHNpbmcsIEkgaGF2ZSBubyBpZGVhIHdoZXRoZXIgSeKAmW0gaW4gdHVuZSBvciBub3QuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiQ29tcGxldGVseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIlN0cm9uZ2x5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiRGlzYWdyZWVcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCJOZWl0aGVyIEFncmVlIG5vciBEaXNhZ3JlZVwifSx7dmFsdWU6XCI1XCIsdGV4dDpcIkFncmVlXCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiU3Ryb25nbHkgQWdyZWVcIn0se3ZhbHVlOlwiN1wiLHRleHQ6XCJDb21wbGV0ZWx5IEFncmVlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjI0XCIsdGl0bGU6XCJNdXNpYyBpcyBraW5kIG9mIGFuIGFkZGljdGlvbiBmb3IgbWUgLSBJIGNvdWxkbuKAmXQgbGl2ZSB3aXRob3V0IGl0LlwifSx7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIkNvbXBsZXRlbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiMlwiLHRleHQ6XCJTdHJvbmdseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIkRpc2FncmVlXCJ9LHt2YWx1ZTpcIjRcIix0ZXh0OlwiTmVpdGhlciBBZ3JlZSBub3IgRGlzYWdyZWVcIn0se3ZhbHVlOlwiNVwiLHRleHQ6XCJBZ3JlZVwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIlN0cm9uZ2x5IEFncmVlXCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiQ29tcGxldGVseSBBZ3JlZVwifV0saXNSZXF1aXJlZDp0cnVlLG5hbWU6XCIyNVwiLHRpdGxlOlwiSSBkb27igJl0IGxpa2Ugc2luZ2luZyBpbiBwdWJsaWMgYmVjYXVzZSBJ4oCZbSBhZnJhaWQgdGhhdCBJIHdvdWxkIHNpbmcgd3Jvbmcgbm90ZXMuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiQ29tcGxldGVseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIlN0cm9uZ2x5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiRGlzYWdyZWVcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCJOZWl0aGVyIEFncmVlIG5vciBEaXNhZ3JlZVwifSx7dmFsdWU6XCI1XCIsdGV4dDpcIkFncmVlXCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiU3Ryb25nbHkgQWdyZWVcIn0se3ZhbHVlOlwiN1wiLHRleHQ6XCJDb21wbGV0ZWx5IEFncmVlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjI2XCIsdGl0bGU6XCJXaGVuIEkgaGVhciBhIHBpZWNlIG9mIG11c2ljIEkgY2FuIHVzdWFsbHkgaWRlbnRpZnkgaXRzIGdlbnJlLlwifSx7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIkNvbXBsZXRlbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiMlwiLHRleHQ6XCJTdHJvbmdseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIkRpc2FncmVlXCJ9LHt2YWx1ZTpcIjRcIix0ZXh0OlwiTmVpdGhlciBBZ3JlZSBub3IgRGlzYWdyZWVcIn0se3ZhbHVlOlwiNVwiLHRleHQ6XCJBZ3JlZVwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIlN0cm9uZ2x5IEFncmVlXCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiQ29tcGxldGVseSBBZ3JlZVwifV0saXNSZXF1aXJlZDp0cnVlLG5hbWU6XCIyN1wiLHRpdGxlOlwiSSB3b3VsZCBub3QgY29uc2lkZXIgbXlzZWxmIGEgbXVzaS0gY2lhbi5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiMjhcIix0aXRsZTpcIkkga2VlcCB0cmFjayBvZiBuZXcgbXVzaWMgdGhhdCBJIGNvbWUgYWNyb3NzIChlLmcuIG5ldyBhcnRpc3RzIG9yIHJlY29yZGluZ3MpLlwifSx7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIkNvbXBsZXRlbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiMlwiLHRleHQ6XCJTdHJvbmdseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIkRpc2FncmVlXCJ9LHt2YWx1ZTpcIjRcIix0ZXh0OlwiTmVpdGhlciBBZ3JlZSBub3IgRGlzYWdyZWVcIn0se3ZhbHVlOlwiNVwiLHRleHQ6XCJBZ3JlZVwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIlN0cm9uZ2x5IEFncmVlXCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiQ29tcGxldGVseSBBZ3JlZVwifV0saXNSZXF1aXJlZDp0cnVlLG5hbWU6XCIyOVwiLHRpdGxlOlwiQWZ0ZXIgaGVhcmluZyBhIG5ldyBzb25nIHR3byBvciB0aHJlZSB0aW1lcywgSSBjYW4gdXN1YWxseSBzaW5nIGl0IGJ5IG15c2VsZi5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCJDb21wbGV0ZWx5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiU3Ryb25nbHkgRGlzYWdyZWVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCJEaXNhZ3JlZVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIk5laXRoZXIgQWdyZWUgbm9yIERpc2FncmVlXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiQWdyZWVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCJTdHJvbmdseSBBZ3JlZVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIkNvbXBsZXRlbHkgQWdyZWVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiMzBcIix0aXRsZTpcIkkgb25seSBuZWVkIHRvIGhlYXIgYSBuZXcgdHVuZSBvbmNlIGFuZCBJIGNhbiBzaW5nIGl0IGJhY2sgaG91cnMgbGF0ZXIuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiQ29tcGxldGVseSBEaXNhZ3JlZVwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIlN0cm9uZ2x5IERpc2FncmVlXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiRGlzYWdyZWVcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCJOZWl0aGVyIEFncmVlIG5vciBEaXNhZ3JlZVwifSx7dmFsdWU6XCI1XCIsdGV4dDpcIkFncmVlXCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiU3Ryb25nbHkgQWdyZWVcIn0se3ZhbHVlOlwiN1wiLHRleHQ6XCJDb21wbGV0ZWx5IEFncmVlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjMxXCIsdGl0bGU6XCJNdXNpYyBjYW4gZXZva2UgbXkgbWVtb3JpZXMgb2YgcGFzdCBwZW9wbGUgYW5kIHBsYWNlcy5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCIwXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiMVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIjJcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCIzXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiNCAtIDVcIn0se3ZhbHVlOlwiNlwiLHRleHQ6XCI2IC0gOVwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIjEwIG9yIG1vcmVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiMzJcIix0aXRsZTpcIkkgZW5nYWdlZCBpbiByZWd1bGFyLCBkYWlseSBwcmFjdGljZSBvZiBhIG11c2ljYWwgaW5zdHJ1bWVudCAoaW5jbHVkaW5nIHZvaWNlKSBmb3IgX19fIHllYXJzLlwifSx7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIjBcIn0se3ZhbHVlOlwiMlwiLHRleHQ6XCIwLjVcIn0se3ZhbHVlOlwiM1wiLHRleHQ6XCIxXCJ9LHt2YWx1ZTpcIjRcIix0ZXh0OlwiMS41XCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiMlwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIjMgLSA0XCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiNSBvciBtb3JlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjMzXCIsdGl0bGU6XCJBdCB0aGUgcGVhayBvZiBteSBpbnRlcmVzdCwgSSBwcmFjdGljZWQgX19fIGhvdXJzIHBlciBkYXkgb24gbXkgcHJpbWFyeSBpbnN0cnVtZW50LlwifSx7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIjBcIn0se3ZhbHVlOlwiMlwiLHRleHQ6XCIxXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiMlwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIjNcIn0se3ZhbHVlOlwiNVwiLHRleHQ6XCI0IC0gNlwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIjcgLSAxMFwifSx7dmFsdWU6XCI3XCIsdGV4dDpcIjExIG9yIG1vcmVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiMzRcIix0aXRsZTpcIkkgaGF2ZSBhdHRlbmRlZCBfX18gbGl2ZSBtdXNpYyBldmVudHMgYXMgYW4gYXVkaWVuY2UgbWVtYmVyIGluIHRoZSBwYXN0IHR3ZWx2ZSBtb250aHMuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiMFwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIjAuNVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIjFcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCIyXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiM1wifSx7dmFsdWU6XCI2XCIsdGV4dDpcIjQgLSA2XCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiNyBvciBtb3JlXCJ9XSxpc1JlcXVpcmVkOnRydWUsbmFtZTpcIjM1XCIsdGl0bGU6XCJJIGhhdmUgaGFkIGZvcm1hbCB0cmFpbmluZyBpbiBtdXNpYyB0aGVvcnkgZm9yIF9fXyB5ZWFycy5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCIwXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiMC41XCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiMVwifSx7dmFsdWU6XCI0XCIsdGV4dDpcIjJcIn0se3ZhbHVlOlwiNVwiLHRleHQ6XCIzIC01XCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiNiAtIDkgXCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiMTAgb3IgbW9yZVwifV0saXNSZXF1aXJlZDp0cnVlLG5hbWU6XCIzNlwiLHRpdGxlOlwiSSBoYXZlIGhhZCBfX18geWVhcnMgb2YgZm9ybWFsIHRyYWluaW5nIG9uIGEgbXVzaWNhbCBpbnN0cnVtZW50IChpbmNsdWRpbmcgdm9pY2UpIGR1cmluZyBteSBsaWZldGltZS5cIn0se3R5cGU6XCJyYWRpb2dyb3VwXCIsY2hvaWNlczpbe3ZhbHVlOlwiMVwiLHRleHQ6XCIwXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiMVwifSx7dmFsdWU6XCIzXCIsdGV4dDpcIjJcIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCIzXCJ9LHt2YWx1ZTpcIjVcIix0ZXh0OlwiNFwifSx7dmFsdWU6XCI2XCIsdGV4dDpcIjVcIn0se3ZhbHVlOlwiN1wiLHRleHQ6XCI2IG9yIG1vcmVcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiMzdcIix0aXRsZTpcIkkgY2FuIHBsYXkgX19fIG11c2ljYWwgaW5zdHJ1bWVudHMuXCJ9LHt0eXBlOlwicmFkaW9ncm91cFwiLGNob2ljZXM6W3t2YWx1ZTpcIjFcIix0ZXh0OlwiMCAtIDE1IG1pblwifSx7dmFsdWU6XCIyXCIsdGV4dDpcIjE1IC0gMzAgbWluXCJ9LHt2YWx1ZTpcIjNcIix0ZXh0OlwiMzAgLSA2MCBtaW5cIn0se3ZhbHVlOlwiNFwiLHRleHQ6XCI2MCAtIDkwIG1pblwifSx7dmFsdWU6XCI1XCIsdGV4dDpcIjIgaHJzXCJ9LHt2YWx1ZTpcIjZcIix0ZXh0OlwiMiAtIDMgaHJzXCJ9LHt2YWx1ZTpcIjdcIix0ZXh0OlwiNCBocnMgb3IgbW9yZVwifV0saXNSZXF1aXJlZDp0cnVlLG5hbWU6XCIzOFwiLHRpdGxlOlwiSSBsaXN0ZW4gYXR0ZW50aXZlbHkgdG8gbXVzaWMgZm9yIF9fXyBwZXIgZGF5LlwifSx7dHlwZTpcInJhZGlvZ3JvdXBcIixjaG9pY2VzOlt7dmFsdWU6XCIxXCIsdGV4dDpcIlBvcC9Sb2NrIE11c2ljXCJ9LHt2YWx1ZTpcIjJcIix0ZXh0OlwiSmF6eiBNdXNpY1wifSx7dmFsdWU6XCIzXCIsdGV4dDpcIkNsYXNzaWNhbCBNdXNpY1wifSx7dmFsdWU6XCI0XCIsdGV4dDpcIkV2ZXJ5dGhpbmdcIn1dLGlzUmVxdWlyZWQ6dHJ1ZSxuYW1lOlwiSSB1c3VhbGx5IGxpc3RlbiB0byBfX19cIn1dLG5hbWU6XCJwYWdlMVwifV1cbiAgICB9O1xuXG4gICAgU3VydmV5LmRlZmF1bHRCb290c3RyYXBDc3MubmF2aWdhdGlvbkJ1dHRvbiA9ICdidG4gYnRuLXByaW1hcnknO1xuICAgIFN1cnZleS5TdXJ2ZXkuY3NzVHlwZSA9ICdib290c3RyYXAnO1xuICAgIHNlbGYubW9kZWwgPSBuZXcgU3VydmV5Lk1vZGVsKHNlbGYuc3VydmV5SlNPTik7XG4gICAgc2VsZi5zYXZlU3VydmV5UmVzdWx0cyA9IGZ1bmN0aW9uIChzdXJ2ZXkpe1xuICAgICAgICAvLyB2YXIgcmVzdWx0QXNTdHJpbmcgPSBKU09OLnN0cmluZ2lmeShzdXJ2ZXkuZGF0YSk7XG4gICAgICAgIC8vIGFsZXJ0KHJlc3VsdEFzU3RyaW5nKTsgLy9zZW5kIEFqYXggcmVxdWVzdCB0byB5b3VyIHdlYiBzZXJ2ZXIuXG4gICAgICAgIHNlbGYuZmllbGRzKClbJ3N1cnZleSBhbnN3ZXJzJ10oc3VydmV5LmRhdGEpO1xuICAgICAgICBzZWxmLnN1cnZleUNvbXBsZXRlZCh0cnVlKTtcbiAgICB9O1xuICAgIC8vVXNlIG9uQ29tcGxldGUgZXZlbnQgdG8gc2F2ZSB0aGUgZGF0YVxuICAgIHNlbGYubW9kZWwub25Db21wbGV0ZS5hZGQoc2VsZi5zYXZlU3VydmV5UmVzdWx0cyk7XG5cbiAgICBzZWxmLnRyaWdnZXIgPSBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgc2VsZi5jb250ZXh0LmV2ZW50c1tpZF0oc2VsZi5jb250ZXh0LCBzZWxmLm91dHB1dCk7XG4gICAgfTtcbn1cblxuVmlld01vZGVsLnByb3RvdHlwZS5pZCA9ICdzdXJ2ZXktZm9ybSc7XG5cblZpZXdNb2RlbC5wcm90b3R5cGUud2FpdEZvclN0YXR1c0NoYW5nZSA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5faW5pdGlhbGl6aW5nIHx8XG4gICAgICAgICAgIFByb21pc2UucmVzb2x2ZSgpO1xufTtcblxuVmlld01vZGVsLnByb3RvdHlwZS5fY29tcHV0ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLm91dHB1dCA9IHtcbiAgICAgICAgJ3N1cnZleSBhbnN3ZXJzJzogdGhpcy5pbnB1dFsnc3VydmV5IGFuc3dlcnMnXSxcbiAgICAgICAgJ3VzZXIgaWQnOiB0aGlzLmlucHV0Wyd1c2VyIGlkJ10sXG4gICAgfVxuICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgICAgZmllbGRzID0ge1xuICAgICAgICAgICAgJ3N1cnZleSBhbnN3ZXJzJzoga28ub2JzZXJ2YWJsZSh0aGlzLmlucHV0WydzdXJ2ZXkgYW5zd2VycyddKSxcbiAgICAgICAgICAgICd1c2VyIGlkJzoga28ub2JzZXJ2YWJsZSh0aGlzLmlucHV0Wyd1c2VyIGlkJ10pLFxuICAgICAgICB9LFxuICAgICAgICBlcnJvcnMgPSB7XG4gICAgICAgICAgICAnc3VydmV5IGFuc3dlcnMnOiBrby5vYnNlcnZhYmxlKHRoaXMuaW5wdXRbJ3N1cnZleSBhbnN3ZXJzLWVycm9yJ10pLFxuICAgICAgICAgICAgJ3VzZXIgaWQnOiBrby5vYnNlcnZhYmxlKHRoaXMuaW5wdXRbJ3VzZXIgaWQtZXJyb3InXSksXG4gICAgICAgIH07XG4gICAgZmllbGRzWydzdXJ2ZXkgYW5zd2VycyddLnN1YnNjcmliZShmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgc2VsZi5vdXRwdXRbJ3N1cnZleSBhbnN3ZXJzJ10gPSB2YWx1ZTtcbiAgICAgICAgc2VsZi5lcnJvcnMoKVsnc3VydmV5IGFuc3dlcnMnXSh1bmRlZmluZWQpO1xuICAgIH0pO1xuICAgIGZpZWxkc1sndXNlciBpZCddLnN1YnNjcmliZShmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgc2VsZi5vdXRwdXRbJ3VzZXIgaWQnXSA9IHZhbHVlO1xuICAgICAgICBzZWxmLmVycm9ycygpWyd1c2VyIGlkJ10odW5kZWZpbmVkKTtcbiAgICB9KTtcbiAgICB0aGlzLmZpZWxkcyhmaWVsZHMpO1xuICAgIHRoaXMuZXJyb3JzKGVycm9ycyk7XG4gICAgdGhpcy5zdGF0dXMoJ2NvbXB1dGVkJyk7XG59O1xuXG5cblZpZXdNb2RlbC5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG4gICAgdGhpcy5vdXRwdXQgPSB1bmRlZmluZWQ7XG4gICAgdGhpcy5maWVsZHMoe30pO1xuICAgIHRoaXMuZXJyb3JzKHt9KTtcbiAgICB0aGlzLmlucHV0ID0gb3B0aW9ucy5pbnB1dCB8fCB7fTtcbiAgICB0aGlzLnN0YXR1cygncmVhZHknKTtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgdGhpcy5faW5pdGlhbGl6aW5nID0gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUpIHtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBzZWxmLl9jb21wdXRlKCk7XG4gICAgICAgICAgICByZXNvbHZlKCk7XG4gICAgICAgICAgICBzZWxmLl9pbml0aWFsaXppbmcgPSB1bmRlZmluZWQ7XG4gICAgICAgIH0sIDEpO1xuICAgIH0pO1xufTtcblxuZXhwb3J0cy5yZWdpc3RlciA9IGZ1bmN0aW9uICgpIHtcbiAgICBrby5jb21wb25lbnRzLnJlZ2lzdGVyKCdjLXN1cnZleS1mb3JtJywge1xuICAgICAgICB2aWV3TW9kZWw6IHtcbiAgICAgICAgICAgIGNyZWF0ZVZpZXdNb2RlbDogZnVuY3Rpb24gKHBhcmFtcywgY29tcG9uZW50SW5mbykge1xuICAgICAgICAgICAgICAgIHZhciB2bSA9IG5ldyBWaWV3TW9kZWwocGFyYW1zKTtcbiAgICAgICAgICAgICAgICBwYXJhbXMuY29udGV4dC52bXNbdm0uaWRdID0gdm07XG4gICAgICAgICAgICAgICAga28udXRpbHMuZG9tTm9kZURpc3Bvc2FsLmFkZERpc3Bvc2VDYWxsYmFjayhjb21wb25lbnRJbmZvLmVsZW1lbnQsIGZ1bmN0aW9uICgpIHsgZGVsZXRlIHBhcmFtcy5jb250ZXh0LnZtc1t2bS5pZF07IH0pO1xuICAgICAgICAgICAgICAgIHJldHVybiB2bTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgdGVtcGxhdGU6IHJlcXVpcmUoJy4vaW5kZXguaHRtbCcpLFxuICAgICAgICBzeW5jaHJvbm91czogdHJ1ZVxuICAgIH0pO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gXCI8c3Bhbj5cXG4gICAgPCEtLSBTdXJ2ZXkgLS0+XFxuICAgIDxjLXN1cnZleS1mb3JtIHBhcmFtcz1cXFwiY29udGV4dDogY29udGV4dFxcXCI+PC9jLXN1cnZleS1mb3JtPlxcbjwvc3Bhbj5cIjtcbiIsIi8qanNsaW50IG5vZGU6IHRydWUsIG5vbWVuOiB0cnVlICovXG5cInVzZSBzdHJpY3RcIjtcblxudmFyIGtvID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJ2tvJ10gOiB0eXBlb2YgZ2xvYmFsICE9PSBcInVuZGVmaW5lZFwiID8gZ2xvYmFsWydrbyddIDogbnVsbCk7XG5cbmZ1bmN0aW9uIFZpZXdNb2RlbChwYXJhbXMpIHtcbiAgICB2YXIgc2VsZiA9IHRoaXM7XG4gICAgc2VsZi5jb250ZXh0ID0gcGFyYW1zLmNvbnRleHQ7XG5cbiAgICBzZWxmLmluaXQgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgICAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgICAgICAgc2VsZi5jaGlsZHJlbi5mb3JFYWNoKGZ1bmN0aW9uIChjaGlsZCl7XG4gICAgICAgICAgICBpZiAoY2hpbGQgPT09IG9wdGlvbnMubWFzaykge1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHNlbGYuY29udGV4dC52bXNbY2hpbGRdLmluaXQob3B0aW9ucyk7XG4gICAgICAgIH0pO1xuICAgIH07XG5cbiAgICBzZWxmLnRyaWdnZXIgPSBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgc2VsZi5jb250ZXh0LmV2ZW50c1tpZF0oc2VsZi5jb250ZXh0KTtcbiAgICB9O1xufVxuXG5WaWV3TW9kZWwucHJvdG90eXBlLmlkID0gJ3N1cnZleS12aWV3JztcblZpZXdNb2RlbC5wcm90b3R5cGUuY2hpbGRyZW4gPSBbXG4gICAgJ3N1cnZleS1mb3JtJyAvLyBTdXJ2ZXlcbl07XG5cbmV4cG9ydHMucmVnaXN0ZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAga28uY29tcG9uZW50cy5yZWdpc3RlcignYy1zdXJ2ZXktdmlldycsIHtcbiAgICAgICAgdmlld01vZGVsOiB7XG4gICAgICAgICAgICBjcmVhdGVWaWV3TW9kZWw6IGZ1bmN0aW9uIChwYXJhbXMsIGNvbXBvbmVudEluZm8pIHtcbiAgICAgICAgICAgICAgICB2YXIgdm0gPSBuZXcgVmlld01vZGVsKHBhcmFtcyk7XG4gICAgICAgICAgICAgICAgcGFyYW1zLmNvbnRleHQudm1zW3ZtLmlkXSA9IHZtO1xuICAgICAgICAgICAgICAgIHBhcmFtcy5jb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbdm0uaWRdID0gW107XG4gICAgICAgICAgICAgICAga28udXRpbHMuZG9tTm9kZURpc3Bvc2FsLmFkZERpc3Bvc2VDYWxsYmFjayhjb21wb25lbnRJbmZvLmVsZW1lbnQsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zLmNvbnRleHQucnVubmluZ0FjdGlvbnNCeUNvbnRhaW5lclt2bS5pZF0uZm9yRWFjaChmdW5jdGlvbiAocHJvbWlzZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcHJvbWlzZS5jYW5jZWwoKTtcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgZGVsZXRlIHBhcmFtcy5jb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbdm0uaWRdO1xuICAgICAgICAgICAgICAgICAgICBkZWxldGUgcGFyYW1zLmNvbnRleHQudm1zW3ZtLmlkXTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdm07XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIHRlbXBsYXRlOiByZXF1aXJlKCcuL2luZGV4Lmh0bWwnKSxcbiAgICAgICAgc3luY2hyb25vdXM6IHRydWVcbiAgICB9KTtcbn07XG4iLCJtb2R1bGUuZXhwb3J0cyA9IFwiPGRpdiBpZD1cXFwibG9hZGluZy1vdmVybGF5XFxcIiBkYXRhLWJpbmQ9XFxcInZpc2libGU6IGxvYWRpbmdcXFwiPmxvYWRpbmcuLi48L2Rpdj5cXG5cXG48ZGl2IGRhdGEtYmluZD1cXFwidmlzaWJsZTogcmVhZHlcXFwiPlxcblxcbiAgICA8ZGl2IGRhdGEtYmluZD1cXFwicHJvZ3Jlc3M6IHsgdmFsdWU6IHBlcmNlbnRhZ2VWYWx1ZSwgdHlwZTogJ2luZm8nLCB0ZXh0OiAnQ29tcGxldGUnLCB0ZXh0SGlkZGVuOiBmYWxzZSwgYW5pbWF0ZWQ6IHRydWUsIHN0cmlwZWQ6IHRydWUgfVxcXCI+PC9kaXY+XFxuXFxuICAgIDxoMz5DaG9vc2UgeW91ciBmYXZvdXJpdGUgY29tcGxleGl0eSB2YWx1ZSEgKFRlc3Qgbi4gPHNwYW4gZGF0YS1iaW5kPVxcXCJ0ZXh0OiB0ZXN0TnVtYmVyXFxcIj48L3NwYW4+ICkgPC9oMz5cXG4gICAgPHA+XFxuICAgICAgICBFdmVyeSBwb3NpdGlvbiBvZiB0aGUgc2xpZGVyIGlzIGFzc29jaWF0ZWQgd2l0aCBzZXF1ZW5jZXMgb2YgY2hvcmRzIHRoYXQgeW91IHdpbGwgZmluZCBsZXNzIG9yIG1vcmUgY29tcGxleC4gUHJlc3MgdGhlIFxcXCJwbGF5XFxcIiBidXR0b24gYW5kIG1vdmUgdGhlIHNsaWRlciBiZWxvdyB1bnRpbCB5b3UgZmluZCB0aGUgc2V0IG9mIGNob3JkIHByb2dyZXNzaW9ucyB0aGF0IHlvdSBsaWtlIG1vc3QuXFxuICAgICAgICBUaGVuIGNsaWNrIHRoZSBidXR0b24gXFxcIk5leHQgVGVzdFxcXCIgYW5kIHdhaXQgZm9yIHRoZSBuZXcgdGVzdCB0byBsb2FkLlxcbiAgICA8L3A+XFxuICAgIDxwPlxcbiAgICAgICAgV2hlbiB5b3UgbW92ZSB0aGUgc2xpZGVyIHRoZSBwcm9ncmVzc2lvbiB3aWxsIHN0b3AgYW5kIHlvdSBuZWVkIHRvIHByZXNzIFxcXCJwbGF5XFxcIiB0byBsaXN0ZW4gdG8gdGhlIG5leHQgcHJvZ3Jlc3Npb24uXFxuICAgIDwvcD5cXG5cXG4gICAgPGRpdiBjbGFzcz1cXFwiY29sLXhzLTEyXFxcIiBzdHlsZT1cXFwiaGVpZ2h0OjUwcHhcXFwiPjwvZGl2PlxcblxcbiAgICA8ZGl2IGNsYXNzPVxcXCJyb3dcXFwiPlxcbiAgICAgICAgPGRpdiBpZD1cXFwiaWNvbjFcXFwiPlxcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcImljb24xXFxcIiBkYXRhLWJpbmQ9XFxcInZpc2libGU6ICFpc1BsYXlpbmcoKVxcXCI+XFxuICAgICAgICAgICAgPCEtLSA8YSBpZD1cXFwicGxheS1pY29uXFxcIiBjbGFzcz1cXFwiZ2x5cGhpY29uIGdseXBoaWNvbi1wbGF5XFxcIiBkYXRhLWJpbmQ9XFxcImNsaWNrOiBwbGF5XFxcIj5QbGF5PC9hPiAtLT5cXG4gICAgICAgICAgICAgICAgPCEtLSA8YSB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgYnRuLWNpcmNsZSBidG4tbGdcXFwiIGRhdGEtYmluZD1cXFwiY2xpY2s6IHBsYXksIGVuYWJsZTogcGxheUJ1dHRvbkVuYWJsZWRcXFwiPjxpIGNsYXNzPVxcXCJmYSBmYS1wbGF5IGZhLTR4XFxcIj48L2k+PC9hPiAtLT5cXG4gICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cXFwiYnRuIGJ0bi1tZCBidG4tZGVmYXVsdFxcXCIgZGF0YS1iaW5kPVxcXCJjbGljazogcGxheSwgZW5hYmxlOiBwbGF5QnV0dG9uRW5hYmxlZFxcXCI+PGkgY2xhc3M9XFxcImZhIGZhLXBsYXkgZmEtM3hcXFwiPjwvaT48L2J1dHRvbj5cXG5cXG4gICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJpY29uMVxcXCIgZGF0YS1iaW5kPVxcXCJ2aXNpYmxlOiBpc1BsYXlpbmcoKVxcXCI+XFxuICAgICAgICAgICAgPCEtLSA8YSBjbGFzcz1cXFwiZ2x5cGhpY29uIGdseXBoaWNvbi1wYXVzZVxcXCIgZGF0YS1iaW5kPVxcXCJjbGljazogcGF1c2VcXFwiPlBhdXNlPC9hPiAtLT5cXG4gICAgICAgICAgICAgICAgPCEtLSA8YnV0dG9uIGRhdGEtYmluZD1cXFwiY2xpY2s6IHBhdXNlXFxcIj48aSBjbGFzcz1cXFwiZmEgZmEtcGF1c2UtY2lyY2xlIGZhLTR4XFxcIiBhcmlhLWhpZGRlbj1cXFwidHJ1ZVxcXCI+PC9pPjwvYT4gLS0+XFxuICAgICAgICAgICAgICAgIDxidXR0b24gY2xhc3M9XFxcImJ0biBidG4tbWQgYnRuLWRlZmF1bHRcXFwiIGRhdGEtYmluZD1cXFwiY2xpY2s6IHBhdXNlXFxcIj48aSBjbGFzcz1cXFwiZmEgZmEtcGF1c2UgZmEtM3hcXFwiPjwvaT48L2J1dHRvbj5cXG4gICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgIDwvZGl2PlxcbiAgICA8L2Rpdj5cXG5cXG4gICAgPGRpdiBjbGFzcz1cXFwicm93XFxcIj5cXG4gICAgICAgIDxkaXYgaWQ9XFxcInRlc3Qtcm93XFxcIj5cXG4gICAgICAgICAgICA8Yj5zaW1wbGU8L2I+XFxuICAgICAgICAgICAgPGlucHV0IGRhdGEtc2xpZGVyLWlkPVxcXCJzbGlkZXIxXFxcIiBkYXRhLXNsaWRlci10b29sdGlwPVxcXCJoaWRlXFxcIiB0eXBlPVxcXCJ0ZXh0XFxcIiBkYXRhLXNsaWRlci1taW49XFxcIjBcXFwiIGRhdGEtc2xpZGVyLW1heD1cXFwiMjlcXFwiIGRhdGEtc2xpZGVyLXN0ZXA9XFxcIjFcXFwiIGRhdGEtc2xpZGVyLXZhbHVlPVxcXCIwXFxcIiBkYXRhLWJpbmQ9XFxcInNsaWRlclZhbHVlOnt2YWx1ZTogc2xpZGVyVmFsdWV9XFxcIj5cXG4gICAgICAgICAgICA8Yj5jb21wbGV4PC9iPlxcbiAgICAgICAgPC9kaXY+XFxuICAgIDwvZGl2PlxcblxcbiAgICA8ZGl2IGNsYXNzPVxcXCJjb2wteHMtMTJcXFwiIHN0eWxlPVxcXCJoZWlnaHQ6NTBweFxcXCI+PC9kaXY+XFxuXFxuICAgIDxkaXYgY2xhc3M9XFxcInN0ZXB3aXphcmRcXFwiPlxcbiAgICAgICAgPGRpdiBjbGFzcz1cXFwic3RlcHdpemFyZC1yb3dcXFwiPlxcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInN0ZXB3aXphcmQtc3RlcFxcXCI+XFxuICAgICAgICAgICAgICAgIDxhIHR5cGU9XFxcImJ1dHRvblxcXCIgY2xhc3M9XFxcImJ0biBidG4tZGVmYXVsdCBidG4tY2lyY2xlXFxcIiBkYXRhLWJpbmQ9XFxcImNzczogYnV0dG9uMFN0YXR1c1xcXCIgZGlzYWJsZWQ9XFxcImRpc2FibGVkXFxcIj4xPC9hPlxcbiAgICAgICAgICAgICAgICA8IS0tIDxwPjwvcD4gLS0+XFxuICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwic3RlcHdpemFyZC1zdGVwXFxcIj5cXG4gICAgICAgICAgICAgICAgPGEgdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IGJ0bi1jaXJjbGVcXFwiIGRhdGEtYmluZD1cXFwiY3NzOiBidXR0b24xU3RhdHVzXFxcIiBkaXNhYmxlZD1cXFwiZGlzYWJsZWRcXFwiPjI8L2E+XFxuICAgICAgICAgICAgICAgIDxwPjwvcD5cXG4gICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJzdGVwd2l6YXJkLXN0ZXBcXFwiPlxcbiAgICAgICAgICAgICAgICA8YSB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgYnRuLWNpcmNsZVxcXCIgZGF0YS1iaW5kPVxcXCJjc3M6YnV0dG9uMlN0YXR1c1xcXCIgZGlzYWJsZWQ9XFxcImRpc2FibGVkXFxcIj4zPC9hPlxcbiAgICAgICAgICAgICAgICA8cD48L3A+XFxuICAgICAgICAgICAgPC9kaXY+XFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwic3RlcHdpemFyZC1zdGVwXFxcIj5cXG4gICAgICAgICAgICAgICAgPGEgdHlwZT1cXFwiYnV0dG9uXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1kZWZhdWx0IGJ0bi1jaXJjbGVcXFwiIGRhdGEtYmluZD1cXFwiY3NzOiBidXR0b24zU3RhdHVzXFxcIiBkaXNhYmxlZD1cXFwiZGlzYWJsZWRcXFwiPjQ8L2E+XFxuICAgICAgICAgICAgICAgIDxwPjwvcD5cXG4gICAgICAgICAgICA8L2Rpdj5cXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJzdGVwd2l6YXJkLXN0ZXBcXFwiPlxcbiAgICAgICAgICAgICAgICA8YSB0eXBlPVxcXCJidXR0b25cXFwiIGNsYXNzPVxcXCJidG4gYnRuLWRlZmF1bHQgYnRuLWNpcmNsZVxcXCIgZGF0YS1iaW5kPVxcXCJjc3M6IGJ1dHRvbjRTdGF0dXNcXFwiIGRpc2FibGVkPVxcXCJkaXNhYmxlZFxcXCI+NTwvYT5cXG4gICAgICAgICAgICAgICAgPHA+PC9wPlxcbiAgICAgICAgICAgIDwvZGl2PlxcbiAgICAgICAgPC9kaXY+XFxuICAgIDwvZGl2PlxcblxcblxcbiAgICA8IS0tIDxkaXYgY2xhc3MgPVxcXCJyb3dcXFwiPlxcbiAgICAgICAgPGRpdiBkYXRhLWJpbmQ9XFxcInZpc2libGU6ICFpc1BsYXlpbmcoKVxcXCI+XFxuICAgICAgICAgICAgPCEtLSA8YSBpZD1cXFwicGxheS1pY29uXFxcIiBjbGFzcz1cXFwiZ2x5cGhpY29uIGdseXBoaWNvbi1wbGF5XFxcIiBkYXRhLWJpbmQ9XFxcImNsaWNrOiBwbGF5XFxcIj5QbGF5PC9hPlxcbiAgICAgICAgICAgIDxhIGRhdGEtYmluZD1cXFwiY2xpY2s6IHBsYXlcXFwiPjxpIGNsYXNzPVxcXCJmYSBmYS1wbGF5LWNpcmNsZSBmYS00eFxcXCIgYXJpYS1oaWRkZW49XFxcInRydWVcXFwiPjwvaT48L2E+XFxuICAgICAgICA8L2Rpdj5cXG4gICAgICAgIDxkaXYgZGF0YS1iaW5kPVxcXCJ2aXNpYmxlOiBpc1BsYXlpbmcoKVxcXCI+XFxuICAgICAgICAgICAgPCEtLSA8YSBjbGFzcz1cXFwiZ2x5cGhpY29uIGdseXBoaWNvbi1wYXVzZVxcXCIgZGF0YS1iaW5kPVxcXCJjbGljazogcGF1c2VcXFwiPlBhdXNlPC9hPlxcbiAgICAgICAgICAgIDxhIGRhdGEtYmluZD1cXFwiY2xpY2s6IHBsYXlcXFwiPjxpIGNsYXNzPVxcXCJmYSBmYS1wYXVzZS1jaXJjbGUgZmEtNHhcXFwiIGFyaWEtaGlkZGVuPVxcXCJ0cnVlXFxcIj48L2k+PC9hPlxcbiAgICAgICAgPC9kaXY+XFxuICAgIDwvZGl2PiAtLT5cXG5cXG4gICAgPGRpdiBjbGFzcz1cXFwicm93XFxcIiBpZD1cXFwiYnV0dG9uLXJvd1xcXCI+XFxuICAgICAgICA8ZGl2IGRhdGEtYmluZD1cXFwidmlzaWJsZTogaXNMYXN0VGVzdCgpXFxcIj5cXG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVxcXCJjb2wteHMtMiBidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCIgZGF0YS1iaW5kPVxcXCJjbGljazogc2VuZFByZWZlcmVuY2UsIGRpc2FibGUgOiBkaXNhYmxlTmV4dEJ1dHRvblxcXCI+U2VuZCBQcmVmZXJlbmNlPC9idXR0b24+XFxuICAgICAgICA8L2Rpdj5cXG4gICAgICAgIDxkaXYgZGF0YS1iaW5kPVxcXCJ2aXNpYmxlOiAhaXNMYXN0VGVzdCgpXFxcIj5cXG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVxcXCJjb2wteHMtMiBidG4gYnRuLXByaW1hcnkgcHVsbC1yaWdodFxcXCIgZGF0YS1iaW5kPVxcXCJjbGljazogbmV4dFRlc3QsIGRpc2FibGUgOiBkaXNhYmxlTmV4dEJ1dHRvblxcXCI+TmV4dCBUZXN0PC9idXR0b24+XFxuICAgICAgICA8L2Rpdj5cXG4gICAgPC9kaXY+XFxuXFxuXFxuPC9kaXY+XCI7XG4iLCIvKmpzbGludCBub2RlOiB0cnVlLCBub21lbjogdHJ1ZSAqL1xuLypqc2hpbnQgc3ViOnRydWUqL1xuLypnbG9iYWxzIEF1ZGlvQ29udGV4dCovXG4vKmdsb2JhbHMgU291bmRmb250Ki9cblwidXNlIHN0cmljdFwiO1xuXG52YXIga28gPSAodHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvd1sna28nXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ2tvJ10gOiBudWxsKSxcbiAgICBQcm9taXNlID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJ1Byb21pc2UnXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ1Byb21pc2UnXSA6IG51bGwpO1xuXG5mdW5jdGlvbiBWaWV3TW9kZWwocGFyYW1zKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHNlbGYuY29udGV4dCA9IHBhcmFtcy5jb250ZXh0O1xuICAgIHNlbGYuc3RhdHVzID0ga28ub2JzZXJ2YWJsZSgnJyk7XG4gICAgc2VsZi5maWVsZHMgPSBrby5vYnNlcnZhYmxlKHt9KTtcbiAgICBzZWxmLmVycm9ycyA9IGtvLm9ic2VydmFibGUoe30pO1xuICAgIHNlbGYucGlhbm8gPSB1bmRlZmluZWQ7XG4gICAgc2VsZi5sb2FkaW5nID0ga28ub2JzZXJ2YWJsZSh0cnVlKTsgIC8vdXNlZCB0byBkaXNwbGF5IFwiTG9hZGluZ1wiIHRleHRcbiAgICBzZWxmLnJlYWR5ID0ga28ub2JzZXJ2YWJsZShmYWxzZSk7IC8vdXNlZCB0byBkaXNwbGF5IHRoZSBwYWdlIHdoZW4gdGhlIHNhbXBsZXMgYXJlIGxvYWRlZFxuICAgIHNlbGYuYWMgPSBuZXcgQXVkaW9Db250ZXh0KCk7XG4gICAgc2VsZi5zbGlkZXJWYWx1ZSA9IGtvLm9ic2VydmFibGUoMCk7XG4gICAgc2VsZi5pc1BsYXlpbmcgPSAga28ub2JzZXJ2YWJsZShmYWxzZSk7IC8vdXNlZCB0byBzdG9wIHRoZSBpbmZpbml0ZSBwbGF5aW5nIHdoZW4gd2Ugd2FudCB0byBsZWZ0IHRoZSBwYWdlXG4gICAgc2VsZi5wbGF5QnV0dG9uRW5hYmxlZCA9IGtvLm9ic2VydmFibGUodHJ1ZSk7IC8vdXNlZCB0byBhdm9pZCByYXBpZCBkb3VibGUgY2xpY2sgb24gcGxheVxuICAgIHNlbGYuaXNMYXN0VGVzdCA9IGtvLm9ic2VydmFibGUoZmFsc2UpO1xuICAgIHNlbGYucGVyY2VudGFnZVZhbHVlID0ga28ub2JzZXJ2YWJsZSgwKTtcbiAgICBzZWxmLnRlc3ROdW1iZXIgPSBrby5vYnNlcnZhYmxlKDEpO1xuICAgIHNlbGYuaXNTeW5jID0ga28ub2JzZXJ2YWJsZSh0cnVlKTtcbiAgICBzZWxmLmlzQ2hvcmRQbGF5ZWQgPSBba28ub2JzZXJ2YWJsZShmYWxzZSksa28ub2JzZXJ2YWJsZShmYWxzZSksa28ub2JzZXJ2YWJsZShmYWxzZSksa28ub2JzZXJ2YWJsZShmYWxzZSksa28ub2JzZXJ2YWJsZShmYWxzZSldO1xuICAgIHNlbGYuZGlzYWJsZU5leHRCdXR0b24gPSBrby5vYnNlcnZhYmxlKHRydWUpO1xuICAgIHNlbGYuYnV0dG9uMFN0YXR1cyA9IGtvLnB1cmVDb21wdXRlZChmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKHNlbGYuaXNDaG9yZFBsYXllZFswXSgpID09IGZhbHNlKXtcbiAgICAgICAgICAgIHJldHVybiAnYnRuLWRlZmF1bHQnXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoc2VsZi5pc1N5bmMoKSkgcmV0dXJuICdidG4tc3VjY2VzcydcbiAgICAgICAgZWxzZSByZXR1cm4gJ2J0bi13YXJuaW5nJztcblxuICAgIH0pO1xuICAgIHNlbGYuYnV0dG9uMVN0YXR1cyA9IGtvLnB1cmVDb21wdXRlZChmdW5jdGlvbigpIHtcbiAgICAgICAgaWYgKHNlbGYuaXNDaG9yZFBsYXllZFsxXSgpID09IGZhbHNlKXtcbiAgICAgICAgICAgIHJldHVybiAnYnRuLWRlZmF1bHQnXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoc2VsZi5pc1N5bmMoKSkgcmV0dXJuICdidG4tc3VjY2Vzcyc7XG4gICAgICAgIGVsc2UgcmV0dXJuICdidG4td2FybmluZyc7XG4gICAgfSk7XG4gICAgc2VsZi5idXR0b24yU3RhdHVzID0ga28ucHVyZUNvbXB1dGVkKGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAoc2VsZi5pc0Nob3JkUGxheWVkWzJdKCkgPT0gZmFsc2Upe1xuICAgICAgICAgICAgcmV0dXJuICdidG4tZGVmYXVsdCc7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoc2VsZi5pc1N5bmMoKSkgcmV0dXJuICdidG4tc3VjY2Vzcyc7XG4gICAgICAgIGVsc2UgcmV0dXJuICdidG4td2FybmluZyc7XG4gICAgfSk7XG4gICAgc2VsZi5idXR0b24zU3RhdHVzID0ga28ucHVyZUNvbXB1dGVkKGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAoc2VsZi5pc0Nob3JkUGxheWVkWzNdKCkgPT0gZmFsc2Upe1xuICAgICAgICAgICAgcmV0dXJuICdidG4tZGVmYXVsdCc7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoc2VsZi5pc1N5bmMoKSkgcmV0dXJuICdidG4tc3VjY2Vzcyc7XG4gICAgICAgIGVsc2UgcmV0dXJuICdidG4td2FybmluZyc7XG4gICAgfSk7XG4gICAgc2VsZi5idXR0b240U3RhdHVzID0ga28ucHVyZUNvbXB1dGVkKGZ1bmN0aW9uKCkge1xuICAgICAgICBpZiAoc2VsZi5pc0Nob3JkUGxheWVkWzRdKCkgPT0gZmFsc2Upe1xuICAgICAgICAgICAgcmV0dXJuICdidG4tZGVmYXVsdCc7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAoc2VsZi5pc1N5bmMoKSkgcmV0dXJuICdidG4tc3VjY2Vzcyc7XG4gICAgICAgIGVsc2UgcmV0dXJuICdidG4td2FybmluZyc7XG4gICAgfSk7XG5cbiAgICBzZWxmLnRyaWdnZXIgPSBmdW5jdGlvbiAoaWQpIHtcbiAgICAgICAgc2VsZi5jb250ZXh0LmV2ZW50c1tpZF0oc2VsZi5jb250ZXh0LCBzZWxmLm91dHB1dCk7XG4gICAgfTtcblxuICAgIHNlbGYudXBkYXRlSXNMYXN0VGVzdCA9IGZ1bmN0aW9uKCl7XG4gICAgICAgIHNlbGYucGVyY2VudGFnZVZhbHVlKE1hdGgucm91bmQoc2VsZi5maWVsZHMoKVsndGVzdCBudW1iZXInXSgpICoxMDAvOCkpO1xuICAgICAgICBpZiAoc2VsZi5maWVsZHMoKVsndGVzdCBudW1iZXInXSgpID09IDcpe1xuICAgICAgICAgICAgc2VsZi5pc0xhc3RUZXN0KHRydWUpO1xuICAgICAgICAgICAgc2VsZi50ZXN0TnVtYmVyKHNlbGYuZmllbGRzKClbJ3Rlc3QgbnVtYmVyJ10oKSArMSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZXtcbiAgICAgICAgICAgIHNlbGYuaXNMYXN0VGVzdChmYWxzZSk7XG4gICAgICAgICAgICBzZWxmLnRlc3ROdW1iZXIoc2VsZi5maWVsZHMoKVsndGVzdCBudW1iZXInXSgpICsxKTtcbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIHNlbGYubG9hZFNhbXBsZXMgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgU291bmRmb250Lmluc3RydW1lbnQoc2VsZi5hYywgJ2Fjb3VzdGljX2dyYW5kX3BpYW5vJykudGhlbihmdW5jdGlvbiAocGlhbm8pIHtcbiAgICAgICAgICAgIHNlbGYucGlhbm8gPSBwaWFubztcbiAgICAgICAgICAgIHNlbGYubG9hZGluZyhmYWxzZSk7XG4gICAgICAgICAgICBzZWxmLnJlYWR5KHRydWUpO1xuICAgICAgICAgICAgLy8gc2VsZi5wbGF5Q2hvcmRzKCk7XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgc2VsZi5sb2FkU2FtcGxlcygpOyAgLy9sb2FkIHRoZSBzYW1wbGVzIHdoZW4gdGhlIHBhZ2UgaXMgbG9hZGluZ1xuXG4gICAgLy9zaW1wbHkgcGxheSB0aGUgaW5wdXQgYXJyYXlcbiAgICBzZWxmLnBsYXlOb3RlcyA9IGZ1bmN0aW9uKG5vdGVBcnJheSl7XG4gICAgICAgIGZvciAodmFyIG4gaW4gbm90ZUFycmF5KXtcbiAgICAgICAgICAgIHNlbGYucGlhbm8ucGxheShub3RlQXJyYXlbbl0sIHNlbGYuYWMuY3VycmVudFRpbWUsIHsgZHVyYXRpb246IDF9KTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICAvL2ZpcnN0IGNhbGwgdG8gdGhlIHJlY3Vyc2l2ZSBmdW5jdGlvbiB0byBwbGF5IGZpZWxkcygpWydjaG9yZHMnXSgpXG4gICAgc2VsZi5wbGF5Q2hvcmRzID0gZnVuY3Rpb24oKXtcbiAgICAgICAgc2VsZi5kaXNhYmxlTmV4dEJ1dHRvbihmYWxzZSk7IC8vZW5hYmxlIHRvIGNoYW5nZSB0ZXN0XG4gICAgICAgIC8vcmVzZXQgdGhlIGNvbG9yc1xuICAgICAgICBmb3IgKHZhciBpaSBpbiBzZWxmLmlzQ2hvcmRQbGF5ZWQpe1xuICAgICAgICAgICAgc2VsZi5pc0Nob3JkUGxheWVkW2lpXShmYWxzZSk7XG4gICAgICAgIH1cbiAgICAgICAgdmFyIGluZGV4ID0gMDtcbiAgICAgICAgc2VsZi5pc1N5bmModHJ1ZSk7XG4gICAgICAgIHNlbGYuZmllbGRzKClbJ2NvbXBsZXhpdHknXShzZWxmLnNsaWRlclZhbHVlKCkgfHwgMCk7XG4gICAgICAgIHZhciBwcm9ncmVzc2lvbnMgPSBzZWxmLmZpZWxkcygpWydwcm9ncmVzc2lvbnMnXSgpW3NlbGYuZmllbGRzKClbJ2NvbXBsZXhpdHknXSgpXTtcbiAgICAgICAgdmFyIHJhbmRvbV9pbmRleCA9IE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIChwcm9ncmVzc2lvbnMubGVuZ3RoLTEpKTtcbiAgICAgICAgdmFyIGNob3JkcyA9IHByb2dyZXNzaW9uc1tyYW5kb21faW5kZXhdLmNob3JkcztcbiAgICAgICAgc2VsZi5yZWN1cnNpdmVQbGF5KGluZGV4LGNob3Jkcyk7XG4gICAgfTtcbiAgICAvL3JlY3Vyc2l2ZWx5IHBsYXkgY2hvcmRzIGluIG9yZGVyIHRvIHdhaXQgMSBzZWNvbmQgYmV0d2VlbiBjaG9yZHMgaW4gbm9uLWJsb2NraW5nIHdheVxuICAgIHNlbGYucmVjdXJzaXZlUGxheSA9IGZ1bmN0aW9uKGluZGV4LCBjaG9yZHMpe1xuICAgICAgICAvL3BsYXkgdGhlIGZpcnN0IDQgY2hvcmRzXG4gICAgICAgIGlmIChpbmRleCA8IE9iamVjdC5rZXlzKGNob3JkcykubGVuZ3RoIC0xICYmIHNlbGYuaXNQbGF5aW5nKCkpe1xuICAgICAgICAgICAgc2VsZi5pc0Nob3JkUGxheWVkW2luZGV4XSh0cnVlKTtcbiAgICAgICAgICAgIHNlbGYucGxheU5vdGVzKGNob3Jkc1tpbmRleF0pO1xuICAgICAgICAgICAgdmFyIG5ld0luZGV4ID0gaW5kZXggKzE7XG4gICAgICAgICAgICBpZiAoc2VsZi5pc1BsYXlpbmcoKSl7IC8vYWRkIGEgY29udHJvbCB0byBhdm9pZCBwbGF5LWNsaWNrIGR1cmluZyB0aGUgd2FpdCBzZWNvbmQgdG8gcmVzdGFydCB0aGUgcHJvZ3Jlc3Npb25cbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBzZWxmLnJlY3Vyc2l2ZVBsYXkobmV3SW5kZXgsY2hvcmRzKTtcbiAgICAgICAgICAgICAgICB9LCAxMDAwKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBlbHNleyAvL3BsYXkgdGhlIGxhc3QgY2hvcmRcbiAgICAgICAgICAgIGlmIChzZWxmLmlzUGxheWluZygpKXtcbiAgICAgICAgICAgICAgICBzZWxmLmlzQ2hvcmRQbGF5ZWRbaW5kZXhdKHRydWUpO1xuICAgICAgICAgICAgICAgIHNlbGYucGxheU5vdGVzKGNob3Jkc1tpbmRleF0pO1xuICAgICAgICAgICAgICAgIC8vc2F2ZSB0aGUgY29tcGxleGl0eSB2YWx1ZVxuICAgICAgICAgICAgICAgIHNlbGYuZmllbGRzKClbJ3ZhbHVlcyBoaXN0b3J5J10oKVtzZWxmLmZpZWxkcygpWyd0ZXN0IG51bWJlciddKCldLnB1c2goc2VsZi5maWVsZHMoKVsnY29tcGxleGl0eSddKCkpO1xuICAgICAgICAgICAgICAgIGlmIChzZWxmLmlzUGxheWluZygpKXsgLy9hZGQgYSBjb250cm9sIHRvIGF2b2lkIHBsYXktY2xpY2sgZHVyaW5nIHRoZSB3YWl0IHNlY29uZCB0byByZXN0YXJ0IHRoZSBwcm9ncmVzc2lvblxuICAgICAgICAgICAgICAgICAgICAvL3dhaXQgMSBzIGZvciB0aGUgY2hvcmQgYW5kIHBsYXkgdGhlIG5ldyBwcm9ncmVzc2lvbiBhZnRlciBhbm90aGVyIDEgc2Vjb25kXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvL3Jlc2V0IHRoZSBjb2xvcnNcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGlpIGluIHNlbGYuaXNDaG9yZFBsYXllZCl7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VsZi5pc0Nob3JkUGxheWVkW2lpXShmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZi5pc1BsYXlpbmcoKSl7IC8vYWRkIGEgY29udHJvbCB0byBhdm9pZCBwbGF5LWNsaWNrIGR1cmluZyB0aGUgd2FpdCBzZWNvbmQgdG8gcmVzdGFydCB0aGUgcHJvZ3Jlc3Npb25cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWxmLnBsYXlDaG9yZHMoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAxMDAwKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSwgMTAwMCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfTtcblxuICAgIC8vc3RvcCB0aGUgaW5maW5pdGUgc291bmQgYW5kIGdvIHRvIGNvbW1lbnRzXG4gICAgc2VsZi5zZW5kUHJlZmVyZW5jZSA9IGZ1bmN0aW9uKCl7XG4gICAgICAgIHNlbGYuZGlzYWJsZU5leHRCdXR0b24odHJ1ZSk7IC8vZGlzYWJsZSAgdG8gYXZvaWQgZG91YmxlIGNsaWNrc1xuICAgICAgICAvL3N0b3JlIHRoZSBjdXJyZW50IGNvbXBsZXhpdHlcbiAgICAgICAgc2VsZi5maWVsZHMoKVsndmFsdWVzIGhpc3RvcnknXSgpW3NlbGYuZmllbGRzKClbJ3Rlc3QgbnVtYmVyJ10oKV0ucHVzaChzZWxmLmZpZWxkcygpWydjb21wbGV4aXR5J10oKSk7XG4gICAgICAgIHNlbGYuaXNQbGF5aW5nKGZhbHNlKTtcbiAgICAgICAgc2VsZi50cmlnZ2VyKCdzZW5kLXByZWZlcmVuY2UtZXZlbnQnKTtcbiAgICB9O1xuXG4gICAgLy9zdG9wIHRoZSBpbmZpbml0ZSBzb3VuZCBhbmQgZ28gdG8gbmV4dCB0ZXN0XG4gICAgc2VsZi5uZXh0VGVzdCA9IGZ1bmN0aW9uKCl7XG4gICAgICAgIHNlbGYuZGlzYWJsZU5leHRCdXR0b24odHJ1ZSk7IC8vZGlzYWJsZSAgdG8gYXZvaWQgZG91YmxlIGNsaWNrc1xuICAgICAgICAvL3Jlc2V0IHRoZSBjb2xvcnNcbiAgICAgICAgZm9yICh2YXIgaWkgaW4gc2VsZi5pc0Nob3JkUGxheWVkKXtcbiAgICAgICAgICAgIHNlbGYuaXNDaG9yZFBsYXllZFtpaV0oZmFsc2UpO1xuICAgICAgICB9XG4gICAgICAgIC8vc3RvcmUgdGhlIGN1cnJlbnQgY29tcGxleGl0eVxuICAgICAgICBzZWxmLmZpZWxkcygpWyd2YWx1ZXMgaGlzdG9yeSddKClbc2VsZi5maWVsZHMoKVsndGVzdCBudW1iZXInXSgpXS5wdXNoKHNlbGYuZmllbGRzKClbJ2NvbXBsZXhpdHknXSgpKTtcbiAgICAgICAgc2VsZi5pc1BsYXlpbmcoZmFsc2UpO1xuICAgICAgICBzZWxmLmZpZWxkcygpWydjb21wbGV4aXR5J10oMCk7XG4gICAgICAgIHNlbGYuc2xpZGVyVmFsdWUoMCk7XG4gICAgICAgIHNlbGYudHJpZ2dlcignbmV4dC10ZXN0LWV2ZW50Jyk7XG4gICAgfTtcblxuICAgIHNlbGYucGxheSA9ZnVuY3Rpb24oKXtcbiAgICAgICAgaWYoIXNlbGYuaXNQbGF5aW5nKCkpe1xuICAgICAgICAgICAgc2VsZi5pc1BsYXlpbmcodHJ1ZSk7XG4gICAgICAgICAgICBzZWxmLnBsYXlDaG9yZHMoKTtcbiAgICAgICAgfVxuXG4gICAgfTtcblxuICAgIHNlbGYucGF1c2UgPSBmdW5jdGlvbigpe1xuICAgICAgICBzZWxmLnBsYXlCdXR0b25FbmFibGVkKGZhbHNlKTtcbiAgICAgICAgc2VsZi5pc1BsYXlpbmcoZmFsc2UpO1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgc2VsZi5wbGF5QnV0dG9uRW5hYmxlZCh0cnVlKTtcbiAgICAgICAgfSwgMTAxMCk7XG4gICAgfTtcblxuXG4gICAgc2VsZi5zbGlkZXJWYWx1ZS5zdWJzY3JpYmUoZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgIHNlbGYucGxheUJ1dHRvbkVuYWJsZWQoZmFsc2UpOyAvL3RvIGF2b2lkIGZhc3QgZG91YmxlIGNsaWNrIGJlZm9yZSB0aGUgcHJvZ3Jlc2lvbiBzdG9wXG4gICAgICAgIHNlbGYuaXNQbGF5aW5nKGZhbHNlKTtcbiAgICAgICAgc2VsZi5pc1N5bmMoZmFsc2UpO1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgc2VsZi5wbGF5QnV0dG9uRW5hYmxlZCh0cnVlKTtcbiAgICAgICAgfSwgMTAxMCk7XG4gICAgfSk7XG5cbn1cblxuVmlld01vZGVsLnByb3RvdHlwZS5pZCA9ICd0ZXN0LWZvcm0nO1xuXG5WaWV3TW9kZWwucHJvdG90eXBlLndhaXRGb3JTdGF0dXNDaGFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMuX2luaXRpYWxpemluZyB8fFxuICAgICAgICAgICBQcm9taXNlLnJlc29sdmUoKTtcbn07XG5cblZpZXdNb2RlbC5wcm90b3R5cGUuX2NvbXB1dGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5vdXRwdXQgPSB7XG4gICAgICAgICdwcm9ncmVzc2lvbnMnOiB0aGlzLmlucHV0Wydwcm9ncmVzc2lvbnMnXSxcbiAgICAgICAgJ2NvbXBsZXhpdHknOiB0aGlzLmlucHV0Wydjb21wbGV4aXR5J10sXG4gICAgICAgICdzdXJ2ZXkgYW5zd2Vycyc6IHRoaXMuaW5wdXRbJ3N1cnZleSBhbnN3ZXJzJ10sXG4gICAgICAgICd1c2VyIGlkJzogdGhpcy5pbnB1dFsndXNlciBpZCddLFxuICAgICAgICAndmFsdWVzIGhpc3RvcnknOiB0aGlzLmlucHV0Wyd2YWx1ZXMgaGlzdG9yeSddLFxuICAgICAgICAndGVzdCBudW1iZXInIDogdGhpcy5pbnB1dFsndGVzdCBudW1iZXInXSxcbiAgICB9O1xuICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgICAgZmllbGRzID0ge1xuICAgICAgICAgICAgJ3Byb2dyZXNzaW9ucyc6IGtvLm9ic2VydmFibGUodGhpcy5pbnB1dFsncHJvZ3Jlc3Npb25zJ10pLFxuICAgICAgICAgICAgJ2NvbXBsZXhpdHknOiBrby5vYnNlcnZhYmxlKHRoaXMuaW5wdXRbJ2NvbXBsZXhpdHknXSksXG4gICAgICAgICAgICAnc3VydmV5IGFuc3dlcnMnOiBrby5vYnNlcnZhYmxlKHRoaXMuaW5wdXRbJ3N1cnZleSBhbnN3ZXJzJ10pLFxuICAgICAgICAgICAgJ3VzZXIgaWQnOiBrby5vYnNlcnZhYmxlKHRoaXMuaW5wdXRbJ3VzZXIgaWQnXSksXG4gICAgICAgICAgICAndmFsdWVzIGhpc3RvcnknOiBrby5vYnNlcnZhYmxlKHRoaXMuaW5wdXRbJ3ZhbHVlcyBoaXN0b3J5J10pLFxuICAgICAgICAgICAgJ3Rlc3QgbnVtYmVyJyA6IGtvLm9ic2VydmFibGUodGhpcy5pbnB1dFsndGVzdCBudW1iZXInXSksXG4gICAgICAgIH0sXG4gICAgICAgIGVycm9ycyA9IHtcbiAgICAgICAgICAgICdwcm9ncmVzc2lvbnMnOiBrby5vYnNlcnZhYmxlKHRoaXMuaW5wdXRbJ3Byb2dyZXNzaW9ucy1lcnJvciddKSxcbiAgICAgICAgICAgICdjb21wbGV4aXR5Jzoga28ub2JzZXJ2YWJsZSh0aGlzLmlucHV0Wydjb21wbGV4aXR5LWVycm9yJ10pLFxuICAgICAgICAgICAgJ3N1cnZleSBhbnN3ZXJzJzoga28ub2JzZXJ2YWJsZSh0aGlzLmlucHV0WydzdXJ2ZXkgYW5zd2Vycy1lcnJvciddKSxcbiAgICAgICAgICAgICd1c2VyIGlkJzoga28ub2JzZXJ2YWJsZSh0aGlzLmlucHV0Wyd1c2VyIGlkLWVycm9yJ10pLFxuICAgICAgICAgICAgJ3ZhbHVlcyBoaXN0b3J5Jzoga28ub2JzZXJ2YWJsZSh0aGlzLmlucHV0Wyd2YWx1ZXMgaGlzdG9yeS1lcnJvciddKSxcbiAgICAgICAgICAgICd0ZXN0IG51bWJlcicgOiBrby5vYnNlcnZhYmxlKHRoaXMuaW5wdXRbJ3Rlc3QgbnVtYmVyLWVycm9yJ10pXG4gICAgICAgIH07XG4gICAgZmllbGRzWydwcm9ncmVzc2lvbnMnXS5zdWJzY3JpYmUoZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgIHNlbGYub3V0cHV0WydjaG9yZHMnXSA9IHZhbHVlO1xuICAgICAgICBzZWxmLmVycm9ycygpWydjaG9yZHMnXSh1bmRlZmluZWQpO1xuICAgIH0pO1xuICAgIGZpZWxkc1snY29tcGxleGl0eSddLnN1YnNjcmliZShmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgc2VsZi5vdXRwdXRbJ2NvbXBsZXhpdHknXSA9IHZhbHVlO1xuICAgICAgICBzZWxmLmVycm9ycygpWydjb21wbGV4aXR5J10odW5kZWZpbmVkKTtcbiAgICB9KTtcbiAgICBmaWVsZHNbJ3N1cnZleSBhbnN3ZXJzJ10uc3Vic2NyaWJlKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICBzZWxmLm91dHB1dFsnc3VydmV5IGFuc3dlcnMnXSA9IHZhbHVlO1xuICAgICAgICBzZWxmLmVycm9ycygpWydzdXJ2ZXkgYW5zd2VycyddKHVuZGVmaW5lZCk7XG4gICAgfSk7XG4gICAgZmllbGRzWyd1c2VyIGlkJ10uc3Vic2NyaWJlKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICBzZWxmLm91dHB1dFsndXNlciBpZCddID0gdmFsdWU7XG4gICAgICAgIHNlbGYuZXJyb3JzKClbJ3VzZXIgaWQnXSh1bmRlZmluZWQpO1xuICAgIH0pO1xuICAgIGZpZWxkc1sndmFsdWVzIGhpc3RvcnknXS5zdWJzY3JpYmUoZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgIHNlbGYub3V0cHV0Wyd2YWx1ZXMgaGlzdG9yeSddID0gdmFsdWU7XG4gICAgICAgIHNlbGYuZXJyb3JzKClbJ3ZhbHVlcyBoaXN0b3J5J10odW5kZWZpbmVkKTtcbiAgICB9KTtcbiAgICBmaWVsZHNbJ3Rlc3QgbnVtYmVyJ10uc3Vic2NyaWJlKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICBzZWxmLm91dHB1dFsndGVzdCBudW1iZXInXSA9IHZhbHVlO1xuICAgICAgICBzZWxmLmVycm9ycygpWyd0ZXN0IG51bWJlciddKHVuZGVmaW5lZCk7XG4gICAgfSk7XG4gICAgdGhpcy5maWVsZHMoZmllbGRzKTtcbiAgICBzZWxmLnVwZGF0ZUlzTGFzdFRlc3QoKTtcbiAgICB0aGlzLmVycm9ycyhlcnJvcnMpO1xuICAgIHRoaXMuc3RhdHVzKCdjb21wdXRlZCcpO1xufTtcblxuXG5WaWV3TW9kZWwucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAob3B0aW9ucykge1xuICAgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICAgIHRoaXMub3V0cHV0ID0gdW5kZWZpbmVkO1xuICAgIHRoaXMuZmllbGRzKHt9KTtcbiAgICB0aGlzLmVycm9ycyh7fSk7XG4gICAgdGhpcy5pbnB1dCA9IG9wdGlvbnMuaW5wdXQgfHwge307XG4gICAgdGhpcy5zdGF0dXMoJ3JlYWR5Jyk7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHRoaXMuX2luaXRpYWxpemluZyA9IG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgc2VsZi5fY29tcHV0ZSgpO1xuICAgICAgICAgICAgcmVzb2x2ZSgpO1xuICAgICAgICAgICAgc2VsZi5faW5pdGlhbGl6aW5nID0gdW5kZWZpbmVkO1xuICAgICAgICB9LCAxKTtcbiAgICB9KTtcbn07XG5cbmV4cG9ydHMucmVnaXN0ZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAga28uY29tcG9uZW50cy5yZWdpc3RlcignYy10ZXN0LWZvcm0nLCB7XG4gICAgICAgIHZpZXdNb2RlbDoge1xuICAgICAgICAgICAgY3JlYXRlVmlld01vZGVsOiBmdW5jdGlvbiAocGFyYW1zLCBjb21wb25lbnRJbmZvKSB7XG4gICAgICAgICAgICAgICAgdmFyIHZtID0gbmV3IFZpZXdNb2RlbChwYXJhbXMpO1xuICAgICAgICAgICAgICAgIHBhcmFtcy5jb250ZXh0LnZtc1t2bS5pZF0gPSB2bTtcbiAgICAgICAgICAgICAgICBrby51dGlscy5kb21Ob2RlRGlzcG9zYWwuYWRkRGlzcG9zZUNhbGxiYWNrKGNvbXBvbmVudEluZm8uZWxlbWVudCwgZnVuY3Rpb24gKCkgeyBkZWxldGUgcGFyYW1zLmNvbnRleHQudm1zW3ZtLmlkXTsgfSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHZtO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB0ZW1wbGF0ZTogcmVxdWlyZSgnLi9pbmRleC5odG1sJyksXG4gICAgICAgIHN5bmNocm9ub3VzOiB0cnVlXG4gICAgfSk7XG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxzcGFuPlxcbiAgICA8IS0tIFRlc3QgRm9ybSAtLT5cXG4gICAgPGMtdGVzdC1mb3JtIHBhcmFtcz1cXFwiY29udGV4dDogY29udGV4dFxcXCI+PC9jLXRlc3QtZm9ybT5cXG48L3NwYW4+XCI7XG4iLCIvKmpzbGludCBub2RlOiB0cnVlLCBub21lbjogdHJ1ZSAqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBrbyA9ICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93WydrbyddIDogdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbFsna28nXSA6IG51bGwpO1xuXG5mdW5jdGlvbiBWaWV3TW9kZWwocGFyYW1zKSB7XG4gICAgdmFyIHNlbGYgPSB0aGlzO1xuICAgIHNlbGYuY29udGV4dCA9IHBhcmFtcy5jb250ZXh0O1xuXG4gICAgc2VsZi5pbml0ID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICAgICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge307XG4gICAgICAgIHNlbGYuY2hpbGRyZW4uZm9yRWFjaChmdW5jdGlvbiAoY2hpbGQpe1xuICAgICAgICAgICAgaWYgKGNoaWxkID09PSBvcHRpb25zLm1hc2spIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzZWxmLmNvbnRleHQudm1zW2NoaWxkXS5pbml0KG9wdGlvbnMpO1xuICAgICAgICB9KTtcbiAgICB9O1xuXG4gICAgc2VsZi50cmlnZ2VyID0gZnVuY3Rpb24gKGlkKSB7XG4gICAgICAgIHNlbGYuY29udGV4dC5ldmVudHNbaWRdKHNlbGYuY29udGV4dCk7XG4gICAgfTtcbn1cblxuVmlld01vZGVsLnByb3RvdHlwZS5pZCA9ICd0ZXN0LXZpZXcnO1xuVmlld01vZGVsLnByb3RvdHlwZS5jaGlsZHJlbiA9IFtcbiAgICAndGVzdC1mb3JtJyAvLyBUZXN0IEZvcm1cbl07XG5cbmV4cG9ydHMucmVnaXN0ZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAga28uY29tcG9uZW50cy5yZWdpc3RlcignYy10ZXN0LXZpZXcnLCB7XG4gICAgICAgIHZpZXdNb2RlbDoge1xuICAgICAgICAgICAgY3JlYXRlVmlld01vZGVsOiBmdW5jdGlvbiAocGFyYW1zLCBjb21wb25lbnRJbmZvKSB7XG4gICAgICAgICAgICAgICAgdmFyIHZtID0gbmV3IFZpZXdNb2RlbChwYXJhbXMpO1xuICAgICAgICAgICAgICAgIHBhcmFtcy5jb250ZXh0LnZtc1t2bS5pZF0gPSB2bTtcbiAgICAgICAgICAgICAgICBwYXJhbXMuY29udGV4dC5ydW5uaW5nQWN0aW9uc0J5Q29udGFpbmVyW3ZtLmlkXSA9IFtdO1xuICAgICAgICAgICAgICAgIGtvLnV0aWxzLmRvbU5vZGVEaXNwb3NhbC5hZGREaXNwb3NlQ2FsbGJhY2soY29tcG9uZW50SW5mby5lbGVtZW50LCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHBhcmFtcy5jb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbdm0uaWRdLmZvckVhY2goZnVuY3Rpb24gKHByb21pc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb21pc2UuY2FuY2VsKCk7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIGRlbGV0ZSBwYXJhbXMuY29udGV4dC5ydW5uaW5nQWN0aW9uc0J5Q29udGFpbmVyW3ZtLmlkXTtcbiAgICAgICAgICAgICAgICAgICAgZGVsZXRlIHBhcmFtcy5jb250ZXh0LnZtc1t2bS5pZF07XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHZtO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICB0ZW1wbGF0ZTogcmVxdWlyZSgnLi9pbmRleC5odG1sJyksXG4gICAgICAgIHN5bmNocm9ub3VzOiB0cnVlXG4gICAgfSk7XG59O1xuIiwiLypqc2xpbnQgbm9kZTogdHJ1ZSwgbm9tZW46IHRydWUgKi9cblwidXNlIHN0cmljdFwiO1xuXG52YXIga28gPSAodHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvd1sna28nXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ2tvJ10gOiBudWxsKTtcblxuZXhwb3J0cy5yZWdpc3RlciA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXF1aXJlKCcuL21haW4tYXBwbGljYXRpb24nKS5yZWdpc3RlcigpO1xuICAgIHJlcXVpcmUoJy4vYy1maW5hbC1wYWdlLXZpZXcnKS5yZWdpc3RlcigpO1xuICAgIHJlcXVpcmUoJy4vYy1ob21lLXBhZ2UnKS5yZWdpc3RlcigpO1xuICAgIHJlcXVpcmUoJy4vYy1zdXJ2ZXktdmlldycpLnJlZ2lzdGVyKCk7XG4gICAgcmVxdWlyZSgnLi9jLXRlc3QtdmlldycpLnJlZ2lzdGVyKCk7XG4gICAgcmVxdWlyZSgnLi9jLXN1cnZleS1mb3JtJykucmVnaXN0ZXIoKTtcbiAgICByZXF1aXJlKCcuL2MtdGVzdC1mb3JtJykucmVnaXN0ZXIoKTtcbiAgICByZXF1aXJlKCcuL2MtZW1haWwtZm9ybScpLnJlZ2lzdGVyKCk7XG4gICAgcmVxdWlyZSgnLi9jLWNsb3NpbmctcGFnZScpLnJlZ2lzdGVyKCk7XG4gICAgcmVxdWlyZSgnLi9jLWVycm9yLXZpZXcnKS5yZWdpc3RlcigpO1xufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gXCI8bmF2IGNsYXNzPVxcXCJuYXZiYXIgbmF2YmFyLWRlZmF1bHRcXFwiPlxcbiAgICA8ZGl2IGNsYXNzPVxcXCJjb250YWluZXJcXFwiPlxcbiAgICAgICAgPGRpdiBjbGFzcz1cXFwibmF2YmFyLWhlYWRlclxcXCI+XFxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cXFwibmF2YmFyLXRvZ2dsZSBjb2xsYXBzZWRcXFwiIHR5cGU9XFxcImJ1dHRvblxcXCIgZGF0YS10b2dnbGU9XFxcImNvbGxhcHNlXFxcIiBkYXRhLXRhcmdldD1cXFwiI2xhbmRtYXJrLW1lbnVcXFwiIGFyaWEtZXhwYW5kZWQ9XFxcImZhbHNlXFxcIj5cXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XFxcInNyLW9ubHlcXFwiPlRvZ2dsZSBuYXZpZ2F0aW9uPC9zcGFuPlxcbiAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwiaWNvbi1iYXJcXFwiPjwvc3Bhbj5cXG4gICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XFxcImljb24tYmFyXFxcIj48L3NwYW4+XFxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVxcXCJpY29uLWJhclxcXCI+PC9zcGFuPlxcbiAgICAgICAgICAgIDwvYnV0dG9uPlxcbiAgICAgICAgPC9kaXY+XFxuICAgICAgICA8ZGl2IGlkPVxcXCJsYW5kbWFyay1tZW51XFxcIiBjbGFzcz1cXFwiY29sbGFwc2UgbmF2YmFyLWNvbGxhcHNlXFxcIj5cXG4gICAgICAgICAgICA8dWwgY2xhc3M9XFxcIm5hdiBuYXZiYXItbmF2XFxcIj5cXG4gICAgICAgICAgICA8L3VsPlxcbiAgICAgICAgPC9kaXY+XFxuICAgIDwvZGl2PlxcbjwvbmF2PlxcbjxkaXYgY2xhc3M9XFxcImNvbnRhaW5lclxcXCI+XFxuICAgIDxkaXYgY2xhc3M9XFxcInJvd1xcXCI+XFxuICAgICAgICA8c3BhbiBkYXRhLWJpbmQ9XFxcImlmOiBhY3RpdmUoKSA9PT0gJ2ZpbmFsLXBhZ2UtdmlldydcXFwiPlxcbiAgICAgICAgICAgIDxjLWZpbmFsLXBhZ2UtdmlldyBwYXJhbXM9XFxcImNvbnRleHQ6IGNvbnRleHRcXFwiIGNsYXNzPVxcXCJjb250YWluZXJcXFwiPjwvYy1maW5hbC1wYWdlLXZpZXc+XFxuICAgICAgICA8L3NwYW4+XFxuICAgICAgICA8c3BhbiBkYXRhLWJpbmQ9XFxcImlmOiBhY3RpdmUoKSA9PT0gJ2hvbWUtcGFnZSdcXFwiPlxcbiAgICAgICAgICAgIDxjLWhvbWUtcGFnZSBwYXJhbXM9XFxcImNvbnRleHQ6IGNvbnRleHRcXFwiIGNsYXNzPVxcXCJjb250YWluZXJcXFwiPjwvYy1ob21lLXBhZ2U+XFxuICAgICAgICA8L3NwYW4+XFxuICAgICAgICA8c3BhbiBkYXRhLWJpbmQ9XFxcImlmOiBhY3RpdmUoKSA9PT0gJ3N1cnZleS12aWV3J1xcXCI+XFxuICAgICAgICAgICAgPGMtc3VydmV5LXZpZXcgcGFyYW1zPVxcXCJjb250ZXh0OiBjb250ZXh0XFxcIiBjbGFzcz1cXFwiY29udGFpbmVyXFxcIj48L2Mtc3VydmV5LXZpZXc+XFxuICAgICAgICA8L3NwYW4+XFxuICAgICAgICA8c3BhbiBkYXRhLWJpbmQ9XFxcImlmOiBhY3RpdmUoKSA9PT0gJ3Rlc3QtdmlldydcXFwiPlxcbiAgICAgICAgICAgIDxjLXRlc3QtdmlldyBwYXJhbXM9XFxcImNvbnRleHQ6IGNvbnRleHRcXFwiIGNsYXNzPVxcXCJjb250YWluZXJcXFwiPjwvYy10ZXN0LXZpZXc+XFxuICAgICAgICA8L3NwYW4+XFxuICAgICAgICA8c3BhbiBkYXRhLWJpbmQ9XFxcImlmOiBhY3RpdmUoKSA9PT0gJ2Nsb3NpbmctcGFnZSdcXFwiPlxcbiAgICAgICAgICAgIDxjLWNsb3NpbmctcGFnZSBwYXJhbXM9XFxcImNvbnRleHQ6IGNvbnRleHRcXFwiIGNsYXNzPVxcXCJjb250YWluZXJcXFwiPjwvYy1jbG9zaW5nLXBhZ2U+XFxuICAgICAgICA8L3NwYW4+XFxuICAgICAgICA8c3BhbiBkYXRhLWJpbmQ9XFxcImlmOiBhY3RpdmUoKSA9PT0gJ2Vycm9yLXZpZXcnXFxcIj5cXG4gICAgICAgICAgICA8Yy1lcnJvci12aWV3IHBhcmFtcz1cXFwiY29udGV4dDogY29udGV4dFxcXCIgY2xhc3M9XFxcImNvbnRhaW5lclxcXCI+PC9jLWVycm9yLXZpZXc+XFxuICAgICAgICA8L3NwYW4+XFxuICAgIDwvZGl2PlxcbjwvZGl2PlwiO1xuIiwiLypqc2xpbnQgbm9kZTogdHJ1ZSwgbm9tZW46IHRydWUgKi9cblwidXNlIHN0cmljdFwiO1xuXG52YXIga28gPSAodHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvd1sna28nXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJ2tvJ10gOiBudWxsKSxcbiAgICAkID0gKHR5cGVvZiB3aW5kb3cgIT09IFwidW5kZWZpbmVkXCIgPyB3aW5kb3dbJyQnXSA6IHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIgPyBnbG9iYWxbJyQnXSA6IG51bGwpO1xuXG5leHBvcnRzLnJlZ2lzdGVyID0gZnVuY3Rpb24gKCkge1xuICAgIGtvLmNvbXBvbmVudHMucmVnaXN0ZXIoJ21haW4tYXBwbGljYXRpb24nLCB7XG4gICAgICAgIHZpZXdNb2RlbDogZnVuY3Rpb24ocGFyYW1zKSB7XG4gICAgICAgICAgICB2YXIgc2VsZiA9IHRoaXMsXG4gICAgICAgICAgICAgICAgZGVmYXVsdENoaWxkID0gJ2hvbWUtcGFnZSc7XG4gICAgICAgICAgICBzZWxmLmNvbnRleHQgPSBwYXJhbXMuY29udGV4dDtcbiAgICAgICAgICAgIHNlbGYuYWN0aXZlID0ga28ub2JzZXJ2YWJsZSh1bmRlZmluZWQpO1xuXG4gICAgICAgICAgICBzZWxmLmxhbmRtYXJrID0gZnVuY3Rpb24gKGlkKSB7XG4gICAgICAgICAgICAgICAgc2VsZi5hY3RpdmUoaWQpO1xuICAgICAgICAgICAgICAgIHNlbGYuY29udGV4dC52bXNbaWRdLmluaXQoKTtcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBzZWxmLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgc2VsZi5hY3RpdmUoZGVmYXVsdENoaWxkKTtcbiAgICAgICAgICAgICAgICBpZiAoc2VsZi5jb250ZXh0LnZtc1tkZWZhdWx0Q2hpbGRdKSB7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYuY29udGV4dC52bXNbZGVmYXVsdENoaWxkXS5pbml0KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgc2VsZi5jb250ZXh0LnRvcCA9IHNlbGY7XG4gICAgICAgIH0sXG4gICAgICAgIHRlbXBsYXRlOiByZXF1aXJlKCcuL2luZGV4Lmh0bWwnKSxcbiAgICAgICAgc3luY2hyb25vdXM6IHRydWVcbiAgICB9KTtcbn07XG4iLCIvKmpzbGludCBub2RlOiB0cnVlLCBub21lbjogdHJ1ZSAqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuY3JlYXRlRXZlbnQgPSBmdW5jdGlvbiAoKSB7IC8vIGFkZCBcIm9wdGlvbnNcIiBwYXJhbWV0ZXIgaWYgbmVlZGVkXG4gICAgcmV0dXJuIGZ1bmN0aW9uIChjb250ZXh0KSB7XG4gICAgICAgIGlmICghY29udGV4dC52bXNbJ2Vycm9yLXZpZXcnXSkge1xuICAgICAgICAgICAgY29udGV4dC50b3AuYWN0aXZlKCdlcnJvci12aWV3Jyk7XG4gICAgICAgIH1cbiAgICAgICAgY29udGV4dC52bXNbJ2Vycm9yLXZpZXcnXS5pbml0KCk7XG4gICAgfTtcbn07XG4iLCIvKmpzbGludCBub2RlOiB0cnVlLCBub21lbjogdHJ1ZSAqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuY3JlYXRlRXZlbnQgPSBmdW5jdGlvbiAoKSB7IC8vIGFkZCBcIm9wdGlvbnNcIiBwYXJhbWV0ZXIgaWYgbmVlZGVkXG4gICAgcmV0dXJuIGZ1bmN0aW9uIChjb250ZXh0LCBkYXRhKSB7XG4gICAgICAgIGlmICghY29udGV4dC52bXNbJ3Rlc3QtdmlldyddKSB7XG4gICAgICAgICAgICBjb250ZXh0LnRvcC5hY3RpdmUoJ3Rlc3QtdmlldycpO1xuICAgICAgICAgICAgY29udGV4dC52bXNbJ3Rlc3QtdmlldyddLmluaXQoe21hc2s6ICd0ZXN0LWZvcm0nfSk7XG4gICAgICAgIH1cbiAgICAgICAgZGF0YSA9IGRhdGEgfHwge307XG4gICAgICAgIHZhciBwYWNrZXQgPSB7XG4gICAgICAgICAgICAndXNlciBpZCcgOiBkYXRhWyd1c2VyIGlkJ10sXG4gICAgICAgICAgICAncHJvZ3Jlc3Npb25zJyA6IGRhdGFbJ3Byb2dyZXNzaW9ucyddLFxuICAgICAgICAgICAgJ3N1cnZleSBhbnN3ZXJzJyA6IGRhdGFbJ3N1cnZleSBhbnN3ZXJzJ10sXG4gICAgICAgICAgICAndmFsdWVzIGhpc3RvcnknIDogZGF0YVsndmFsdWVzIGhpc3RvcnknXSxcbiAgICAgICAgICAgICd0ZXN0IG51bWJlcicgOiBkYXRhWyd0ZXN0IG51bWJlciddXG4gICAgICAgIH07XG4gICAgICAgIGNvbnRleHQudm1zWyd0ZXN0LWZvcm0nXS5pbml0KHtpbnB1dDogcGFja2V0fSk7XG4gICAgfTtcbn07XG4iLCIvKmpzbGludCBub2RlOiB0cnVlLCBub21lbjogdHJ1ZSAqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuY3JlYXRlRXZlbnQgPSBmdW5jdGlvbiAoKSB7IC8vIGFkZCBcIm9wdGlvbnNcIiBwYXJhbWV0ZXIgaWYgbmVlZGVkXG4gICAgcmV0dXJuIGZ1bmN0aW9uIChjb250ZXh0KSB7XG4gICAgICAgIGlmICghY29udGV4dC52bXNbJ2Nsb3NpbmctcGFnZSddKSB7XG4gICAgICAgICAgICBjb250ZXh0LnRvcC5hY3RpdmUoJ2Nsb3NpbmctcGFnZScpO1xuICAgICAgICB9XG4gICAgICAgIGNvbnRleHQudm1zWydjbG9zaW5nLXBhZ2UnXS5pbml0KCk7XG4gICAgfTtcbn07XG4iLCIvKmpzbGludCBub2RlOiB0cnVlLCBub21lbjogdHJ1ZSAqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuY3JlYXRlRXZlbnQgPSBmdW5jdGlvbiAoKSB7IC8vIGFkZCBcIm9wdGlvbnNcIiBwYXJhbWV0ZXIgaWYgbmVlZGVkXG4gICAgcmV0dXJuIGZ1bmN0aW9uIChjb250ZXh0LCBkYXRhKSB7XG4gICAgICAgIGlmICghY29udGV4dC52bXNbJ2ZpbmFsLXBhZ2UtdmlldyddKSB7XG4gICAgICAgICAgICBjb250ZXh0LnRvcC5hY3RpdmUoJ2ZpbmFsLXBhZ2UtdmlldycpO1xuICAgICAgICAgICAgY29udGV4dC52bXNbJ2ZpbmFsLXBhZ2UtdmlldyddLmluaXQoe21hc2s6ICdlbWFpbC1mb3JtJ30pO1xuICAgICAgICB9XG4gICAgICAgIGRhdGEgPSBkYXRhIHx8IHt9O1xuICAgICAgICB2YXIgcGFja2V0ID0ge1xuICAgICAgICAgICAgJ3VzZXIgaWQnIDogZGF0YVsndXNlciBpZCddXG4gICAgICAgIH07XG4gICAgICAgIGNvbnRleHQudm1zWydlbWFpbC1mb3JtJ10uaW5pdCh7aW5wdXQ6IHBhY2tldH0pO1xuICAgIH07XG59O1xuIiwiLypqc2xpbnQgbm9kZTogdHJ1ZSwgbm9tZW46IHRydWUgKi9cblwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLmNyZWF0ZUV2ZW50ID0gZnVuY3Rpb24gKCkgeyAvLyBhZGQgXCJvcHRpb25zXCIgcGFyYW1ldGVyIGlmIG5lZWRlZFxuICAgIHJldHVybiBmdW5jdGlvbiAoY29udGV4dCwgZGF0YSkge1xuICAgICAgICBpZiAoIWNvbnRleHQudm1zWydzdXJ2ZXktdmlldyddKSB7XG4gICAgICAgICAgICBjb250ZXh0LnRvcC5hY3RpdmUoJ3N1cnZleS12aWV3Jyk7XG4gICAgICAgICAgICBjb250ZXh0LnZtc1snc3VydmV5LXZpZXcnXS5pbml0KHttYXNrOiAnc3VydmV5LWZvcm0nfSk7XG4gICAgICAgIH1cbiAgICAgICAgZGF0YSA9IGRhdGEgfHwge307XG4gICAgICAgIHZhciBwYWNrZXQgPSB7XG4gICAgICAgICAgICAndXNlciBpZCcgOiBkYXRhWyd1c2VyIGlkJ11cbiAgICAgICAgfTtcbiAgICAgICAgY29udGV4dC52bXNbJ3N1cnZleS1mb3JtJ10uaW5pdCh7aW5wdXQ6IHBhY2tldH0pO1xuICAgIH07XG59O1xuIiwiLypqc2xpbnQgbm9kZTogdHJ1ZSwgbm9tZW46IHRydWUgKi9cblwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLmNyZWF0ZUV2ZW50cyA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgJ3N0YXJ0LXN1cnZleS1ldmVudCc6IHJlcXVpcmUoJy4vc3RhcnQtc3VydmV5LWV2ZW50JykuY3JlYXRlRXZlbnQob3B0aW9ucylcbiAgICAgICAgLCdzZW5kLXN1cnZleS1ldmVudCc6IHJlcXVpcmUoJy4vc2VuZC1zdXJ2ZXktZXZlbnQnKS5jcmVhdGVFdmVudChvcHRpb25zKVxuICAgICAgICAsJ2JlZ2luLXRlc3QtZXZlbnQnOiByZXF1aXJlKCcuL2JlZ2luLXRlc3QtZXZlbnQnKS5jcmVhdGVFdmVudChvcHRpb25zKVxuICAgICAgICAsJ2ZpbmFsLXBhZ2UtZXZlbnQnOiByZXF1aXJlKCcuL2ZpbmFsLXBhZ2UtZXZlbnQnKS5jcmVhdGVFdmVudChvcHRpb25zKVxuICAgICAgICAsJ2hhdmUtaWQtZXZlbnQnOiByZXF1aXJlKCcuL2hhdmUtaWQtZXZlbnQnKS5jcmVhdGVFdmVudChvcHRpb25zKVxuICAgICAgICAsJ3NlbmQtcHJlZmVyZW5jZS1ldmVudCc6IHJlcXVpcmUoJy4vc2VuZC1wcmVmZXJlbmNlLWV2ZW50JykuY3JlYXRlRXZlbnQob3B0aW9ucylcbiAgICAgICAgLCdzZW5kLW1haWwtZXZlbnQnOiByZXF1aXJlKCcuL3NlbmQtbWFpbC1ldmVudCcpLmNyZWF0ZUV2ZW50KG9wdGlvbnMpXG4gICAgICAgICwnbmV4dC10ZXN0LWV2ZW50JyA6IHJlcXVpcmUoJy4vbmV4dC10ZXN0LWV2ZW50JykuY3JlYXRlRXZlbnQob3B0aW9ucylcbiAgICAgICAgLCdjbG9zZS1wYWdlLWV2ZW50JzogcmVxdWlyZSgnLi9jbG9zZS1wYWdlLWV2ZW50JykuY3JlYXRlRXZlbnQob3B0aW9ucylcbiAgICAgICAgLCdoYXZlLWlkLWVycm9yLWV2ZW50JzogcmVxdWlyZSgnLi9oYXZlLWlkLWVycm9yLWV2ZW50JykuY3JlYXRlRXZlbnQob3B0aW9ucylcbiAgICAgICAgLCdiZWdpbi10ZXN0LWVycm9yLWV2ZW50JzogcmVxdWlyZSgnLi9iZWdpbi10ZXN0LWVycm9yLWV2ZW50JykuY3JlYXRlRXZlbnQob3B0aW9ucylcbiAgICAgICAgLCdmaW5hbC1wYWdlLWVycm9yLWV2ZW50JzogcmVxdWlyZSgnLi9maW5hbC1wYWdlLWVycm9yLWV2ZW50JykuY3JlYXRlRXZlbnQob3B0aW9ucylcbiAgICB9O1xufTtcbiIsIi8qanNsaW50IG5vZGU6IHRydWUsIG5vbWVuOiB0cnVlICovXG5cInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5jcmVhdGVFdmVudCA9IGZ1bmN0aW9uICgpIHsgLy8gYWRkIFwib3B0aW9uc1wiIHBhcmFtZXRlciBpZiBuZWVkZWRcbiAgICByZXR1cm4gZnVuY3Rpb24gKGNvbnRleHQsIGRhdGEpIHtcbiAgICAgICAgZGF0YSA9IGRhdGEgfHwge307XG4gICAgICAgIHZhciBwYWNrZXQgPSB7XG4gICAgICAgICAgICAnc3VydmV5IGFuc3dlcnMnIDogZGF0YVsnc3VydmV5IGFuc3dlcnMnXVxuICAgICAgICAgICAgLCd1c2VyIGlkJyA6IGRhdGFbJ3VzZXIgaWQnXVxuICAgICAgICAgICAgLCd2YWx1ZXMgaGlzdG9yeScgOiBkYXRhWyd2YWx1ZXMgaGlzdG9yeSddXG4gICAgICAgICAgICAsJ3Rlc3QgbnVtYmVyJyA6IGRhdGFbJ3Rlc3QgbnVtYmVyJ10gKzFcbiAgICAgICAgfTtcbiAgICAgICAgdmFyIHByb21pc2UgPSBjb250ZXh0LmFjdGlvbnNbJ2NvbXB1dGUtZmlyc3QtY2hvcmRzJ10oe2ZpbHRlcnM6IHBhY2tldH0pO1xuICAgICAgICBjb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbJ3Rlc3QtdmlldyddLnB1c2gocHJvbWlzZSk7XG4gICAgICAgIHByb21pc2UudGhlbihmdW5jdGlvbiAocmVzdWx0KSB7XG4gICAgICAgICAgICBjb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbJ3Rlc3QtdmlldyddLnNwbGljZShcbiAgICAgICAgICAgICAgICBjb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbJ3Rlc3QtdmlldyddLmluZGV4T2YocHJvbWlzZSksIDFcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBpZiAocmVzdWx0LmV2ZW50KSB7XG4gICAgICAgICAgICAgICAgY29udGV4dC5ldmVudHNbcmVzdWx0LmV2ZW50XShjb250ZXh0LCByZXN1bHQuZGF0YSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH07XG59O1xuIiwiLypqc2xpbnQgbm9kZTogdHJ1ZSwgbm9tZW46IHRydWUgKi9cblwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLmNyZWF0ZUV2ZW50ID0gZnVuY3Rpb24gKCkgeyAvLyBhZGQgXCJvcHRpb25zXCIgcGFyYW1ldGVyIGlmIG5lZWRlZFxuICAgIHJldHVybiBmdW5jdGlvbiAoY29udGV4dCwgZGF0YSkge1xuICAgICAgICBkYXRhID0gZGF0YSB8fCB7fTtcbiAgICAgICAgdmFyIHBhY2tldCA9IHtcbiAgICAgICAgICAgICdtYWlsJyA6IGRhdGFbJ2VtYWlsJ11cbiAgICAgICAgICAgICwndXNlciBpZCcgOiBkYXRhWyd1c2VyIGlkJ11cbiAgICAgICAgICAgICwnY29tbWVudHMnIDogZGF0YVsnY29tbWVudHMnXVxuICAgICAgICB9O1xuICAgICAgICB2YXIgcHJvbWlzZSA9IGNvbnRleHQuYWN0aW9uc1snc2VuZC1tYWlsJ10oe2ZpbHRlcnM6IHBhY2tldH0pO1xuICAgICAgICBjb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbJ2ZpbmFsLXBhZ2UtdmlldyddLnB1c2gocHJvbWlzZSk7XG4gICAgICAgIHByb21pc2UudGhlbihmdW5jdGlvbiAocmVzdWx0KSB7XG4gICAgICAgICAgICBjb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbJ2ZpbmFsLXBhZ2UtdmlldyddLnNwbGljZShcbiAgICAgICAgICAgICAgICBjb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbJ2ZpbmFsLXBhZ2UtdmlldyddLmluZGV4T2YocHJvbWlzZSksIDFcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgICBpZiAocmVzdWx0LmV2ZW50KSB7XG4gICAgICAgICAgICAgICAgY29udGV4dC5ldmVudHNbcmVzdWx0LmV2ZW50XShjb250ZXh0LCByZXN1bHQuZGF0YSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH07XG59O1xuIiwiLypqc2xpbnQgbm9kZTogdHJ1ZSwgbm9tZW46IHRydWUgKi9cblwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLmNyZWF0ZUV2ZW50ID0gZnVuY3Rpb24gKCkgeyAvLyBhZGQgXCJvcHRpb25zXCIgcGFyYW1ldGVyIGlmIG5lZWRlZFxuICAgIHJldHVybiBmdW5jdGlvbiAoY29udGV4dCwgZGF0YSkge1xuICAgICAgICBkYXRhID0gZGF0YSB8fCB7fTtcbiAgICAgICAgdmFyIHBhY2tldCA9IHtcbiAgICAgICAgICAgICd1c2VyIGlkJyA6IGRhdGFbJ3VzZXIgaWQnXVxuICAgICAgICAgICAgLCd2YWx1ZXMgaGlzdG9yeScgOiBkYXRhWyd2YWx1ZXMgaGlzdG9yeSddXG4gICAgICAgICAgICAsJ2NvbXBsZXhpdHknIDogZGF0YVsnY29tcGxleGl0eSddXG4gICAgICAgICAgICAsJ3N1cnZleSBhbnN3ZXJzJyA6IGRhdGFbJ3N1cnZleSBhbnN3ZXJzJ11cbiAgICAgICAgfTtcbiAgICAgICAgdmFyIHByb21pc2UgPSBjb250ZXh0LmFjdGlvbnNbJ3NlbmQtcHJlZmVyZW5jZSddKHtmaWx0ZXJzOiBwYWNrZXR9KTtcbiAgICAgICAgY29udGV4dC5ydW5uaW5nQWN0aW9uc0J5Q29udGFpbmVyWyd0ZXN0LXZpZXcnXS5wdXNoKHByb21pc2UpO1xuICAgICAgICBwcm9taXNlLnRoZW4oZnVuY3Rpb24gKHJlc3VsdCkge1xuICAgICAgICAgICAgY29udGV4dC5ydW5uaW5nQWN0aW9uc0J5Q29udGFpbmVyWyd0ZXN0LXZpZXcnXS5zcGxpY2UoXG4gICAgICAgICAgICAgICAgY29udGV4dC5ydW5uaW5nQWN0aW9uc0J5Q29udGFpbmVyWyd0ZXN0LXZpZXcnXS5pbmRleE9mKHByb21pc2UpLCAxXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgaWYgKHJlc3VsdC5ldmVudCkge1xuICAgICAgICAgICAgICAgIGNvbnRleHQuZXZlbnRzW3Jlc3VsdC5ldmVudF0oY29udGV4dCwgcmVzdWx0LmRhdGEpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9O1xufTtcbiIsIi8qanNsaW50IG5vZGU6IHRydWUsIG5vbWVuOiB0cnVlICovXG5cInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5jcmVhdGVFdmVudCA9IGZ1bmN0aW9uICgpIHsgLy8gYWRkIFwib3B0aW9uc1wiIHBhcmFtZXRlciBpZiBuZWVkZWRcbiAgICByZXR1cm4gZnVuY3Rpb24gKGNvbnRleHQsIGRhdGEpIHtcbiAgICAgICAgZGF0YSA9IGRhdGEgfHwge307XG4gICAgICAgIHZhciBwYWNrZXQgPSB7XG4gICAgICAgICAgICAndGVzdCBudW1iZXInIDogMCxcbiAgICAgICAgICAgICd1c2VyIGlkJyA6IGRhdGFbJ3VzZXIgaWQnXSxcbiAgICAgICAgICAgICdzdXJ2ZXkgYW5zd2VycycgOiBkYXRhWydzdXJ2ZXkgYW5zd2VycyddXG4gICAgICAgIH07XG4gICAgICAgIHZhciBwcm9taXNlID0gY29udGV4dC5hY3Rpb25zWydjb21wdXRlLWZpcnN0LWNob3JkcyddKHtmaWx0ZXJzOiBwYWNrZXR9KTtcbiAgICAgICAgY29udGV4dC5ydW5uaW5nQWN0aW9uc0J5Q29udGFpbmVyWydzdXJ2ZXktdmlldyddLnB1c2gocHJvbWlzZSk7XG4gICAgICAgIHByb21pc2UudGhlbihmdW5jdGlvbiAocmVzdWx0KSB7XG4gICAgICAgICAgICBjb250ZXh0LnJ1bm5pbmdBY3Rpb25zQnlDb250YWluZXJbJ3N1cnZleS12aWV3J10uc3BsaWNlKFxuICAgICAgICAgICAgICAgIGNvbnRleHQucnVubmluZ0FjdGlvbnNCeUNvbnRhaW5lclsnc3VydmV5LXZpZXcnXS5pbmRleE9mKHByb21pc2UpLCAxXG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgaWYgKHJlc3VsdC5ldmVudCkge1xuICAgICAgICAgICAgICAgIGNvbnRleHQuZXZlbnRzW3Jlc3VsdC5ldmVudF0oY29udGV4dCwgcmVzdWx0LmRhdGEpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9O1xufTtcbiIsIi8qanNsaW50IG5vZGU6IHRydWUsIG5vbWVuOiB0cnVlICovXG5cInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5jcmVhdGVFdmVudCA9IGZ1bmN0aW9uICgpIHsgLy8gYWRkIFwib3B0aW9uc1wiIHBhcmFtZXRlciBpZiBuZWVkZWRcbiAgICByZXR1cm4gZnVuY3Rpb24gKGNvbnRleHQpIHtcbiAgICAgICAgdmFyIHByb21pc2UgPSBjb250ZXh0LmFjdGlvbnNbJ2dldC1pZCddKCk7XG4gICAgICAgIGNvbnRleHQucnVubmluZ0FjdGlvbnNCeUNvbnRhaW5lclsnaG9tZS1wYWdlJ10ucHVzaChwcm9taXNlKTtcbiAgICAgICAgcHJvbWlzZS50aGVuKGZ1bmN0aW9uIChyZXN1bHQpIHtcbiAgICAgICAgICAgIGNvbnRleHQucnVubmluZ0FjdGlvbnNCeUNvbnRhaW5lclsnaG9tZS1wYWdlJ10uc3BsaWNlKFxuICAgICAgICAgICAgICAgIGNvbnRleHQucnVubmluZ0FjdGlvbnNCeUNvbnRhaW5lclsnaG9tZS1wYWdlJ10uaW5kZXhPZihwcm9taXNlKSwgMVxuICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIGlmIChyZXN1bHQuZXZlbnQpIHtcbiAgICAgICAgICAgICAgICBjb250ZXh0LmV2ZW50c1tyZXN1bHQuZXZlbnRdKGNvbnRleHQsIHJlc3VsdC5kYXRhKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfTtcbn07XG4iLCIvKmpzbGludCBub2RlOiB0cnVlLCBub21lbjogdHJ1ZSAqL1xuXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBrbyA9ICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93WydrbyddIDogdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbFsna28nXSA6IG51bGwpLFxuICAgIHJlcG9zaXRvcmllcyA9IHJlcXVpcmUoJy4vcmVwb3NpdG9yaWVzJyksXG4gICAgY29udHJvbHMgPSByZXF1aXJlKCcuL2NvbnRyb2xzJyksXG4gICAgZXZlbnRzID0gcmVxdWlyZSgnLi9ldmVudHMnKSxcbiAgICBhY3Rpb25zID0gcmVxdWlyZSgnLi9hY3Rpb25zJyksXG4gICAgUHJvbWlzZSA9ICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93WydQcm9taXNlJ10gOiB0eXBlb2YgZ2xvYmFsICE9PSBcInVuZGVmaW5lZFwiID8gZ2xvYmFsWydQcm9taXNlJ10gOiBudWxsKTtcblxuUHJvbWlzZS5jb25maWcoe2NhbmNlbGxhdGlvbjogdHJ1ZX0pO1xuXG5jb250cm9scy5yZWdpc3RlcigpO1xuLy8gVE9ETzogcmVnaXN0ZXIgYW55IGN1c3RvbSBjb250cm9sXG5cbmZ1bmN0aW9uIEFwcGxpY2F0aW9uVmlld01vZGVsKCkge1xuICAgIC8vIFRPRE86IGluaXRpYWxpemUgZ2xvYmFsIHN0YXRlXG4gICAgdmFyIHJlcG9zID0gcmVwb3NpdG9yaWVzLmNyZWF0ZVJlcG9zaXRvcmllcyh7fSk7XG4gICAgdGhpcy5jb250ZXh0ID0ge1xuICAgICAgICByZXBvc2l0b3JpZXM6IHJlcG9zLFxuICAgICAgICBldmVudHM6IGV2ZW50cy5jcmVhdGVFdmVudHMoe30pLFxuICAgICAgICBhY3Rpb25zOiBhY3Rpb25zLmNyZWF0ZUFjdGlvbnMoe3JlcG9zaXRvcmllczogcmVwb3N9KSxcbiAgICAgICAgdm1zOiB7fSxcbiAgICAgICAgcnVubmluZ0FjdGlvbnNCeUNvbnRhaW5lcjoge31cbiAgICB9O1xufVxuXG52YXIgYXBwbGljYXRpb24gPSBuZXcgQXBwbGljYXRpb25WaWV3TW9kZWwoKTtcblxua28uYXBwbHlCaW5kaW5ncyhhcHBsaWNhdGlvbik7XG5cbmFwcGxpY2F0aW9uLmNvbnRleHQudG9wLmluaXQoKTtcbiIsIi8qanNsaW50IG5vZGU6dHJ1ZSwgbm9tZW46IHRydWUgKi9cblwidXNlIHN0cmljdFwiO1xuXG52YXIgJCA9ICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiID8gd2luZG93WyckJ10gOiB0eXBlb2YgZ2xvYmFsICE9PSBcInVuZGVmaW5lZFwiID8gZ2xvYmFsWyckJ10gOiBudWxsKSxcblByb21pc2UgPSAodHlwZW9mIHdpbmRvdyAhPT0gXCJ1bmRlZmluZWRcIiA/IHdpbmRvd1snUHJvbWlzZSddIDogdHlwZW9mIGdsb2JhbCAhPT0gXCJ1bmRlZmluZWRcIiA/IGdsb2JhbFsnUHJvbWlzZSddIDogbnVsbCk7XG5cblxuZnVuY3Rpb24gQVBJX2NhbGxlcigpIHtcblx0aWYgKCEodGhpcyBpbnN0YW5jZW9mIEFQSV9jYWxsZXIpKSB7XG5cdFx0cmV0dXJuIG5ldyBBUElfY2FsbGVyKCk7XG5cdH1cblx0dGhpcy5zZXJ2ZXJfdXJsID0gJ2h0dHBzOi8vaGFybW9uaWNjb21wbGV4aXR5Lmhlcm9rdWFwcC5jb20nO1xufVxuXG5BUElfY2FsbGVyLnByb3RvdHlwZS5jYWxsX2FwaSA9IGZ1bmN0aW9uKHVybF9lbmQsIGNhbGxfdHlwZSwgY2FsbF9kYXRhKSB7XG5cdHZhciBzZWxmID0gdGhpcztcblx0cmV0dXJuIG5ldyBQcm9taXNlKGZ1bmN0aW9uKHJlc29sdmUsIHJlamVjdCkge1xuXHRcdCQuYWpheCh7XG5cdFx0XHR1cmw6IHNlbGYuc2VydmVyX3VybCArIHVybF9lbmQsXG5cdFx0XHR0eXBlOiBjYWxsX3R5cGUsXG5cdFx0XHRoZWFkZXJzOiB7XG5cdFx0XHRcdCdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbjsgY2hhcnNldD11dGYtOCdcblx0XHRcdH0sXG5cdFx0XHRjb250ZW50VHlwZTogXCJhcHBsaWNhdGlvbi9qc29uXCIsXG5cdFx0XHRkYXRhOiBjYWxsX2RhdGFcblx0XHR9KS5kb25lKGZ1bmN0aW9uKHJlc3VsdCkge1xuXHRcdFx0cmVzb2x2ZShyZXN1bHQpO1xuXHRcdH0pLmZhaWwoZnVuY3Rpb24oanFYSFIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKSB7XG5cdFx0XHR2YXIgZXJyb3IgPSBuZXcgRXJyb3IoZXJyb3JUaHJvd24pO1xuXHRcdFx0ZXJyb3IudGV4dFN0YXR1cyA9IHRleHRTdGF0dXM7XG5cdFx0XHRlcnJvci5qcVhIUiA9IGpxWEhSO1xuXHRcdFx0cmVqZWN0KGVycm9yKTtcblx0XHR9KTtcblx0fSk7XG59O1xuXG5leHBvcnRzLkFQSV9jYWxsZXIgPSBBUElfY2FsbGVyO1xuIiwiLypqc2xpbnQgbm9kZTogdHJ1ZSwgbm9tZW46IHRydWUgKi9cblwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLmNyZWF0ZVJlcG9zaXRvcmllcyA9IGZ1bmN0aW9uIChvcHRpb25zKSB7XG4gICAgdmFyIHJlcG9zaXRvcmllcyA9IHt9XG4gICAgcmVwb3NpdG9yaWVzWydhcGlfY2FsbGVyJ10gPSByZXF1aXJlKCcuL2FwaV9jYWxsZXInKS5BUElfY2FsbGVyKG9wdGlvbnMpXG4gICAgcmV0dXJuIHJlcG9zaXRvcmllcztcbn07XG4iXX0=
