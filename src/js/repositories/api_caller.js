/*jslint node:true, nomen: true */
"use strict";

var $ = require('jquery'),
Promise = require('bluebird');


function API_caller() {
	if (!(this instanceof API_caller)) {
		return new API_caller();
	}
	this.server_url = 'https://harmoniccomplexity.herokuapp.com';
}

API_caller.prototype.call_api = function(url_end, call_type, call_data) {
	var self = this;
	return new Promise(function(resolve, reject) {
		$.ajax({
			url: self.server_url + url_end,
			type: call_type,
			headers: {
				'Content-Type': 'application/json; charset=utf-8'
			},
			contentType: "application/json",
			data: call_data
		}).done(function(result) {
			resolve(result);
		}).fail(function(jqXHR, textStatus, errorThrown) {
			var error = new Error(errorThrown);
			error.textStatus = textStatus;
			error.jqXHR = jqXHR;
			reject(error);
		});
	});
};

exports.API_caller = API_caller;
