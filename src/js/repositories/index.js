/*jslint node: true, nomen: true */
"use strict";

exports.createRepositories = function (options) {
    var repositories = {}
    repositories['api_caller'] = require('./api_caller').API_caller(options)
    return repositories;
};
