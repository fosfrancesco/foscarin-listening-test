/*jslint node: true, nomen: true */
/*jshint sub:true*/
/*globals AudioContext*/
/*globals Soundfont*/
"use strict";

var ko = require('knockout'),
    Promise = require('bluebird');

function ViewModel(params) {
    var self = this;
    self.context = params.context;
    self.status = ko.observable('');
    self.fields = ko.observable({});
    self.errors = ko.observable({});
    self.piano = undefined;
    self.loading = ko.observable(true);  //used to display "Loading" text
    self.ready = ko.observable(false); //used to display the page when the samples are loaded
    self.ac = new AudioContext();
    self.sliderValue = ko.observable(0);
    self.isPlaying =  ko.observable(false); //used to stop the infinite playing when we want to left the page
    self.playButtonEnabled = ko.observable(true); //used to avoid rapid double click on play
    self.isLastTest = ko.observable(false);
    self.percentageValue = ko.observable(0);
    self.testNumber = ko.observable(1);
    self.isSync = ko.observable(true);
    self.isChordPlayed = [ko.observable(false),ko.observable(false),ko.observable(false),ko.observable(false),ko.observable(false)];
    self.disableNextButton = ko.observable(true);
    self.button0Status = ko.pureComputed(function() {
        if (self.isChordPlayed[0]() == false){
            return 'btn-default'
        }
        else if (self.isSync()) return 'btn-success'
        else return 'btn-warning';

    });
    self.button1Status = ko.pureComputed(function() {
        if (self.isChordPlayed[1]() == false){
            return 'btn-default'
        }
        else if (self.isSync()) return 'btn-success';
        else return 'btn-warning';
    });
    self.button2Status = ko.pureComputed(function() {
        if (self.isChordPlayed[2]() == false){
            return 'btn-default';
        }
        else if (self.isSync()) return 'btn-success';
        else return 'btn-warning';
    });
    self.button3Status = ko.pureComputed(function() {
        if (self.isChordPlayed[3]() == false){
            return 'btn-default';
        }
        else if (self.isSync()) return 'btn-success';
        else return 'btn-warning';
    });
    self.button4Status = ko.pureComputed(function() {
        if (self.isChordPlayed[4]() == false){
            return 'btn-default';
        }
        else if (self.isSync()) return 'btn-success';
        else return 'btn-warning';
    });

    self.trigger = function (id) {
        self.context.events[id](self.context, self.output);
    };

    self.updateIsLastTest = function(){
        self.percentageValue(Math.round(self.fields()['test number']() *100/8));
        if (self.fields()['test number']() == 7){
            self.isLastTest(true);
            self.testNumber(self.fields()['test number']() +1);
        }
        else{
            self.isLastTest(false);
            self.testNumber(self.fields()['test number']() +1);
        }

    };

    self.loadSamples = function() {
        Soundfont.instrument(self.ac, 'acoustic_grand_piano').then(function (piano) {
            self.piano = piano;
            self.loading(false);
            self.ready(true);
            // self.playChords();
        });
    };
    self.loadSamples();  //load the samples when the page is loading

    //simply play the input array
    self.playNotes = function(noteArray){
        for (var n in noteArray){
            self.piano.play(noteArray[n], self.ac.currentTime, { duration: 1});
        }
    };

    //first call to the recursive function to play fields()['chords']()
    self.playChords = function(){
        self.disableNextButton(false); //enable to change test
        //reset the colors
        for (var ii in self.isChordPlayed){
            self.isChordPlayed[ii](false);
        }
        var index = 0;
        self.isSync(true);
        self.fields()['complexity'](self.sliderValue() || 0);
        var progressions = self.fields()['progressions']()[self.fields()['complexity']()];
        var random_index = Math.floor(Math.random() * (progressions.length-1));
        var chords = progressions[random_index].chords;
        self.recursivePlay(index,chords);
    };
    //recursively play chords in order to wait 1 second between chords in non-blocking way
    self.recursivePlay = function(index, chords){
        //play the first 4 chords
        if (index < Object.keys(chords).length -1 && self.isPlaying()){
            self.isChordPlayed[index](true);
            self.playNotes(chords[index]);
            var newIndex = index +1;
            if (self.isPlaying()){ //add a control to avoid play-click during the wait second to restart the progression
                setTimeout(function() {
                    self.recursivePlay(newIndex,chords);
                }, 1000);
            }
        }
        else{ //play the last chord
            if (self.isPlaying()){
                self.isChordPlayed[index](true);
                self.playNotes(chords[index]);
                //save the complexity value
                self.fields()['values history']()[self.fields()['test number']()].push(self.fields()['complexity']());
                if (self.isPlaying()){ //add a control to avoid play-click during the wait second to restart the progression
                    //wait 1 s for the chord and play the new progression after another 1 second
                    setTimeout(function() {
                        //reset the colors
                        for (var ii in self.isChordPlayed){
                            self.isChordPlayed[ii](false);
                        }
                        if (self.isPlaying()){ //add a control to avoid play-click during the wait second to restart the progression
                            setTimeout(function() {
                                self.playChords();
                            }, 1000);
                        }
                    }, 1000);
                }
            }
        }
    };

    //stop the infinite sound and go to comments
    self.sendPreference = function(){
        self.disableNextButton(true); //disable  to avoid double clicks
        //store the current complexity
        self.fields()['values history']()[self.fields()['test number']()].push(self.fields()['complexity']());
        self.isPlaying(false);
        self.trigger('send-preference-event');
    };

    //stop the infinite sound and go to next test
    self.nextTest = function(){
        self.disableNextButton(true); //disable  to avoid double clicks
        //reset the colors
        for (var ii in self.isChordPlayed){
            self.isChordPlayed[ii](false);
        }
        //store the current complexity
        self.fields()['values history']()[self.fields()['test number']()].push(self.fields()['complexity']());
        self.isPlaying(false);
        self.fields()['complexity'](0);
        self.sliderValue(0);
        self.trigger('next-test-event');
    };

    self.play =function(){
        if(!self.isPlaying()){
            self.isPlaying(true);
            self.playChords();
        }

    };

    self.pause = function(){
        self.playButtonEnabled(false);
        self.isPlaying(false);
        setTimeout(function() {
            self.playButtonEnabled(true);
        }, 1010);
    };


    self.sliderValue.subscribe(function (value) {
        self.playButtonEnabled(false); //to avoid fast double click before the progresion stop
        self.isPlaying(false);
        self.isSync(false);
        setTimeout(function() {
            self.playButtonEnabled(true);
        }, 1010);
    });

}

ViewModel.prototype.id = 'test-form';

ViewModel.prototype.waitForStatusChange = function () {
    return this._initializing ||
           Promise.resolve();
};

ViewModel.prototype._compute = function () {
    this.output = {
        'progressions': this.input['progressions'],
        'complexity': this.input['complexity'],
        'survey answers': this.input['survey answers'],
        'user id': this.input['user id'],
        'values history': this.input['values history'],
        'test number' : this.input['test number'],
    };
    var self = this,
        fields = {
            'progressions': ko.observable(this.input['progressions']),
            'complexity': ko.observable(this.input['complexity']),
            'survey answers': ko.observable(this.input['survey answers']),
            'user id': ko.observable(this.input['user id']),
            'values history': ko.observable(this.input['values history']),
            'test number' : ko.observable(this.input['test number']),
        },
        errors = {
            'progressions': ko.observable(this.input['progressions-error']),
            'complexity': ko.observable(this.input['complexity-error']),
            'survey answers': ko.observable(this.input['survey answers-error']),
            'user id': ko.observable(this.input['user id-error']),
            'values history': ko.observable(this.input['values history-error']),
            'test number' : ko.observable(this.input['test number-error'])
        };
    fields['progressions'].subscribe(function (value) {
        self.output['chords'] = value;
        self.errors()['chords'](undefined);
    });
    fields['complexity'].subscribe(function (value) {
        self.output['complexity'] = value;
        self.errors()['complexity'](undefined);
    });
    fields['survey answers'].subscribe(function (value) {
        self.output['survey answers'] = value;
        self.errors()['survey answers'](undefined);
    });
    fields['user id'].subscribe(function (value) {
        self.output['user id'] = value;
        self.errors()['user id'](undefined);
    });
    fields['values history'].subscribe(function (value) {
        self.output['values history'] = value;
        self.errors()['values history'](undefined);
    });
    fields['test number'].subscribe(function (value) {
        self.output['test number'] = value;
        self.errors()['test number'](undefined);
    });
    this.fields(fields);
    self.updateIsLastTest();
    this.errors(errors);
    this.status('computed');
};


ViewModel.prototype.init = function (options) {
    options = options || {};
    this.output = undefined;
    this.fields({});
    this.errors({});
    this.input = options.input || {};
    this.status('ready');
    var self = this;
    this._initializing = new Promise(function (resolve) {
        setTimeout(function () {
            self._compute();
            resolve();
            self._initializing = undefined;
        }, 1);
    });
};

exports.register = function () {
    ko.components.register('c-test-form', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                var vm = new ViewModel(params);
                params.context.vms[vm.id] = vm;
                ko.utils.domNodeDisposal.addDisposeCallback(componentInfo.element, function () { delete params.context.vms[vm.id]; });
                return vm;
            }
        },
        template: require('./index.html'),
        synchronous: true
    });
};
