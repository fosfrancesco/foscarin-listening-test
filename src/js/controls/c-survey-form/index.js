/*jslint node: true, nomen: true */
/*globals Survey*/
"use strict";

var ko = require('knockout'),
    Promise = require('bluebird');

function ViewModel(params) {
    var self = this;
    self.context = params.context;
    self.status = ko.observable('');
    self.fields = ko.observable({});
    self.errors = ko.observable({});
    self.surveyCompleted = ko.observable(false);
    self.surveyJSON = {
        pages:[{elements:[{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"1",title:"I spend a lot of my free time doing music-related activities."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"2",title:"I sometimes choose music that can trigger shivers down my spine."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"3",title:"I enjoy writing about music, for example on blogs and forums."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"4",title:"If somebody starts singing a song I don’t know, I can usually join in."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"5",title:"I am able to judge whether someone is a good singer or not."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"6",title:"I usually know when I’m hearing a song for the first time."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"7",title:"I can sing or play music from memory."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"8",title:"I’m intrigued by musical styles I’m not familiar with and want to find out more."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"9",title:"Pieces of music rarely evoke emotions for me."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"10",title:"I am able to hit the right notes when I sing along with a recording."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"11",title:"I find it difficult to spot mistakes in a performance of a song even if I know the tune."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"12",title:"I can compare and discuss differences between two performances or versions of the same piece of music."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"13",title:"I have trouble recognizing a familiar song when played in a different way or by a different performer."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"14",title:"I have never been complimented for my talents as a musical performer."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"15",title:"I often read or search the internet for things related to music."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"16",title:"I often pick certain music to motivate or excite me."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"17",title:"I am not able to sing in harmony when somebody is singing a familiar tune."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"18",title:"I can tell when people sing or play out of time with the beat."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"19",title:"I am able to identify what is special about a given musical piece."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"20",title:"I am able to talk about the emotions that a piece of music evokes for me."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"21",title:"I don’t spend much of my disposable income on music."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"22",title:"I can tell when people sing or play out of tune."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"23",title:"When I sing, I have no idea whether I’m in tune or not."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"24",title:"Music is kind of an addiction for me - I couldn’t live without it."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"25",title:"I don’t like singing in public because I’m afraid that I would sing wrong notes."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"26",title:"When I hear a piece of music I can usually identify its genre."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"27",title:"I would not consider myself a musi- cian."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"28",title:"I keep track of new music that I come across (e.g. new artists or recordings)."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"29",title:"After hearing a new song two or three times, I can usually sing it by myself."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"30",title:"I only need to hear a new tune once and I can sing it back hours later."},{type:"radiogroup",choices:[{value:"1",text:"Completely Disagree"},{value:"2",text:"Strongly Disagree"},{value:"3",text:"Disagree"},{value:"4",text:"Neither Agree nor Disagree"},{value:"5",text:"Agree"},{value:"6",text:"Strongly Agree"},{value:"7",text:"Completely Agree"}],isRequired:true,name:"31",title:"Music can evoke my memories of past people and places."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"1"},{value:"3",text:"2"},{value:"4",text:"3"},{value:"5",text:"4 - 5"},{value:"6",text:"6 - 9"},{value:"7",text:"10 or more"}],isRequired:true,name:"32",title:"I engaged in regular, daily practice of a musical instrument (including voice) for ___ years."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"0.5"},{value:"3",text:"1"},{value:"4",text:"1.5"},{value:"5",text:"2"},{value:"6",text:"3 - 4"},{value:"7",text:"5 or more"}],isRequired:true,name:"33",title:"At the peak of my interest, I practiced ___ hours per day on my primary instrument."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"1"},{value:"3",text:"2"},{value:"4",text:"3"},{value:"5",text:"4 - 6"},{value:"6",text:"7 - 10"},{value:"7",text:"11 or more"}],isRequired:true,name:"34",title:"I have attended ___ live music events as an audience member in the past twelve months."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"0.5"},{value:"3",text:"1"},{value:"4",text:"2"},{value:"5",text:"3"},{value:"6",text:"4 - 6"},{value:"7",text:"7 or more"}],isRequired:true,name:"35",title:"I have had formal training in music theory for ___ years."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"0.5"},{value:"3",text:"1"},{value:"4",text:"2"},{value:"5",text:"3 -5"},{value:"6",text:"6 - 9 "},{value:"7",text:"10 or more"}],isRequired:true,name:"36",title:"I have had ___ years of formal training on a musical instrument (including voice) during my lifetime."},{type:"radiogroup",choices:[{value:"1",text:"0"},{value:"2",text:"1"},{value:"3",text:"2"},{value:"4",text:"3"},{value:"5",text:"4"},{value:"6",text:"5"},{value:"7",text:"6 or more"}],isRequired:true,name:"37",title:"I can play ___ musical instruments."},{type:"radiogroup",choices:[{value:"1",text:"0 - 15 min"},{value:"2",text:"15 - 30 min"},{value:"3",text:"30 - 60 min"},{value:"4",text:"60 - 90 min"},{value:"5",text:"2 hrs"},{value:"6",text:"2 - 3 hrs"},{value:"7",text:"4 hrs or more"}],isRequired:true,name:"38",title:"I listen attentively to music for ___ per day."},{type:"radiogroup",choices:[{value:"1",text:"Pop/Rock Music"},{value:"2",text:"Jazz Music"},{value:"3",text:"Classical Music"},{value:"4",text:"Everything"}],isRequired:true,name:"I usually listen to ___"}],name:"page1"}]
    };

    Survey.defaultBootstrapCss.navigationButton = 'btn btn-primary';
    Survey.Survey.cssType = 'bootstrap';
    self.model = new Survey.Model(self.surveyJSON);
    self.saveSurveyResults = function (survey){
        // var resultAsString = JSON.stringify(survey.data);
        // alert(resultAsString); //send Ajax request to your web server.
        self.fields()['survey answers'](survey.data);
        self.surveyCompleted(true);
    };
    //Use onComplete event to save the data
    self.model.onComplete.add(self.saveSurveyResults);

    self.trigger = function (id) {
        self.context.events[id](self.context, self.output);
    };
}

ViewModel.prototype.id = 'survey-form';

ViewModel.prototype.waitForStatusChange = function () {
    return this._initializing ||
           Promise.resolve();
};

ViewModel.prototype._compute = function () {
    this.output = {
        'survey answers': this.input['survey answers'],
        'user id': this.input['user id'],
    }
    var self = this,
        fields = {
            'survey answers': ko.observable(this.input['survey answers']),
            'user id': ko.observable(this.input['user id']),
        },
        errors = {
            'survey answers': ko.observable(this.input['survey answers-error']),
            'user id': ko.observable(this.input['user id-error']),
        };
    fields['survey answers'].subscribe(function (value) {
        self.output['survey answers'] = value;
        self.errors()['survey answers'](undefined);
    });
    fields['user id'].subscribe(function (value) {
        self.output['user id'] = value;
        self.errors()['user id'](undefined);
    });
    this.fields(fields);
    this.errors(errors);
    this.status('computed');
};


ViewModel.prototype.init = function (options) {
    options = options || {};
    this.output = undefined;
    this.fields({});
    this.errors({});
    this.input = options.input || {};
    this.status('ready');
    var self = this;
    this._initializing = new Promise(function (resolve) {
        setTimeout(function () {
            self._compute();
            resolve();
            self._initializing = undefined;
        }, 1);
    });
};

exports.register = function () {
    ko.components.register('c-survey-form', {
        viewModel: {
            createViewModel: function (params, componentInfo) {
                var vm = new ViewModel(params);
                params.context.vms[vm.id] = vm;
                ko.utils.domNodeDisposal.addDisposeCallback(componentInfo.element, function () { delete params.context.vms[vm.id]; });
                return vm;
            }
        },
        template: require('./index.html'),
        synchronous: true
    });
};
