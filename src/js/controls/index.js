/*jslint node: true, nomen: true */
"use strict";

var ko = require('knockout');

exports.register = function () {
    require('./main-application').register();
    require('./c-final-page-view').register();
    require('./c-home-page').register();
    require('./c-survey-view').register();
    require('./c-test-view').register();
    require('./c-survey-form').register();
    require('./c-test-form').register();
    require('./c-email-form').register();
    require('./c-closing-page').register();
    require('./c-error-view').register();
};
