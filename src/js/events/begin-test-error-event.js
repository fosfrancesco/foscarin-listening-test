/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context) {
        if (!context.vms['error-view']) {
            context.top.active('error-view');
        }
        context.vms['error-view'].init();
    };
};
