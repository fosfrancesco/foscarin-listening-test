/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        data = data || {};
        var packet = {
            'test number' : 0,
            'user id' : data['user id'],
            'survey answers' : data['survey answers']
        };
        var promise = context.actions['compute-first-chords']({filters: packet});
        context.runningActionsByContainer['survey-view'].push(promise);
        promise.then(function (result) {
            context.runningActionsByContainer['survey-view'].splice(
                context.runningActionsByContainer['survey-view'].indexOf(promise), 1
            );
            if (result.event) {
                context.events[result.event](context, result.data);
            }
        });
    };
};
