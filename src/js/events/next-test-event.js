/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        data = data || {};
        var packet = {
            'survey answers' : data['survey answers']
            ,'user id' : data['user id']
            ,'values history' : data['values history']
            ,'test number' : data['test number'] +1
        };
        var promise = context.actions['compute-first-chords']({filters: packet});
        context.runningActionsByContainer['test-view'].push(promise);
        promise.then(function (result) {
            context.runningActionsByContainer['test-view'].splice(
                context.runningActionsByContainer['test-view'].indexOf(promise), 1
            );
            if (result.event) {
                context.events[result.event](context, result.data);
            }
        });
    };
};
