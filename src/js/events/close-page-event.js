/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context) {
        if (!context.vms['closing-page']) {
            context.top.active('closing-page');
        }
        context.vms['closing-page'].init();
    };
};
