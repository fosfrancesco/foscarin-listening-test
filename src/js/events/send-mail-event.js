/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        data = data || {};
        var packet = {
            'mail' : data['email']
            ,'user id' : data['user id']
            ,'comments' : data['comments']
        };
        var promise = context.actions['send-mail']({filters: packet});
        context.runningActionsByContainer['final-page-view'].push(promise);
        promise.then(function (result) {
            context.runningActionsByContainer['final-page-view'].splice(
                context.runningActionsByContainer['final-page-view'].indexOf(promise), 1
            );
            if (result.event) {
                context.events[result.event](context, result.data);
            }
        });
    };
};
