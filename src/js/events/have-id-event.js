/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        if (!context.vms['survey-view']) {
            context.top.active('survey-view');
            context.vms['survey-view'].init({mask: 'survey-form'});
        }
        data = data || {};
        var packet = {
            'user id' : data['user id']
        };
        context.vms['survey-form'].init({input: packet});
    };
};
