/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        if (!context.vms['test-view']) {
            context.top.active('test-view');
            context.vms['test-view'].init({mask: 'test-form'});
        }
        data = data || {};
        var packet = {
            'user id' : data['user id'],
            'progressions' : data['progressions'],
            'survey answers' : data['survey answers'],
            'values history' : data['values history'],
            'test number' : data['test number']
        };
        context.vms['test-form'].init({input: packet});
    };
};
