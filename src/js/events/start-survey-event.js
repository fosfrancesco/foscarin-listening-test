/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context) {
        var promise = context.actions['get-id']();
        context.runningActionsByContainer['home-page'].push(promise);
        promise.then(function (result) {
            context.runningActionsByContainer['home-page'].splice(
                context.runningActionsByContainer['home-page'].indexOf(promise), 1
            );
            if (result.event) {
                context.events[result.event](context, result.data);
            }
        });
    };
};
