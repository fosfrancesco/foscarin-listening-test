/*jslint node: true, nomen: true */
"use strict";

exports.createEvent = function () { // add "options" parameter if needed
    return function (context, data) {
        if (!context.vms['final-page-view']) {
            context.top.active('final-page-view');
            context.vms['final-page-view'].init({mask: 'email-form'});
        }
        data = data || {};
        var packet = {
            'user id' : data['user id']
        };
        context.vms['email-form'].init({input: packet});
    };
};
