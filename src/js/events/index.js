/*jslint node: true, nomen: true */
"use strict";

exports.createEvents = function (options) {
    return {
        'start-survey-event': require('./start-survey-event').createEvent(options)
        ,'send-survey-event': require('./send-survey-event').createEvent(options)
        ,'begin-test-event': require('./begin-test-event').createEvent(options)
        ,'final-page-event': require('./final-page-event').createEvent(options)
        ,'have-id-event': require('./have-id-event').createEvent(options)
        ,'send-preference-event': require('./send-preference-event').createEvent(options)
        ,'send-mail-event': require('./send-mail-event').createEvent(options)
        ,'next-test-event' : require('./next-test-event').createEvent(options)
        ,'close-page-event': require('./close-page-event').createEvent(options)
        ,'have-id-error-event': require('./have-id-error-event').createEvent(options)
        ,'begin-test-error-event': require('./begin-test-error-event').createEvent(options)
        ,'final-page-error-event': require('./final-page-error-event').createEvent(options)
    };
};
