/*jslint node: true, nomen: true */
/*globals $*/
"use strict";

var Promise = require('bluebird');

function Action(options) { // add "options" parameters if needed
    // TODO: Global Initialization
    this.api_caller = options.repositories.api_caller;
}

Action.prototype.run = function (parameters, solve) { // add "onCancel" parameters if needed
    // Parameters:
    // parameters['survey answers']
    // parameters['user id']
    // parameters['test number']
    var tables = {
        '0' : 'progression_compound_maj',
        '1' : 'progression_compound_min',
        '2' : 'progression_ngram_maj',
        '3' : 'progression_ngram_min',
        '4' : 'progression_hmm_maj',
        '5' : 'progression_hmm_min',
        '6' : 'progression_rnn_maj',
        '7' : 'progression_rnn_min'
    };

    // TODO: Execution
    var self = this;
    var url = '/api/progression/' + tables[parameters['test number']];
    self.api_caller.call_api(url,'GET', {}).then(function(result){
        // $.notify({message: 'Downloaded chords'}, {allow_dismiss: true, type: 'success'});
        solve({
            event: 'begin-test-event', // Begin Test
            data: {
                'progressions': result.progressions,
                'survey answers': parameters['survey answers'],
                'user id': parameters['user id'],
                'values history': parameters['values history'] || {
                    0 : [],
                    1 : [],
                    2 : [],
                    3 : [],
                    4 : [],
                    5 : [],
                    6 : [],
                    7 : []
                },
                'test number' : parameters['test number']
            }
        });
    }).catch(function(result){
        solve({
            event: 'begin-test-error-event',
            data: {}
        });
    });
    // THIS CAN BE REMOVED (BEGIN)
    // $.notify({message: 'Compute first chords'}, {allow_dismiss: true, type: 'success'});
    // solve({
    //     event: 'begin-test-event', // Begin Test
    //     data: {
    //         'chords': '0',
    //         'survey answers': parameters['survey answers'],
    //         'user id': parameters['user id'],
    //     }
    // });
    // THIS CAN BE REMOVED (END)
};

exports.createAction = function (options) {
    var action = new Action(options);
    return function (data) {
        return new Promise(function (solve, reject, onCancel) {
            var parameters = (data && data.filters) || {};
            action.run(parameters, solve, onCancel);
        });
    };
};
