/*jslint node: true, nomen: true */
"use strict";

var Promise = require('bluebird');

function Action(options) { // add "options" parameters if needed
    // TODO: Global Initialization
    this.api_caller = options.repositories.api_caller;
}

Action.prototype.run = function (parameters, solve) { // add "onCancel" parameters if needed
    // Parameters:
    // parameters['comments']
    // parameters['mail']
    // parameters['user id']

    // TODO: Execution
    var self = this;
    var data = JSON.stringify({

        comments : parameters['comments'],
        email : parameters['mail'],
        user_id : parameters['user id'],
    });
    self.api_caller.call_api('/api/comment','POST', data).then(function(result){
        // $.notify({message: 'Send Mail'}, {allow_dismiss: true, type: 'success'});
        solve({
            event: 'close-page-event', // Close Page
            data: {
            }
        });
    });
    // THIS CAN BE REMOVED (BEGIN)
    // $.notify({message: 'Send Mail'}, {allow_dismiss: true, type: 'success'});
    // solve({
    //     data: {
    //     }
    // });
    // THIS CAN BE REMOVED (END)
};

exports.createAction = function (options) {
    var action = new Action(options);
    return function (data) {
        return new Promise(function (solve, reject, onCancel) {
            var parameters = (data && data.filters) || {};
            action.run(parameters, solve, onCancel);
        });
    };
};
