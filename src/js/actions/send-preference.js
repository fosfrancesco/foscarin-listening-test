/*jslint node: true, nomen: true */
/*globals $*/
"use strict";

var Promise = require('bluebird');

function Action(options) { // add "options" parameters if needed
    // TODO: Global Initialization
    this.api_caller = options.repositories.api_caller;
}

Action.prototype.run = function (parameters, solve) { // add "onCancel" parameters if needed
    // Parameters:
    // parameters['complexity']
    // parameters['survey answers']
    // parameters['user id']
    // parameters['values history']

    // TODO: Execution
    var self = this;
    var data = JSON.stringify({
        survey_answers : parameters['survey answers'],
        user_id : parameters['user id'],
        values_history : parameters['values history']
    });
    self.api_caller.call_api('/api/result','POST', data).then(function(result){
        // $.notify({message: 'Send Preferences'}, {allow_dismiss: true, type: 'success'});
        solve({
            event: 'final-page-event', // Final Page
            data: {
                'user id': parameters['user id'],
            }
        });
    }).catch(function(result){
        solve({
            event: 'final-page-error-event',
            data: {}
        });
    });
    // THIS CAN BE REMOVED (BEGIN)
    // $.notify({message: 'Send Preferences'}, {allow_dismiss: true, type: 'success'});
    // solve({
    //     event: 'final-page-event', // Final Page
    //     data: {
    //         'user id': parameters['user id'],
    //     }
    // });
    // THIS CAN BE REMOVED (END)
};

exports.createAction = function (options) {
    var action = new Action(options);
    return function (data) {
        return new Promise(function (solve, reject, onCancel) {
            var parameters = (data && data.filters) || {};
            action.run(parameters, solve, onCancel);
        });
    };
};
