/*jslint node: true, nomen: true */
"use strict";

exports.createActions = function (options) {
    return {
        'send-mail': require('./send-mail').createAction(options),
        'compute-first-chords': require('./compute-first-chords').createAction(options),
        'send-preference': require('./send-preference').createAction(options),
        'get-id': require('./get-id').createAction(options),
    };
};
