/*jslint node: true, nomen: true */
/*globals $*/
"use strict";

var Promise = require('bluebird');

function Action(options) { // add "options" parameters if needed
    // TODO: Global Initialization
    this.api_caller = options.repositories.api_caller;
}

Action.prototype.run = function (parameters, solve) { // add "onCancel" parameters if needed
    // Parameters:

    // TODO: Execution
    var self = this;
    self.api_caller.call_api('/api/auth','GET', {}).then(function(result){
        // $.notify({message: 'Get Survey'}, {allow_dismiss: true, type: 'success'});
        solve({
            event: 'have-id-event', // Done
            data: {
                'user id':result['user-token'],
            }
        });
    }).catch(function(result){
        solve({
            event: 'have-id-error-event',
            data: {}
        });

    });

    // THIS CAN BE REMOVED (BEGIN)
    // $.notify({message: 'Get Id'}, {allow_dismiss: true, type: 'success'});
    // solve({
    //     event: 'have-id-event', // Done
    //     data: {
    //         'user id': '0',
    //     }
    // });
    // THIS CAN BE REMOVED (END)
};

exports.createAction = function (options) {
    var action = new Action(options);
    return function (data) {
        return new Promise(function (solve, reject, onCancel) {
            var parameters = (data && data.filters) || {};
            action.run(parameters, solve, onCancel);
        });
    };
};
